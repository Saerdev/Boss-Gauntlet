using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class charSelect : MonoBehaviour {

    private string selectedChar;
    private Dictionary<string, GameObject> classDictionary;

    private List<GameObject> spotlights = new List<GameObject>();

    private GameObject cam;
    private GameObject camResetObj;
    private GameObject target;
    public Vector3 camOffset;
    private Vector3 velocity = Vector3.zero;
    public float smoothTime = 0.3f;
    private bool camReset = false;

    public GameObject charDetailM;
    public GameObject charDetailB;
    public GameObject charDetailD;

    public GameObject cancelButton;
    public GameObject createButton;
    public GameObject nameField;

    private bool charCreated;


    void Awake()
    {
        classDictionary = new Dictionary<string, GameObject>()
        {
            { "classBunker", Resources.Load("Prefabs/PlayableCharacters/Bunker") as GameObject },
            { "classManipulator", Resources.Load("Prefabs/PlayableCharacters/Manipulator") as GameObject },
            { "classDemogirl", Resources.Load("Prefabs/PlayableCharacters/Demogirl") as GameObject }
        };

        cam = GameObject.FindGameObjectWithTag("MainCamera");
        camResetObj = GameObject.Find("cameraResetPos");

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Spotlight")) {
            spotlights.Add(obj);
            obj.SetActive(false);
        }
    }


    // Use this for initialization
    void Start()
    {

        charDetailM.SetActive(false);
        charDetailB.SetActive(false);
        charDetailD.SetActive(false);

        cancelButton.SetActive(false);
        createButton.SetActive(false);
        nameField.SetActive(false);
        charCreated = false;

    }


    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonUp(0)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
                if (hit.rigidbody != null) {
                    selectedChar = hit.collider.name;
                    target = hit.transform.gameObject;
                    camReset = false;

                    switch (selectedChar) {
                        case "classManipulator":

                            detailManipulator();

                            break;

                        case "classBunker":

                            detailBunker();
                            break;

                        case "classDemogirl":

                            detailDemogirl();
                            break;

                        default:

                            break;
                    }



                }
        }
    }


    void LateUpdate()
    {

        if (target && target != null && camReset == false)
            cam.transform.position = Vector3.SmoothDamp(cam.transform.position, target.transform.position + camOffset, ref velocity, smoothTime);
        else
            if (camReset)
            cam.transform.position = Vector3.SmoothDamp(cam.transform.position, camResetObj.transform.position + camOffset, ref velocity, smoothTime);
    }

    public void CreatePress()
    {
        if (charCreated == false) {
            charCreated = true;
            Invoke("SwitchScene", 5);
            GameManager.instance.isNewGame = true;
            GameManager.instance.playerClass = classDictionary[selectedChar];
            SaveManager.Instance.saveName = nameField.GetComponent<InputField>().text;
        }
        else
            return;
    }

    public void CancelPress()
    {

        foreach (GameObject light in spotlights)
            light.SetActive(false);

        charDetailM.SetActive(false);
        charDetailB.SetActive(false);
        charDetailD.SetActive(false);

        cancelButton.SetActive(false);
        createButton.SetActive(false);
        nameField.SetActive(false);
        camReset = true;

    }

    void detailManipulator()
    {

        foreach (GameObject light in spotlights)
            if (target == light.transform.parent.gameObject)
                light.SetActive(true);
            else
                light.SetActive(false);

        charDetailM.SetActive(true);
        charDetailB.SetActive(false);
        charDetailD.SetActive(false);


        cancelButton.SetActive(true);
        createButton.SetActive(true);
        nameField.SetActive(true);


    }

    void detailBunker()
    {

        foreach (GameObject light in spotlights)
            if (target == light.transform.parent.gameObject)
                light.SetActive(true);
            else
                light.SetActive(false);

        charDetailM.SetActive(false);
        charDetailB.SetActive(true);
        charDetailD.SetActive(false);

        cancelButton.SetActive(true);
        createButton.SetActive(true);
        nameField.SetActive(true);
    }


    void detailDemogirl()
    {

        foreach (GameObject light in spotlights)
            if (target == light.transform.parent.gameObject)
                light.SetActive(true);
            else
                light.SetActive(false);

        charDetailM.SetActive(false);
        charDetailB.SetActive(false);
        charDetailD.SetActive(true);

        cancelButton.SetActive(true);
        createButton.SetActive(true);
        nameField.SetActive(true);
    }

    void SwitchScene()
    {
        SceneManager.LoadScene("World 1", LoadSceneMode.Single);
    }
}