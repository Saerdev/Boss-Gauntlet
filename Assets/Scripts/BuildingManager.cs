using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class BuildingManager : MonoBehaviour {

    [SerializeField]
    public static int gold;
    [HideInInspector]
    public static Text goldText;

    public GameObject[] buildingTypes;
    private GameObject prevBuildingType;

    public static bool buildMode;
    public static bool placingBuilding;
    private bool validPosition;
    private GameObject draggedBuilding;
    private Collider[] buildingColliders;
    private BoxCollider boxCol;
    private SphereCollider sphereCol;
    private UnityEngine.AI.NavMeshObstacle navMeshObstacle;

    public LayerMask blockMask;

    private enum ColliderType {
        Box,
        Sphere
    }

    private ColliderType colliderType;

    //private Vector3 prevMousePos;

    private Material buildingMaterial;
    private Color originalColor;

	void Start () {
        goldText = GameObject.Find("GoldText").GetComponent<Text>();
        gold = 100;
        UpdateGoldText();

        buildingTypes = Resources.LoadAll<GameObject>("Prefabs/Turrets");
    }
	
	void Update () {
        //Have new structure marker follow mouse
        if (placingBuilding && draggedBuilding) {
            //if (Input.mousePosition != prevMousePos) {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 10)) {
                    draggedBuilding.transform.position = hit.point;
                    //draggedBuilding.transform.position = Vector3.Lerp(draggedBuilding.transform.position, hit.point, 30 * Time.deltaTime);
                    if (colliderType == ColliderType.Box)
                        validPosition = CheckValidPosition(hit.point, boxCol);
                    else if (colliderType == ColliderType.Sphere)
                        validPosition = CheckValidPosition(hit.point, sphereCol);
                }
            //}
            
            //Place building on mouse up
            if (Input.GetKeyUp(KeyCode.Mouse0) && validPosition && !GameManager.eventSystem.IsPointerOverGameObject()) {
                for (int i = 0; i < buildingColliders.Length; i++) {
                    buildingColliders[i].enabled = true;
                }
                navMeshObstacle.enabled = true;
                placingBuilding = false;
                gold -= draggedBuilding.GetComponent<BuildingProperties>().cost;
                UpdateGoldText();

                CreateBuilding(prevBuildingType);
            }

            if (Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyDown(KeyCode.Escape)) {
                Destroy(draggedBuilding);
                placingBuilding = false;
            }
            
            // Optimization so rays aren't cast unless the mouse has moved since the last frame
            //prevMousePos = Input.mousePosition;
        }
	}

    public void CreateBuilding(GameObject buildingType)
    {
        if (!placingBuilding && gold >= buildingType.GetComponent<BuildingProperties>().cost) {
            prevBuildingType = buildingType;
            draggedBuilding = (GameObject)Instantiate(buildingType, Input.mousePosition, Quaternion.identity);
            placingBuilding = true;

            buildingMaterial = draggedBuilding.GetComponentInChildren<SkinnedMeshRenderer>().material;
            originalColor = buildingMaterial.color;

            navMeshObstacle = draggedBuilding.GetComponent<UnityEngine.AI.NavMeshObstacle>();
            navMeshObstacle.enabled = false;

            draggedBuilding.GetComponentsInChildren<Collider>();
            buildingColliders = draggedBuilding.GetComponentsInChildren<Collider>();
            for (int i = 0; i < buildingColliders.Length; i++) {
                buildingColliders[i].enabled = false;
            }
            colliderType = GetColliderType(buildingColliders[0]);
        }
    }

    ColliderType GetColliderType(Collider col)
    {
        if (col.GetType() == typeof(SphereCollider)) {
            sphereCol = draggedBuilding.GetComponent<SphereCollider>();
            return ColliderType.Sphere;
        }
        else if (col.GetType() == typeof(BoxCollider)) {
            boxCol = draggedBuilding.GetComponent<BoxCollider>();
            return ColliderType.Box;
        }

        return ColliderType.Sphere;
    }

    bool CheckValidPosition(Vector3 position, SphereCollider col)
    {
        if (Physics.CheckSphere(position, navMeshObstacle.radius, blockMask)) {
            buildingMaterial.color = Color.red;
            return false;
        }
        else {
            buildingMaterial.color = originalColor;
            return true;
        }
    }

    bool CheckValidPosition(Vector3 position, BoxCollider col)
    {
        if (Physics.CheckBox(position, col.bounds.extents / 2, Quaternion.identity, 1 << 11)) {
            buildingMaterial.color = Color.red;
            return false;
        }
        else {
            buildingMaterial.color = originalColor;
            return true;
        }
    }

    public static void UpdateGoldText()
    {
        if (goldText)
            goldText.text = gold.ToString();
    }
}
