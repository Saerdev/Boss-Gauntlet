using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public class Interact : MonoBehaviour, IKeyListener {

    public delegate void OnPickedUp(GameObject obj);
    public static event OnPickedUp eOnPickedUp;

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.INTERACT, this);
    }

    public void OnTriggerEnter(Collider other)
    {
        //Interactable obj = other.transform.root.GetComponentInChildren<Interactable>();
		Interactable obj = other.GetComponent<Interactable>();
        if (obj) {
            obj.inRange = true;
            GameManager.interactableObjects.Add(obj);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        Interactable obj = other.transform.root.GetComponentInChildren<Interactable>();
        if (obj) {
            obj.inRange = false;
            GameManager.interactableObjects.Remove(obj);
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.INTERACT && GameManager.interactableObjects.Count > 0) {
            GameManager.interactableObjects.RemoveAll(x => x == null);
            Interactable[] sorted = GameManager.interactableObjects.OrderBy(x => GameManager.DistanceSqrd(transform.position, x.transform.position)).ToArray();
            if (eOnPickedUp != null)
                eOnPickedUp(sorted[0].gameObject);
            sorted[0].Interact();

            return true;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }


}
