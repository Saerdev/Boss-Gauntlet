using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class CraftingSlot : MonoBehaviour, IPointerDownHandler {

    [HideInInspector]
    public Item result;
    public GameObject resultObj, ingredientPrefab, ingredient;
    public List<Ingredient> ingredients = new List<Ingredient>();
    public List<GameObject> objRef = new List<GameObject>();

    public Color haveMaterials, unlocked, locked, selected;

    public enum State
    {
        haveMaterials,
            unlocked,
            locked,
            selected
    }

    public State state;

    private Image background;

    void Awake()
    {
        ingredientPrefab = Resources.Load("Prefabs/UI/Ingredient") as GameObject;
        background = GetComponent<Image>();
    }

    void OnEnable()
    {
        Refresh();
    }

	// Use this for initialization
	void Start () {
        Inventory.eOnRefreshed += Refresh;

        if (result) {
            resultObj = transform.GetChild(0).gameObject;
            resultObj.GetComponent<Image>().sprite = result.itemIcon;
            resultObj.GetComponent<Image>().color = Color.white;
            resultObj.GetComponentInChildren<Text>().text = result.recipe.outputQuantity.ToString();

            foreach (Ingredient obj in result.recipe.ingredients) {
                ingredients.Add(obj);
                ingredient = Instantiate(ingredientPrefab);
                objRef.Add(ingredient);
                ingredient.transform.SetParent(transform);
                ingredient.GetComponent<Image>().sprite = obj.ingredient.itemIcon;
                ingredient.GetComponent<Image>().color = Color.white;
                ingredient.GetComponentInChildren<Text>().text = Inventory.instance.QuantityCheck(obj.ingredient).ToString() + " / " +
                                                                 obj.quantity;
            }
        }

        Inventory.eOnRefreshed();
	}

	void Refresh()
    {
        if (result) {
            for (int i = 0; i < ingredients.Count; i++) {
                objRef[i].GetComponentInChildren<Text>().text = Inventory.instance.QuantityCheck(ingredients[i].ingredient).ToString() + " / " + ingredients[i].quantity;

            }
            if (HasMaterials() && CraftingWindow.selectedSlot != this) {
                ChangeState(State.haveMaterials);
            }
            else if (!HasMaterials()){
                ChangeState(State.unlocked);
            }
            else if (HasMaterials() && CraftingWindow.selectedSlot == this) {
                ChangeState(State.selected);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.pointerId == -1)
            CraftingWindow.instance.SelectSlot(this);
    }

    public bool HasMaterials()
    {
        bool hasMats = false;
        for (int i = 0; i < ingredients.Count; i++) {
            if (Inventory.instance.QuantityCheck(ingredients[i].ingredient) >= ingredients[i].quantity)
                hasMats = true;
            else 
                return false;
        }

        return hasMats;
    }

    public void ChangeState(State state)
    {
        switch (state) {
            case State.haveMaterials: background.color = haveMaterials;
                break;
            case State.unlocked: background.color = unlocked;
                break;
            case State.locked: background.color = locked;
                break;
            case State.selected: background.color = selected;
                break;
        }
    }
}
