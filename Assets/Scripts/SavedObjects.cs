using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SavedObjects : MonoBehaviour {}

[Serializable]
public class SavedObjectsList {
    public int sceneID;
    public string playerClass;
    public int skillPoints;
    public List<SavedEnemy> savedEnemies;
    public List<SavedItem> savedItems;
    public List<SavedSkill> savedSkills;
    public List<SavedTurret> savedTurrets;
    public List<SavedTurretUpgrade> savedTurretUpgrades;

    public SavedObjectsList(int newSceneID)
    {
        sceneID = newSceneID;
        savedEnemies = new List<SavedEnemy>();
        savedItems = new List<SavedItem>();
        savedSkills = new List<SavedSkill>();
        savedTurrets = new List<SavedTurret>();
        savedTurretUpgrades = new List<SavedTurretUpgrade>();
    }
}

[Serializable]
public class SavedEnemy {
    public string enemyType;
    public float posX, posY, posZ, hp, energyShield;
    public string target;
}

[Serializable]
public class SavedTurret {
    public int turretIndex;
    public float posX, posY, posZ, hp, energyShield;
}

[Serializable]
public class SavedItem {
    public string itemName;
    public int quantity, slotIndex;
}

[Serializable]
public class SavedTurretUpgrade {
    public string itemName;
    public int turretID, slotIndex;
}

[Serializable]
public class SavedSkill {
    public string skill;
    public List<bool> upgrades;
}
