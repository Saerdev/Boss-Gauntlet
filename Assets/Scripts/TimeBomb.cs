using UnityEngine;
using System.Collections.Generic;
using MovementEffects;

public class TimeBomb : MonoBehaviour {

    public GameObject explosionParticles;
    [HideInInspector] public float detonateDelay, damage, explosionRadius, camShakeAmount;
    [HideInInspector] public LayerMask validTargets;

    public void SetValues(float delay, float damage, float radius, float camShakeAmt, LayerMask validTargets)
    {
        detonateDelay = delay;
        this.damage = damage;
        explosionRadius = radius;
        camShakeAmount = camShakeAmt;
        this.validTargets = validTargets;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground")) {
            Timing.RunCoroutine(_Detonate());
        }
    }

    private IEnumerator<float> _Detonate()
    {
        yield return Timing.WaitForSeconds(detonateDelay);

        Collider[] cols = Physics.OverlapSphere(transform.position, explosionRadius, validTargets);
        foreach (Collider col in cols) {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, (col.transform.position - transform.position).normalized, out hit, explosionRadius, validTargets)) {
                Health targetHp = hit.collider.GetComponent<Health>();
                if (targetHp != null) {
                    targetHp.TakeDamage(damage);
                }
            }
        }

        Instantiate(explosionParticles, transform.position, Quaternion.identity);
        CamShake.instance.ShakeCam(camShakeAmount, 0.3f);
        GlobalBlackboard.explosionCenters.Remove(gameObject);
        Destroy(gameObject);
    }
}
