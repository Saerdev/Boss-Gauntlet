using UnityEngine;
using System.Collections;

public class PersistentUnitySingleton<T> : MonoBehaviour where T : class {
	private static T instance;

	public static T Instance {
		get {
			if (instance == null) {
				instance = GameObject.FindObjectOfType(typeof(T)) as T;

				if (instance == null) {
					GameObject obj = new GameObject();
					obj.hideFlags = HideFlags.HideAndDontSave;
					instance = obj.AddComponent(typeof(T)) as T;
				}
			}
			return instance;
		}
	}

	public virtual void Awake() {
		DontDestroyOnLoad(gameObject);

		if (instance == null)
			instance = this as T;
		else
			Destroy(gameObject);
	}
}
