using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SelectionBase]
public class TurretAI : MonoBehaviour {

    public GameObject turretShot;
    private TargetedProjectile basicShot;
    public float range = 15;
    public float fireRate = 0.5f;
    public float fireRateCap = 0.2f;
    public float damage = 20;
    public float shotSpeed = 10;
    [HideInInspector]
    public bool aspdBuffed;

    private SphereCollider rangeBubble;
    private float lastFire;

    public List<GameObject> targets = new List<GameObject>();
    public GameObject target;
    
    public delegate void OnAttack();
    public OnAttack eOnAttack;

    private TurretInventory turretInv;

    public int deployedIndex;

    void Awake()
    {
        turretInv = GetComponent<TurretInventory>();
        rangeBubble = GetComponentInChildren<SphereCollider>();
    }

    // Use this for initialization
    void Start()
    {
        rangeBubble.radius = range;

        turretShot.CreatePool(10);

        lastFire = 0f;

        //// If a weapon upgrade is equipped
        //if (turretInv.items[0]) {
        //    WeaponUpgrade upgrade = turretInv.items[0].GetComponent<WeaponUpgrade>();
        //    upgrade.turretReference = this;
        //    eOnAttack = upgrade.Attack;
        //    range = upgrade.range;
        //    fireRate = upgrade.fireRate;
        //}
        //else {
        //    eOnAttack = Attack;
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if (lastFire < fireRate)
            lastFire += Time.deltaTime;

        if (eOnAttack != null && lastFire >= fireRate) {
            eOnAttack();
            lastFire = 0;
        }

        rangeBubble.radius = range;
    }

    public void SetValues(float damage, float shotSpeed, float fireRate, float fireRateCap, float range)
    {
        this.damage += damage;
        this.shotSpeed = shotSpeed;
        this.fireRate = fireRate;
        this.fireRateCap = fireRateCap;
        this.range = range;
    }

    public void SetASBuff(bool value)
    {
        aspdBuffed = value;
        if (value) {
            eOnAttack += IncreaseAttackSpeed;
        }
        else {
            eOnAttack -= IncreaseAttackSpeed;

            if (turretInv.items[0]) {
                fireRate = turretInv.items[0].GetComponent<WeaponUpgrade>().fireRate;
            }
            else
                fireRate = 0.5f;
        }
    }

    void IncreaseAttackSpeed()
    {
        if (aspdBuffed && fireRate > fireRateCap) {
            fireRate -= 0.0125f;

            if (fireRate < fireRateCap)
                fireRate = fireRateCap;
        }
    }

    public void Attack()
    {

        //if (lastFire < fireRate)
        //    lastFire += Time.deltaTime;

        if (targets.Count > 0 && lastFire > fireRate) {
            //Attack first enemy to enter perimeter
            if (targets.Count > 0) {
                for (int i = 0; i < targets.Count; i++) {
                    if (targets[i]) {
                        target = targets[i];
                        break;
                    }
                }

                if (target) {
                    Quaternion direction = Quaternion.LookRotation(target.transform.position - transform.position);

                    basicShot = turretShot.Spawn(gameObject.transform.position, direction).GetComponent<TargetedProjectile>();
                    basicShot.SetValues(shotSpeed, damage, target.transform);
                    target.GetComponent<Health>().eOnDestroyed += basicShot.RecycleHelper;

                    //lastFire = 0;
                }
            }
        }
    }

    void RemoveFromTargets(GameObject obj)
    {
        targets.Remove(obj);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            targets.Add(other.gameObject);
            other.GetComponent<Health>().eOnDestroyed += RemoveFromTargets;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            targets.Remove(other.gameObject);
            other.GetComponent<Health>().eOnDestroyed -= RemoveFromTargets;
        }
    }

    void OnDestroy()
    {
        // Ugly D:
        if (deployedIndex == 1)
            TurretDeploy.isActive1 = false;

        if (deployedIndex == 2)
            TurretDeploy.isActive2 = false;
    }

    //public void OnDrawGizmos()
    //{
    //    Gizmos.DrawWireSphere(transform.position, range);
    //}
}
