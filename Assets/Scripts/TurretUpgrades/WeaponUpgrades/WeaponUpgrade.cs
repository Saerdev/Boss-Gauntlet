using UnityEngine;
using System.Collections;

public class WeaponUpgrade : TurretUpgrade {

    public float damage;
    public float range = 15;
    public float fireRate = 0.5f;
    public float fireRateCap = 0.2f;
    public float shotSpeed = 10;
    public GameObject shotPool;
    protected GameObject turretShot;

    protected float lastFire;
}
