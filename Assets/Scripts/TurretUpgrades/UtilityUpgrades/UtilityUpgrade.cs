using UnityEngine;
using System.Collections;

public class UtilityUpgrade : TurretUpgrade {

    public float damage { get; private set; }
    public float range { get; private set; }
    public float fireRate { get; private set; }
    public float fireRateCap { get; private set; }
    public float shotSpeed { get; private set; }
    public GameObject shotPool;
    protected GameObject turretShot;

    protected float lastFire;

    protected virtual void Start()
    {
        lastFire = 0;
        shotPool.CreatePool();
    }

    public virtual void Attack() { }
}
