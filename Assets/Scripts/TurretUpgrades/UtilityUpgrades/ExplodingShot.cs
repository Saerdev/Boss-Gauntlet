using UnityEngine;
using System.Collections;

public class ExplodingShot : UtilityUpgrade {

    public float blastRadius;

    private NexusShot projectile;

    public override void Attack()
    {
        //lastFire += Time.deltaTime;
        if (turretReference.targets.Count > 0) {
            //Attack first enemy to enter perimeter
            if (turretReference.targets.Count > 0) {
                for (int i = 0; i < turretReference.targets.Count; i++) {
                    if (turretReference.targets[i]) {
                        turretReference.target = turretReference.targets[i];
                        break;
                    }
                }

                if (turretReference.target) {
                    Quaternion direction = Quaternion.LookRotation(turretReference.target.transform.position - turretReference.transform.position);

                    projectile = shotPool.Spawn(turretReference.transform.position, direction).GetComponent<NexusShot>();
                    projectile.SetValues(turretReference.shotSpeed, (int)(turretReference.damage * (1 + PlayerPassiveTree.turretDmgModifier)), 3, false, turretReference.target.transform.position, blastRadius);

                    lastFire = 0;
                }
            }
        }
    }
}
