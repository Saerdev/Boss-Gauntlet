using UnityEngine;
using System.Collections;

public class TargetedProjectile : MonoBehaviour {

    [HideInInspector]
	public float speed = 10.0f;
    [HideInInspector]
    public float damage = 20;

	public Transform target;

    private new Transform transform;
    private Vector3 targetPosition;

    private Rigidbody rb;

    void OnEnable()
    {
        transform = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
    }

	// Update is called once per frame
	void Update () {
        if (target != null) {
            Vector3 dir = target.position - transform.position;
            targetPosition = target.position;
            transform.rotation = Quaternion.LookRotation(target.position - transform.position);
            transform.eulerAngles = new Vector3(90, transform.eulerAngles.y, transform.eulerAngles.z);
            rb.velocity = dir.normalized * speed;
            //transform.position = Vector3.Lerp(initialPosition, target.position, speed * Time.deltaTime);
        }
        // If the target died while the projectile is en route
        else if (GameManager.DistanceSqrd(transform.position, targetPosition) < 2 * 2)
                gameObject.Recycle();
	}

    public void SetValues(float speed, float damage, Transform target)
    {
        this.speed = speed;
        this.damage = damage;
        this.target = target;
    }

    void OnDisable()
    {
        if (!GameManager.quittingGame && target)
            target.GetComponent<Enemy>().eOnDestroyed -= RecycleHelper;
    }

    public void RecycleHelper(GameObject obj)
    {
        target = null;
    }

    void OnTriggerEnter(Collider col) {
        if (target) {
            if (col.gameObject == target.gameObject) {
                col.GetComponent<Health>().TakeDamage(damage);

                gameObject.Recycle();
            }
        }
	}
}
