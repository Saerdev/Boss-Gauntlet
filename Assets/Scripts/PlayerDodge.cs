
using UnityEngine;
using System.Collections;
using System;

public class PlayerDodge : MonoBehaviour, IKeyListener {

    public float dodgeSpeed = 10f;
    public float dodgeDuration = 0.4f;
    private float dodgeDurationTimer;

    private Rigidbody rb;
    private Vector3 dodgeDir;
    //private Animator anim;

    [HideInInspector]
    public bool isDodging;

    private StatusEffects playerStatusEffects;
    private Player player;
    private Health playerHealth;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        playerStatusEffects = GetComponent<StatusEffects>();
        playerHealth = GetComponent<Health>();
        player = GetComponent<Player>();
    }

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.DODGE, this);
    }

    void Update()
    {
        if (dodgeDurationTimer > 0)
            dodgeDurationTimer -= Time.deltaTime;
        else
            isDodging = false;

        if (isDodging) {
            dodgeDir.y = rb.velocity.y;
            rb.velocity = dodgeDir;
        }
    }

    //IEnumerator CheckDodgingAnim()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    yield return new WaitUntil(() => !anim.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.DiveForward"));
    //    print("Dodge finished");
    //    isDodging = false;
    //}

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.DODGE && dodgeDurationTimer <= 0 && !isDodging) {
            dodgeDurationTimer = dodgeDuration;

            // Dodge in direction of movement..
            if (player.moveDir.x != 0 || player.moveDir.z != 0)
                // or backwards if no directional keys are being pressed
                dodgeDir = new Vector3(player.moveDir.x, 0, player.moveDir.z);
            else
                dodgeDir = Vector3.back;

            player.DisableMovement(dodgeDuration);
            //anim.SetTrigger("DodgeForward");
            //StartCoroutine(CheckDodgingAnim());
            isDodging = true;
            dodgeDir *= dodgeSpeed;
            playerStatusEffects.AddStatus(new Invulnerable(dodgeDuration, playerHealth));

            return false;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;

    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.DODGE, this);

    }
}
