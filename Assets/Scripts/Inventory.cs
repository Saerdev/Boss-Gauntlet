using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;
using System.Linq;

//[ExecuteInEditMode]
public class Inventory : MonoBehaviour, IKeyListener {

    public const int INVENTORY_SIZE = 30;

    public static Inventory instance;

    private GameObject iconPrefab;
    [SerializeField]
    public List<Item> items = new List<Item>();

    public List<Slot> slots = new List<Slot>(30);

    //public static GameObject draggedItem;

    private GameObject icon;
    //private Transform draggedItemParent;
    private Vector3 startPos;

    public static bool isOpen;
    public static bool changed;

    public static Item changedItem;
    public static int changedIndex;

    public delegate void OnRefreshed();
    public static OnRefreshed eOnRefreshed;

    private GameObject window;

    void Awake()
    {
        instance = this;
        eOnRefreshed = Refresh;

        window = transform.GetChild(0).gameObject;


        iconPrefab = Resources.Load<GameObject>("Prefabs/UI/Icon");
    }

    // Use this for initialization
    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.INVENTORY, this);
        InputManager.Instance.AttachListener(eGameAction.LEFTCLICK, this);
        InputManager.Instance.AttachListener(eGameAction.RIGHTCLICK, this);
        InputManager.Instance.AttachListener(eGameAction.EXIT, this);

        SaveManager.eOnSave += SetSaveData;

        while (items.Count < INVENTORY_SIZE)
            items.Add(null);

        while (slots.Count < INVENTORY_SIZE)
            slots.Add(null);

        int j = 0;
        foreach (Slot slot in window.GetComponentsInChildren<Slot>()) {
            slots[j] = slot;
            j++;
        }

        //AddItem(ItemDatabase.itemDatabase["Crystal"]);
        //AddItem(ItemDatabase.itemDatabase["Ore"]);
        //AddItem(ItemDatabase.itemDatabase["BaseItem"]);

        // Start with inventory closed
        isOpen = true;

        changed = false;
        changedItem = null;
        changedIndex = -1;

        // Set up references
        window.SetActive(true);
        eOnRefreshed();
        window.SetActive(false);
        isOpen = false;

        Player.eOnLooted += AddLoot;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G)) {
            AddItem(ItemDatabase.itemDatabase["Crystal"], 0);
        }

        if (Input.GetKeyDown(KeyCode.X)) {
            eOnRefreshed();
        }
    }

    void AddLoot(Item newItem)
    {
        AddItem(newItem, newItem.quantity);
    }

    public void AddItem(Item newItem, int quantity = 1, int index = -1)
    {
        // Item was added in editor
        if (index != -1) {
            icon = (GameObject)Instantiate(iconPrefab, Vector3.zero, Quaternion.identity);
            icon.transform.SetParent(slots[index].transform);
            items[index] = Instantiate(newItem);
            items[index].name = newItem.name;
            items[index].quantity = quantity;
            items[index].transform.SetParent(icon.transform);
            eOnRefreshed();
            return;
        }

        // Item was added in play mode
        // Check if already holding item type and increase its quantity if true
        for (int j = 0; j < items.Count; j++) {
            if (items[j]) {
                if (items[j].itemName == newItem.itemName && items[j].quantity < items[j].maxStackSize) {
                    items[j].quantity++;
                    quantity--;
                    if (quantity > 0) {
                        AddItem(newItem, quantity);
                        return;
                    }
                    else {
                        eOnRefreshed();
                        return;
                    }
                }
            }
        }

        // If not holding item, add to inventory
        for (int i = 0; i < slots.Count; i++) {
            if (!slots[i].GetComponentInChildren<Item>()) {
                icon = (GameObject)Instantiate(iconPrefab, Vector3.zero, Quaternion.identity);
                icon.transform.SetParent(slots[i].transform);

                items[i] = Instantiate(newItem);
                // Disable all components so it's just an icon
                MonoBehaviour[] comps = items[i].GetComponentsInChildren<MonoBehaviour>();
                for (int j = 0; j < comps.Length; j++) {
                    comps[j].enabled = false;
                }

                // Unless the item is a placeable object...
                PlaceableObject temp = items[i].GetComponentInChildren<PlaceableObject>();
                if (temp)
                    items[i].GetComponentInChildren<PlaceableObject>().gameObject.SetActive(true);

                // Or a turret, which needs its turretID set to remember its inventory
                TurretInventory turInv = items[i].GetComponentInChildren<TurretInventory>();
                TurretInventory addedTurInv = newItem.GetComponentInChildren<TurretInventory>();
                if (addedTurInv && addedTurInv.ID != 0) {
                    turInv.ID = addedTurInv.ID;
                    turInv.enabled = true;
                }
                else if (addedTurInv) {
                    turInv.ID = ++TurretInventory.globalID;
                    turInv.enabled = true;
                }

                items[i].name = newItem.name;
                items[i].transform.SetParent(icon.transform);
                quantity--;
                if (quantity > 0) {
                    AddItem(newItem, quantity);
                    return;
                }

                eOnRefreshed();
                return;
            }
        }
        print("Inventory full!");
    }

    public void RemoveItem(Item item = null, int quantity = 1, int index = -1)
    {
        // Item was removed in editor
        if (index != -1) {
            items[index] = null;
            DestroyImmediate(slots[index].transform.GetChild(0).gameObject);
            return;
        }

        // Item was removed in play mode
        for (int j = items.Count-1; j >= 0; j--) {
            if (items[j]) {
                if (items[j].itemName == item.itemName && items[j].quantity > 1) {
                    items[j].quantity--;
                    quantity--;

                    if (quantity > 0) {
                        RemoveItem(item, quantity);
                        return;
                    }
                    else {
                        eOnRefreshed();
                        return;
                    }
                }
                else if (items[j].itemName == item.itemName) {
                    items[j] = null;
                    Destroy(slots[j].transform.GetChild(0).gameObject);
                    quantity--;
                    if (quantity > 0) {
                        RemoveItem(item, quantity);
                        return;
                    }
                    else {
                        eOnRefreshed();
                        return;
                    }
                }
            }
        }
    }

    void DelayedRefreshHelper()
    {
        StartCoroutine(DelayedRefresh());
    }

    IEnumerator DelayedRefresh()
    {
        yield return new WaitForEndOfFrame();
        Refresh();
    }

    public void Refresh()
    {
        Text quantityText;
        int i = 0;

        // Cycle through slots
        foreach (Slot slot in slots) {

            // Check for icon
            if (slot.GetComponentInChildren<DragHandler>()) {
                items[i] = slot.GetComponentInChildren<Item>();

                icon = slot.GetComponentInChildren<DragHandler>().gameObject;

                // Quantity text
                quantityText = icon.GetComponentInChildren<Text>();
                if (items[i].quantity > 1) {
                    quantityText.text = items[i].quantity.ToString();
                    quantityText.color = Color.red;
                }
                else
                    quantityText.color = Color.clear;

                icon.GetComponent<Image>().sprite = items[i].itemIcon;
                icon.GetComponent<Image>().color = Color.white;
                icon.name = items[i].itemName;
            }
            else
                items[i] = null;
            i++;
        }
    }

    // Not implemented yet
    public void Compact()
    {
        List<Item> duplicates = items.GroupBy(x => x).Where(y => y.Skip(1).Any(g => g)).SelectMany(y => y).ToList();
        foreach(Item item in duplicates) {
            if (item.quantity < item.maxStackSize)
            print(item.itemName + ": " + item.quantity);
        }
    }

    public int QuantityCheck(Item item)
    {
        int quantity = 0;
        foreach(Item _item in items) {
            if (_item && _item.itemName == item.itemName) {
                quantity += _item.quantity;
            }
        }

        return quantity;
    }

    // Allow editing of inventory in editor
    public void OnValidate()
    {
        for (int i = 0; i < items.Count; i++) {
            if (items[i]) {
                // Adding item
                if (!slots[i].GetComponentInChildren<Item>()) {
                    Item temp = items[i];
                    items[i] = null;
                    AddItem(temp, 1, i);
                    eOnRefreshed();
                }
                //// TODO: Replacing item
                //else if (slots[i].GetComponentInChildren<Item>()){
                //    changedIndex = i;
                //    changed = true;
                //    Refresh();
                //    Item temp = items[i];
                //    items[i] = null;
                //    AddItem(temp, i);
                //    Refresh();
                //}
            }
            // TODO: Removing items
            //else if (!items[i] && slots[i].GetComponentInChildren<Item>()) {
            //    changedIndex = i;
            //    changed = true;
            //}
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.INVENTORY) {
            if (window.activeInHierarchy && GameManager.eOnClosedWindow != null)
                GameManager.eOnClosedWindow(window);

            window.gameObject.SetActive(!window.activeInHierarchy);

            if (window.activeInHierarchy)
                GameManager.AddOpenWindow(window);
            else
                GameManager.RemoveOpenWindow(window);

            return true;
        }

        if (action == eGameAction.RIGHTCLICK && DragHandler.draggedItem) {
            DragHandler.draggedItem.transform.position = DragHandler.startPos;
            DragHandler.draggedItem.transform.SetParent(DragHandler.startParent);
            DragHandler.canvasGroup.blocksRaycasts = true;
            DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
            DragHandler.dragging = false;
            DragHandler.draggedItem = null;
            return true;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        if (action == eGameAction.LEFTCLICK && DragHandler.draggedItem && !GameManager.eventSystem.IsPointerOverGameObject()) {
            DragHandler.draggedItem.transform.position = DragHandler.startPos;
            DragHandler.draggedItem.transform.SetParent(DragHandler.startParent);
            DragHandler.canvasGroup.blocksRaycasts = true;
            DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
            DragHandler.dragging = false;
            DragHandler.draggedItem = null;
            return true;
        }

        return false;
    }

    public void SetSaveData()
    {
        for (int i = 0; i < items.Count; i++) {
            SavedItem savedItem = new SavedItem();
            if (items[i]) {
                savedItem.itemName = items[i].itemName;
                savedItem.quantity = items[i].quantity;
                savedItem.slotIndex = i;
                SaveManager.Instance.GetListForScene().savedItems.Add(savedItem);
            }
        }
    }

    void OnDestroy()
    {
        SaveManager.eOnSave -= SetSaveData;
        Player.eOnLooted -= AddLoot;

        InputManager.Instance.RemoveListener(eGameAction.INVENTORY, this);
        InputManager.Instance.RemoveListener(eGameAction.LEFTCLICK, this);
        InputManager.Instance.RemoveListener(eGameAction.RIGHTCLICK, this);
        InputManager.Instance.RemoveListener(eGameAction.EXIT, this);
    }
}
