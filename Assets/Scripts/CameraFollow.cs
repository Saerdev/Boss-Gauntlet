using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraFollow : MonoBehaviour {

    private GameObject target;
    public Vector3 offset;
    private Vector3 velocity = Vector3.zero;
    public float smoothTime = 0.3f;

	// Use this for initialization
	void Start () {
        target = FindObjectOfType<Player>().gameObject;
        //offset = transform.position - target.transform.position;
        if (target)
            transform.position = target.transform.position + offset;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (target)
            transform.position = Vector3.SmoothDamp(transform.position, target.transform.position + offset, ref velocity, smoothTime);
	}
}
