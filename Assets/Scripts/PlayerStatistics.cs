using UnityEngine.SceneManagement;
using UnityEngine;

[System.Serializable]
public class PlayerStatistics {

    public int sceneID;
    public float positionX, positionY, positionZ;

    public float hp, mana, xp;
    public float camPosX, camPosY, camPosZ;
}
