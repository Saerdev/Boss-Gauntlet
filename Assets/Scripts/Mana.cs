using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Mana : MonoBehaviour {

    public float maxMana = 100;
    public float regenPerSecond;
    public float currentMana;

    private Image manaBar;
    private Text manaText;

    void Awake()
    {
        manaBar = GameObject.Find("ManaBar").GetComponent<Image>();
        manaText = GameObject.Find("ManaText").GetComponent<Text>();
    }

    void Start()
    {
        currentMana = maxMana;
    }

	// Update is called once per frame
	void Update () {
        if (currentMana < maxMana) {
            currentMana += regenPerSecond * Time.deltaTime;

            UpdateBar();

            if (currentMana > maxMana) {
                currentMana = maxMana;
            }
        }

        if (currentMana < 0)
            currentMana = 0;
	}

    void UpdateBar()
    {
        manaBar.transform.localScale = new Vector3(currentMana / maxMana, 1, 1);
        manaText.text = currentMana.ToString("N0") + " / " + maxMana.ToString("N0");
    }
}
