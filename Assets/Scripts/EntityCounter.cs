using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EntityCounter : MonoBehaviour {

    public static int entities = 0;

    private Text counterText;

	void Start () {
        counterText = GetComponent<Text>();
        UpdateText(null);
	}
	
    public void UpdateText(GameObject obj)
    {
        if (counterText)
            counterText.text = entities.ToString();
    }
}
