using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[SelectionBase]
public class Spawner : MonoBehaviour {

    [System.Serializable]
    public struct Enemies {
        public GameObject enemy;
        public int weight;
    }

    //public Transform spawnLocation;
    public int spawnsPerWave;

    public List<Enemies> enemies = new List<Enemies>();
    //public float spawnsPerSecond;
    public float minTimeBetweenSpawns, maxTimeBetweenSpawns = 8;
    //public float timeBetweenWaves;

    private OddmentTable<Enemies> enemyList = new OddmentTable<Enemies>();
    public bool isActive, isSpawning;
    private EntityCounter entityCounter;
    private int spawnedCount;
    private GameObject player;

    void Awake()
    {
        entityCounter = FindObjectOfType<EntityCounter>();
    }

    void Start()
    {
        if (FindObjectOfType<Player>())
            player = FindObjectOfType<Player>().gameObject;

        for (int i = 0; i < enemies.Count; i++) {
            enemyList.Add(enemies[i], enemies[i].weight);
        }
		isActive = false;
    }

    void Update()
    {
		
		if (isActive && DayNightCycler.isNightTime == true && !isSpawning) {
            StartCoroutine(Spawn());
        }
    }

    public void StartSpawning()
    {
        if (!isSpawning && player)
            StartCoroutine(Spawn());
    }

	IEnumerator Spawn()
    {
        isSpawning = true;
        for (int i = 0; i < spawnsPerWave; i++) {
            GameObject enemy = enemyList.Pick().enemy;
            string enemyName = enemy.name;
            enemy = (GameObject)Instantiate(enemy, transform.position, Quaternion.identity);
            enemy.name = enemyName;
            Enemy enemyScript = enemy.GetComponent<Enemy>();
            if (enemyScript.enemyName != "Summoner")
                enemyScript.exit = transform.position;
            EntityCounter.entities++;
            entityCounter.UpdateText(null);
            enemyScript.eOnDestroyed += (GameObject obj) => EntityCounter.entities--;
            enemyScript.eOnDestroyed += entityCounter.UpdateText;
            //enemyScript.GetNewPath();
            yield return new WaitForSeconds(Random.Range(minTimeBetweenSpawns, maxTimeBetweenSpawns));
        }

        //yield return new WaitForSeconds(timeBetweenWaves);

        isSpawning = false;
    }
}
