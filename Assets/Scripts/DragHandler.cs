using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IKeyListener, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {

    public static GameObject draggedItem;
    public static Vector3 startPos;
    public static Transform startParent;
    public static bool dragging;
    public static CanvasGroup canvasGroup;

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.RIGHTCLICK, this);
    }

    void Update()
    {
        if (dragging && draggedItem) {
            draggedItem.transform.position = Input.mousePosition;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (draggedItem) {
            dragging = true;
        }
    }

    // Required for OnDrags to work
    public void OnDrag(PointerEventData eventData) { }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (draggedItem) {
            dragging = false;
            draggedItem = null;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Left click only
        if (eventData.pointerId == -1) {
            // Pick up item
            if (!draggedItem) {
                draggedItem = gameObject;

                startPos = transform.position;
                startParent = transform.parent;

                transform.position = Input.mousePosition;

                canvasGroup = GetComponent<CanvasGroup>();
                canvasGroup.blocksRaycasts = false;
                GetComponent<LayoutElement>().ignoreLayout = true;
                transform.SetParent(transform.root);

                if (startParent.parent.name == "TurretInventory") {
                    if ((startParent.name == "WeaponSlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Weapon) ||
                        (startParent.name == "ArmorSlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Armor) ||
                        (startParent.name == "UtilitySlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Utility)) {
                        TurretInvReference reference = startParent.parent.GetComponent<TurretInvReference>();
                        draggedItem.GetComponentInChildren<TurretUpgrade>().lastTurretInvReference = reference;
                        reference.turretInventory.Refresh();
                    }
                }

            }
            // Swap items on mouse down
            else {
                if (dragging) {
                    if (draggedItem) {
                        if (transform.parent.parent.name == "TurretInventory") {
                            if ((transform.parent.name == "WeaponSlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Weapon) || 
                                (transform.parent.name == "ArmorSlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Armor) ||
                                (transform.parent.name == "UtilitySlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Utility)) {
                                TurretInvReference reference = transform.parent.parent.GetComponent<TurretInvReference>();
                                Swap();
                                reference.turretInventory.Refresh();
                            }
                            else {
                                draggedItem.transform.position = startPos;
                                draggedItem.transform.SetParent(startParent);
                                canvasGroup.blocksRaycasts = true;
                                draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
                                dragging = false;
                                draggedItem = null;
                                return;
                            }
                        }
                        else
                            Swap();
                    }
                }
            }
        }

        // Right click
        // Cancel dragging item
        if (eventData.pointerId == -2 && !ObjectPlacer.placingObject) {
            if (gameObject) {
                IUsable obj = gameObject.GetComponentInChildren<IUsable>();
                if (obj != null) {
                    ObjectPlacer.instance.usedClick = true;
                    obj.Use();
                }
            }
        }
    }

    public void Swap()
    {
        DragHandler temp = this;
        draggedItem.transform.SetParent(transform.parent);
        temp.transform.SetParent(startParent);

        dragging = false;
        canvasGroup.blocksRaycasts = true;
        draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
        draggedItem = null;
        Inventory.eOnRefreshed();


    }

    public void Cancel()
    {
        transform.position = startPos;
        transform.SetParent(startParent);

        if (transform.parent.parent.name == "TurretInventory") {
            if ((transform.parent.name == "WeaponSlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Weapon) ||
                (transform.parent.name == "ArmorSlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Armor) ||
                (transform.parent.name == "UtilitySlot" && draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Utility)) {
                TurretInvReference reference = transform.parent.parent.GetComponent<TurretInvReference>();
                reference.turretInventory.Refresh();
            }
        }

        dragging = false;
        canvasGroup.blocksRaycasts = true;
        GetComponent<LayoutElement>().ignoreLayout = false;
    }

    // Required for OnPointers to work
    public void OnPointerClick(PointerEventData eventData) { }

    public void OnPointerUp(PointerEventData eventData)
    {
        // Left click up
        if (eventData.pointerId == -1) {

            if (dragging && (transform.parent == startParent || transform.parent == transform.root)) {
                Cancel();
                //draggedItem = null;
            }
            else if (!dragging && draggedItem) {
                dragging = true;
            }
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        if (ObjectPlacer.instance && ObjectPlacer.instance.usedClick) {
            ObjectPlacer.instance.usedClick = false;
            return true;
        }
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.RIGHTCLICK, this);
    }
}
