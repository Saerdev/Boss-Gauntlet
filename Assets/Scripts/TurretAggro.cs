using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurretAggro : MonoBehaviour {

	public List<GameObject> enemies;

	void Start() {
		enemies = new List<GameObject>();
	}

	void OnTriggerEnter(Collider obj) {
		if (obj.tag == "Enemy")
			enemies.Add(obj.gameObject);
	}

	void OnTriggerExit(Collider obj) {
		enemies.Remove(obj.gameObject);
	}
}
