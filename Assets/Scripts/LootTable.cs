using UnityEngine;
using System.Collections;

public class LootTable : MonoBehaviour {

    [System.Serializable]
    public class WeightedObject {
        public GameObject @object;
        public int rarity;
    }

    [Header("Read-only, total set automatically")]
    [SerializeField]
    private int totalOddment;

    [SerializeField]
    public WeightedObject[] weightedTable = new WeightedObject[0];

    public OddmentTable<GameObject> lootTable = new OddmentTable<GameObject>();

    void Start()
    {
        for (int i = 0; i < weightedTable.Length; i++) {
            lootTable.Add(weightedTable[i].@object, weightedTable[i].rarity);
        }

        //lootTable.Add(null, 1000);
    }

    public void OnValidate()
    {
        totalOddment = 0;

        if (weightedTable != null || weightedTable.Length == 0)
            return;

        foreach (WeightedObject obj in weightedTable) {
            totalOddment += obj.rarity;
        }
    }
}
