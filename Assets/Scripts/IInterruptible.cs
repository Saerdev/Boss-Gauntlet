using UnityEngine;
using System.Collections;

public interface IInterruptible {

    void Interrupt(Brain brain);
}
