using UnityEngine;
using System.Collections;

public class CamShake : MonoBehaviour {

    public static CamShake instance;

    //private Vector3 originalCameraPosition;
    //private Vector3 offset;
    //private GameObject target;

    public float shakeAmt = 0, shakeDuration = 0.3f;

    public Camera mainCamera;

    void Start()
    {
        instance = this;
        //originalCameraPosition = Camera.main.transform.position;
        //target = GameObject.Find("Player");
        //offset = target.transform.position + transform.position;
    }

    //void OnCollisionEnter2D(Collision2D coll)
    //{
    //    shakeAmt = coll.relativeVelocity.magnitude * .0025f;
    //}

    public void ShakeCam(float amount, float duration)
    {
        shakeAmt = amount;
        InvokeRepeating("CameraShake", 0, .01f);
        Invoke("StopShaking", duration);
    }

    void CameraShake()
    {
        if (shakeAmt > 0) {
            float quakeAmt = Random.value * shakeAmt * 2 - shakeAmt;
            Vector3 pp = mainCamera.transform.position;
            pp += Vector3.one * quakeAmt / 5; // can also add to x and/or z
            mainCamera.transform.position = pp;
        }
    }

    void StopShaking()
    {
        CancelInvoke("CameraShake");
        //mainCamera.transform.position = target.transform.position + offset;
    }

}