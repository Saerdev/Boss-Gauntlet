using UnityEngine;
using System.Collections;
using System;

public class Nuke : Skill, IKeyListener {
    public float detonateDelay = 3, damage = 500, detonateRadius = 100;
    public LayerMask validTargets;
    public GameObject targetLocation;

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.SKILL4, this);
    }

    IEnumerator LaunchNuke(Vector3 location)
    {
        GameObject temp = (GameObject)Instantiate(targetLocation, location, Quaternion.identity);
        yield return new WaitForSeconds(detonateDelay);
        Destroy(temp);
        Collider[] targets = Physics.OverlapSphere(location, detonateRadius, validTargets);
        for (int i = 0; i < targets.Length; i++) {
            targets[i].GetComponent<Health>().TakeDamage(damage * (1 + PlayerPassiveTree.skillDmgModifier));
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (action == eGameAction.SKILL4 && mana.currentMana >= manaCost && Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << 10, QueryTriggerInteraction.Ignore)) {
            mana.currentMana -= manaCost;
            StartCoroutine(LaunchNuke(hitInfo.point));
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SKILL4, this);
    }
}
