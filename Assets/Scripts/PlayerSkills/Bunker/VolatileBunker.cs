using UnityEngine;
using System.Collections;
using System;

public class VolatileBunker : Skill, IKeyListener {

    public float detonateTime = 3;
    public float damage = 20;
    public float detonateRadius = 15;
    public float absorbAmount = 75;
    public float moveSpeedMultiplier = .5f;
    private float absorbedAmount;
    public LayerMask validTargets;
    private GameObject shieldEffect;

    private float detonateTimer;
    private bool isShielding, detonated;

    private Health health;
    private Player player;

    protected override void Awake()
    {
        base.Awake();

        player = GetComponent<Player>();

        health = GetComponent<Health>();

        shieldEffect = transform.Find("VolatileBunkerAnimation").gameObject;
    }

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.DODGE, this);
    }

    void DamageShield(float damage, GameObject target = null, GameObject source = null, Type skill = null)
    {
        absorbedAmount += damage;

        if (absorbedAmount >= absorbAmount) {
            health.eOnDamaged -= DamageShield;
            player.moveSpeed *= (1 / moveSpeedMultiplier);
            shieldEffect.SetActive(false);
            isShielding = false;
        }
    }

    void Detonate()
    {
        health.eOnDamaged -= DamageShield;
        player.moveSpeed *= (1 / moveSpeedMultiplier);
        isShielding = false;
        shieldEffect.SetActive(false);                              
        Collider[] targets = Physics.OverlapSphere(transform.position, detonateRadius, validTargets);
        for (int i = 0; i < targets.Length; i++) {
            targets[i].GetComponent<Health>().TakeDamage(damage * (1 + PlayerPassiveTree.skillDmgModifier));
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.DODGE && mana.currentMana >= manaCost) {
            player.moveSpeed *= moveSpeedMultiplier;
            absorbedAmount = 0;
            health.shield += absorbAmount;
            health.eOnDamaged += DamageShield;
            shieldEffect.SetActive(true);
            mana.currentMana -= manaCost;
            isShielding = true;
            detonateTimer = detonateTime;

            //return true;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        if (action == eGameAction.DODGE) {
            detonateTimer -= Time.deltaTime;

            if (detonateTimer <= 0 && isShielding) {
                Detonate();
            }

            //return true;
        }

        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        if (action == eGameAction.DODGE && isShielding) {
            player.moveSpeed *= (1 / moveSpeedMultiplier);
            health.eOnDamaged -= DamageShield;
            health.shield -= (absorbAmount - absorbedAmount);
            isShielding = false;
            shieldEffect.SetActive(false);
        }

        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.DODGE, this);
    }
}
