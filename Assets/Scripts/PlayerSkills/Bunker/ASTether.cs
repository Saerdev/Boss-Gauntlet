using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ASTether : Skill, IKeyListener {

    public float manaDrainPerSecond = 1;
    public LayerMask validTargets;
    private List<GameObject> tethers = new List<GameObject>();

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.SKILL1, this);
    }

    void ApplyTether(GameObject target)
    {
        print("Applied ASTether to " + target);
        tethers.Add(target);
        target.transform.root.GetComponentInChildren<TurretAI>().SetASBuff(true);
    }

    void RemoveTether(GameObject target)
    {
        print("Removed ASTether from " + target);
        tethers.Remove(target);
        target.transform.root.GetComponentInChildren<TurretAI>().SetASBuff(false);
    }

    void Update()
    {
        if (tethers.Count > 0) {
            mana.currentMana -= manaDrainPerSecond * tethers.Count * Time.deltaTime;

            if (mana.currentMana <= 0) {
                for (int i = 0; i < tethers.Count; i++) {
                    RemoveTether(tethers[i]);
                }
                tethers.Clear();
            }
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.SKILL1) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, validTargets, QueryTriggerInteraction.Ignore)) {
                TurretAI target = hitInfo.collider.transform.root.GetComponentInChildren<TurretAI>();
                if (target)
                    if (!tethers.Contains(target.gameObject) && mana.currentMana >= manaDrainPerSecond && mana.currentMana >= manaCost) {
                        mana.currentMana -= manaCost;
                        ApplyTether(target.gameObject);
                    }
                    else if (tethers.Contains(target.gameObject))
                        RemoveTether(target.gameObject);
            }

            return true;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SKILL1, this);
    }
}
