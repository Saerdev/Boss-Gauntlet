using UnityEngine;
using System.Collections;

public class Mine : MonoBehaviour {

    [HideInInspector]
    public float damage, slowAmount, slowDuration, radius;

    public void SetValues(float dmg, float slowAmt, float slowDuration, float radius)
    {
        damage = dmg;
        slowAmount = slowAmt;
        this.slowDuration = slowDuration;
        this.radius = radius;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            Collider[] targets = Physics.OverlapSphere(transform.position, radius, 1 << 8);
            for (int i = 0; i < targets.Length; i++) {
                targets[i].GetComponent<Health>().TakeDamage(damage);
                targets[i].GetComponent<StatusEffectHandler>().ApplySlow(slowDuration, slowAmount);
            }

            Destroy(gameObject);
        }
    }
}
