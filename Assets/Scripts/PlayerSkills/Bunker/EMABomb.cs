using UnityEngine;
using System.Collections;
using System;

public class EMABomb : Skill, IKeyListener {

    public float damage = 5;
    public float explosionRadius = 15;
    public float knockbackForce = 20;

    StatusEffectHandler targetStatusHandler;

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.SKILL3, this);
    }

    void Cast(Vector3 location)
    {
        Collider[] targets = Physics.OverlapSphere(location, explosionRadius);

        for (int i = 0; i < targets.Length; i++) {
            if (targets[i].transform.root.CompareTag("Enemy")) {
                targetStatusHandler = targets[i].transform.root.GetComponentInChildren<StatusEffectHandler>();
                StartCoroutine(targetStatusHandler.Knockback(location, knockbackForce));
            }
            else if (targets[i].transform.root.CompareTag("Turret")) {
                targets[i].transform.root.GetComponentInChildren<Health>().forcedShieldRechargeTime = 4;
            }
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.SKILL3 && mana.currentMana >= manaCost) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << 10)) {
                mana.currentMana -= manaCost;
                Cast(hitInfo.point);
                return true;
            }
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SKILL3, this);
    }
}
