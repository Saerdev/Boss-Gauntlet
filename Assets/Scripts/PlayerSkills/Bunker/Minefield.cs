using UnityEngine;
using System.Collections;
using System;

public class Minefield : Skill, IKeyListener {

    public GameObject minefieldPrefab;
    public float damage = 5, slowAmount = .5f, slowDuration = 2, fieldRadius, mineRadius;

    // Use this for initialization
    void Start () {
        InputManager.Instance.AttachListener(eGameAction.SKILL2, this);
	}

    public bool OnKeyDown(eGameAction action)
    {
        if (mana.currentMana >= manaCost && action == eGameAction.SKILL2) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << 10, QueryTriggerInteraction.Ignore)) {
                GameObject mineField = (GameObject)Instantiate(minefieldPrefab, hitInfo.point, Quaternion.identity);
                mana.currentMana -= manaCost;
                Mine[] mines = mineField.GetComponentsInChildren<Mine>();
                for (int i = 0; i < mines.Length; i++) {
                   mines[i].SetValues(damage * (1 + PlayerPassiveTree.skillDmgModifier), slowAmount, slowDuration, mineRadius);
                }
            }
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SKILL2, this);
    }
}
