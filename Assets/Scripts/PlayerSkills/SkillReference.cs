using UnityEngine;
using System.Collections;

public abstract class SkillReference : MonoBehaviour {

    // Returns false if already learned, so skill points are not spent
    abstract public bool UpgradeSkill();
}
