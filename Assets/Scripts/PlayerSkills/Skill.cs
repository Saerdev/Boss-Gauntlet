using UnityEngine;
using System.Collections;

public class Skill : MonoBehaviour {
    public int manaCost;

    protected Mana mana;

    protected virtual void Awake()
    {
        mana = GetComponent<Mana>();
    }

    public virtual void SaveUpgrades() { }

    public virtual void LoadUpgrades(SavedObjectsList localList) { }
}
