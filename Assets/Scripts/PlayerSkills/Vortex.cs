using UnityEngine;
using System.Collections;

public class Vortex : MonoBehaviour {

    [HideInInspector]
    public float attractionRadius, attractionStrength = 5;
    [HideInInspector]
    public bool isActive;

    public float duration;

    public delegate void OnEnteredRange(Vortex vortex);
    public event OnEnteredRange eOnEnteredRange;

    [HideInInspector]
    public SphereCollider sphereCol;

    void Awake()
    {
        sphereCol = GetComponent<SphereCollider>();
    }

    void Update()
    {
        if (isActive)
            duration -= Time.deltaTime;

        if (duration <= 0)
            Destroy(transform.root.gameObject);
    }

    public void SetValues(float radius, float strength, float duration)
    {
        sphereCol.radius = radius;
        attractionStrength = strength;
        this.duration = duration;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (isActive && other.CompareTag("Enemy")) {
            if (eOnEnteredRange != null)
                eOnEnteredRange(this);
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (isActive && other.CompareTag("Enemy")) {
            other.GetComponent<Rigidbody>().isKinematic = false;
            Vector3 dir = transform.position - other.transform.position;
            dir.y = 0;
            dir.Normalize();
            other.GetComponent<Rigidbody>().AddForce(dir * attractionStrength * Time.deltaTime);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (isActive && other.CompareTag("Enemy")) {
            other.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    void OnDestroy()
    {
        foreach(Collider col in Physics.OverlapSphere(transform.position, attractionRadius, 1 << 8)) {
            col.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

}
