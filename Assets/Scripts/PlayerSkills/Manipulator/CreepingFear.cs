using UnityEngine;
using System.Collections;
using System;

public class CreepingFear : Skill, IKeyListener {

    public float areaRadius = 5, damagePerSecond = 1, duration = 5, timeToSpread = 2, spreadRadius = 5, illusionSpawnChance = 0.07f, sharedDmgRatio = 0.25f, sharedDmgRadius = 7, disorientDuration = 2.5f;
    public GameObject cFearParticle;
    public GameObject cFearTrigger;
    public bool disorient, hysteria, edgeOfMadness, sharedPain;
    private ProjectIllusion projIllusion;

    private Animator anim;

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.SKILL2, this);

        anim = GetComponent<Animator>();

        projIllusion = GetComponent<ProjectIllusion>();

        SaveManager.eOnSave += SaveUpgrades;
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.SKILL2 && mana.currentMana > manaCost) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << 10, QueryTriggerInteraction.Ignore)) {
                Instantiate(cFearParticle, hitInfo.point + Vector3.up, Quaternion.identity);
                Collider[] targets = Physics.OverlapSphere(hitInfo.point, areaRadius, 1 << 8);
                if (targets.Length > 0) {
                    for (int i = 0; i < targets.Length; i++) {
                        Brain brain = targets[i].GetComponent<Brain>();
                        if (brain) {
                            brain.Interrupt(null);
                        }
                        targets[i].GetComponent<Health>().TakeDoT(damagePerSecond, duration, gameObject, typeof(CreepingFear));
                        targets[i].GetComponent<StatusEffectHandler>().ApplyHysteria(damagePerSecond, hysteria ? duration : 0, timeToSpread, hysteria ? spreadRadius : 0, cFearTrigger);

                        if (edgeOfMadness)
                            targets[i].GetComponent<Health>().eOnDefeated += SpawnIllusion;

                        if (sharedPain)
                            targets[i].GetComponent<Health>().eOnDamaged += SharePain;

                        if (disorient)
                            targets[i].GetComponent<StatusEffectHandler>().ApplyDisorient(disorientDuration);
                    }
                }
                mana.currentMana -= manaCost;
                anim.SetLayerWeight(1, 1);
                anim.SetTrigger("Cast2");
            }
        }

        return false;
    }

    public void ApplyDisorient()
    {
        disorient = true;
    }

    public void ApplyHysteria()
    {
        hysteria = true;
    }

    public void ApplyEdgeOfMadness()
    {
        edgeOfMadness = true;
    }

    public void ApplySharedPain()
    {
        sharedPain = true;
    }

    // For Edge of Madness
    void SpawnIllusion(GameObject obj)
    {
        int dice = UnityEngine.Random.Range(0, 100);
        if (dice <= illusionSpawnChance * 100) {

            Collider[] targets = Physics.OverlapSphere(obj.transform.position, 90, 1 << 8);
            float dist = 100;
            GameObject target = null;
            foreach(Collider col in targets) {
                if (GameManager.DistanceSqrd(obj.transform.position, col.transform.position) < dist * dist) {
                    dist = GameManager.DistanceSqrd(obj.transform.position, col.transform.position);
                    target = col.gameObject;
                }
            }

            // Launch illusion at closest target within 90 meters
            if (target)
                projIllusion.eOnCast(target.transform.position, obj.transform.position);
            // If no target, launch in random direction
            else
                projIllusion.eOnCast(obj.transform.position + new Vector3(UnityEngine.Random.onUnitSphere.x, 0, UnityEngine.Random.onUnitSphere.z) * 5, obj.transform.position);
        }
    }

    public void SharePain(float damage, GameObject target, GameObject source, Type skill)
    {
        // Taking damage from share pain does not proc share pain
        if (skill != typeof(SharedPain)) {
            Collider[] targets = Physics.OverlapSphere(target.transform.position, sharedDmgRadius, 1 << 8);
            foreach (Collider col in targets) {
                if (col.gameObject != target)
                    col.transform.root.GetComponentInChildren<Health>().TakeDamage(damage * sharedDmgRatio, target, typeof(SharedPain));
            }
        }
    }

    public override void SaveUpgrades()
    {
        SavedSkill savedSkill = new SavedSkill();
        savedSkill.upgrades = new System.Collections.Generic.List<bool>();

        savedSkill.skill = this.GetType().ToString();
        savedSkill.upgrades.Add(disorient);
        savedSkill.upgrades.Add(hysteria);
        savedSkill.upgrades.Add(edgeOfMadness);
        savedSkill.upgrades.Add(sharedPain);

        SaveManager.Instance.GetListForScene().savedSkills.Add(savedSkill);
    }

    public override void LoadUpgrades(SavedObjectsList localList)
    {
        foreach(SavedSkill skill in localList.savedSkills) {
            if (skill.skill == this.GetType().ToString()) {
                disorient = skill.upgrades[0];
                hysteria = skill.upgrades[1];
                edgeOfMadness = skill.upgrades[2];
                sharedPain = skill.upgrades[3];
            }
        }
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SKILL2, this);

        SaveManager.eOnSave -= SaveUpgrades;
    }
}
