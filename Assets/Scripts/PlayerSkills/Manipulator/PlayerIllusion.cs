using UnityEngine;
using System.Collections;

public class PlayerIllusion : MonoBehaviour {

    [HideInInspector]
    public float passThroughDamage, duration, speed, explosionRadius, explosionDamage;
    [HideInInspector]
    public bool explodes, taunts;

    private Rigidbody rb;
    private bool matter;
    private CreepingFear creepingFear;

    void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        creepingFear = FindObjectOfType<Player>().GetComponent<CreepingFear>();
        GetComponentInParent<StatusEffectHandler>().ApplyInvuln(duration + 1);
    }

    void Update()
    {
        rb.velocity = transform.forward * speed;
    }

    void Explode()
    {
        if (explodes) {
            Collider[] targets = Physics.OverlapSphere(transform.position, explosionRadius, 1 << 8);
            for (int i = 0; i < targets.Length; i++) {
                targets[i].GetComponent<Health>().TakeDamage(explosionDamage, Player.rb.gameObject, typeof(ProjectIllusion));
            }
        }

        gameObject.Recycle();
    }

    public void SetValues(float passDmg, float duration, float spd, bool detonates, bool taunts, float explosionRadius = 0, float expDamage = 0, float tauntRadius = 0, bool matter = false)
    {
		passThroughDamage = passDmg;
        this.duration = duration;
        speed = spd;
        this.explodes = detonates;
        this.taunts = taunts;
        this.explosionRadius = explosionRadius;
        explosionDamage = expDamage;
        this.matter = matter;

        if (taunts)
            GetComponentInChildren<Taunt>(true).SetRadius(tauntRadius);
        else {
            GetComponentInChildren<Taunt>(true).gameObject.SetActive(false);
        }

        Invoke("Explode", duration);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            if (matter) {
                other.GetComponent<Health>().TakeDoT(creepingFear.damagePerSecond, creepingFear.duration);
                other.GetComponent<StatusEffectHandler>().ApplyHysteria(creepingFear.damagePerSecond, creepingFear.hysteria ? duration : 0, creepingFear.timeToSpread, creepingFear.hysteria ? creepingFear.spreadRadius : 0, creepingFear.cFearTrigger);
            }

            other.GetComponent<Health>().TakeDamage(passThroughDamage, Player.rb.gameObject, typeof(ProjectIllusion));
        }
    }
}
