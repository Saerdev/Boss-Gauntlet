using UnityEngine;
using System.Collections;
using System;

public class ProjectIllusion : Skill, IKeyListener {

    public GameObject illusionPrefab;
    public float passThroughDamage = 7, duration = 2.5f, speed = 10, explosionRadius = 12, explosionDamage = 9, tauntRadius = 15;
    public bool explosiveIllusion, smoothOperator, insaneLaughter, smokeAndMirrors;

    [HideInInspector]
    public GameObject illusion;
    [HideInInspector]
    public bool isActive;

    private Animator anim;

    public delegate void OnCast(Vector3 targetPos, Vector3 sourcePos);
    public OnCast eOnCast;

    public bool matter;

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.SKILL1, this);

        anim = GetComponent<Animator>();

        eOnCast = BaseSkill;

        SaveManager.eOnSave += SaveUpgrades;
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (mana.currentMana > manaCost && action == eGameAction.SKILL1 && (!isActive || smoothOperator)) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << 10)) {
                UnityEngine.AI.NavMeshHit navHit;
                if (UnityEngine.AI.NavMesh.SamplePosition(hitInfo.point, out navHit, .5f, UnityEngine.AI.NavMesh.AllAreas)) {
                    eOnCast(hitInfo.point, transform.position);
					mana.currentMana -= manaCost;

                    anim.SetLayerWeight(1, 1);
                    anim.SetTrigger("Cast1");
                }
            }
        }
        else if (action == eGameAction.SKILL1 && isActive && !smoothOperator && illusion) {
            Vector3 playerPos = transform.position;
            transform.position = new Vector3(illusion.transform.position.x, transform.position.y, illusion.transform.position.z);
            illusion.transform.position = playerPos;
        }

        return false;
    }

    void BaseSkill(Vector3 targetPos, Vector3 sourcePos)
    {
        Vector3 direction = targetPos - sourcePos;
        direction.y = 0;
        direction.Normalize();
        Vector3 spawnPos = targetPos;
        spawnPos.y = 1;
        illusion = (GameObject)Instantiate(illusionPrefab, spawnPos, Quaternion.LookRotation(direction));
        PlayerIllusion playerIllusion = illusion.GetComponentInChildren<PlayerIllusion>();
        playerIllusion.SetValues(passThroughDamage * (1 + PlayerPassiveTree.skillDmgModifier), duration, speed, explosiveIllusion, insaneLaughter, explosionRadius, explosionDamage * (1 + PlayerPassiveTree.skillDmgModifier), tauntRadius, matter);
        StartCoroutine(SetActiveState());
    }

    public void ApplyExplosiveIllusion()
    {
        explosiveIllusion = true;
    }

    public void ApplySmoothOperator()
    {
        smoothOperator = true;

        duration = 1;
    }

    public void ApplyInsaneLaughter()
    {
        insaneLaughter = true;
    }

    public void ApplySmokeAndMirrors()
    {
        smokeAndMirrors = true;

        passThroughDamage *= 1.5f;
        if (explosiveIllusion) {
            explosionDamage *= 1.5f;
            explosionRadius *= 1.5f;
        }

        if (insaneLaughter)
            tauntRadius *= 1.5f;

        if (smoothOperator)
            manaCost = (int)(manaCost * 0.5f);
    }

    IEnumerator SetActiveState()
    {
        isActive = true;
        yield return new WaitForSeconds(duration);
        isActive = false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    public override void SaveUpgrades()
    {
        SavedSkill savedSkill = new SavedSkill();
        savedSkill.upgrades = new System.Collections.Generic.List<bool>();

        savedSkill.skill = this.GetType().ToString();
        savedSkill.upgrades.Add(explosiveIllusion);
        savedSkill.upgrades.Add(smoothOperator);
        savedSkill.upgrades.Add(insaneLaughter);
        savedSkill.upgrades.Add(smokeAndMirrors);

        SaveManager.Instance.GetListForScene().savedSkills.Add(savedSkill);
    }

    public override void LoadUpgrades(SavedObjectsList localList)
    {
        foreach (SavedSkill skill in localList.savedSkills) {
            if (skill.skill == this.GetType().ToString()) {
                explosiveIllusion = skill.upgrades[0];
                smoothOperator = skill.upgrades[1];
                insaneLaughter = skill.upgrades[2];
                smokeAndMirrors = skill.upgrades[3];
            }
        }
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SKILL1, this);
        SaveManager.eOnSave -= SaveUpgrades;
    }
}
