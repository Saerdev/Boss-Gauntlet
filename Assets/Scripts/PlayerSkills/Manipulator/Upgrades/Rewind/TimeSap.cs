using UnityEngine;
using System.Collections;

public class TimeSap : SkillReference {

    Rewind skill;

    void Awake()
    {
        skill = FindObjectOfType<Rewind>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.timeSap)
            return false;

        skill.ApplyTimeSap();
        return true;
    }
}
