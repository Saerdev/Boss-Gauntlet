using UnityEngine;
using System.Collections;

public class TemporalShift : SkillReference {

    Rewind skill;

    void Awake()
    {
        skill = FindObjectOfType<Rewind>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.temporalShift)
            return false;

        skill.ApplyTemporalShift();
        return true;
    }
}
