using UnityEngine;
using System.Collections;
using System;

public class Slipstream : SkillReference {

    Rewind skill;

    void Awake()
    {
        skill = FindObjectOfType<Rewind>();
    }
    
    public override bool UpgradeSkill()
    {
        if (skill.slipstream)
            return false;

        skill.ApplySlipstream();
        return true;
    }
}
