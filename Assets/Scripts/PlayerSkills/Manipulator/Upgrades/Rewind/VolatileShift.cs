using UnityEngine;
using System.Collections;

public class VolatileShift : SkillReference {

    Rewind skill;

    void Awake()
    {
        skill = FindObjectOfType<Rewind>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.volatileShift)
            return false;

        skill.ApplyVolatileRewind();
        return true;
    }
}
