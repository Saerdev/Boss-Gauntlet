using UnityEngine;
using System.Collections;

public class ExoticMatter : SkillReference {

    Wormhole skill;

    void Awake()
    {
        skill = FindObjectOfType<Wormhole>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.exoticMatter)
            return false;

        skill.ApplyExoticMatter();
        return true;
    }
}
