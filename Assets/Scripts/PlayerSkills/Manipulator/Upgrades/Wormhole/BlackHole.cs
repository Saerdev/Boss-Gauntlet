using UnityEngine;
using System.Collections;
using System;

public class BlackHole : SkillReference {

    Wormhole skill;

    void Awake()
    {
        skill = FindObjectOfType<Wormhole>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.blackHole)
            return false;

        skill.ApplyVortex();
        return true;
    }
}
