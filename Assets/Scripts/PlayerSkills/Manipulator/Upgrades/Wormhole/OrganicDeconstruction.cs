using UnityEngine;
using System.Collections;

public class OrganicDeconstruction : SkillReference {

    Wormhole skill;

    void Awake()
    {
        skill = FindObjectOfType<Wormhole>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.organicDeconstruction)
            return false;

        skill.ApplyOrganicDeconstruction();
        return true;
    }
}
