using UnityEngine;
using System.Collections;

public class Supermassive : SkillReference {

    Wormhole skill;

    void Awake()
    {
        skill = FindObjectOfType<Wormhole>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.superMassive)
            return false;

        skill.ApplySupermassive();
        return true;
    }
}
