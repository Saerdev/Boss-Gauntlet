using UnityEngine;
using System.Collections;
using System;

public class InsaneLaughter : SkillReference {

    ProjectIllusion skill;

    void Awake()
    {
        skill = FindObjectOfType<ProjectIllusion>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.insaneLaughter)
            return false;

        skill.ApplyInsaneLaughter();
        return true;
    }
}
