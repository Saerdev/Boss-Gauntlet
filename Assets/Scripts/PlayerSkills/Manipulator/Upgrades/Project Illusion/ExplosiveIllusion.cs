using UnityEngine;
using System.Collections;
using System;

public class ExplosiveIllusion : SkillReference {

    ProjectIllusion skill;

    void Awake()
    {
        skill = FindObjectOfType<ProjectIllusion>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.explosiveIllusion)
            return false;

        skill.ApplyExplosiveIllusion();
        return true;
    }
}
