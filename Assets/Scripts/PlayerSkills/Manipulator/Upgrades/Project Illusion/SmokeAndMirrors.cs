using UnityEngine;
using System.Collections;

public class SmokeAndMirrors : SkillReference {

    ProjectIllusion skill;

    void Awake()
    {
        skill = FindObjectOfType<ProjectIllusion>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.smokeAndMirrors)
            return false;

        skill.ApplySmokeAndMirrors();
        return true;
    }
}
