using UnityEngine;
using System.Collections;

public class SmoothOperator : SkillReference {

    ProjectIllusion skill;

    void Awake()
    {
        skill = FindObjectOfType<ProjectIllusion>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.smoothOperator)
            return false;

        skill.ApplySmoothOperator();
        return true;
    }
}
