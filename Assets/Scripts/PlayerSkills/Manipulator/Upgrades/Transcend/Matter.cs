using UnityEngine;
using System.Collections;

public class Matter : SkillReference {

    Transcend skill;

    void Awake()
    {
        skill = FindObjectOfType<Transcend>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.matter)
            return false;

        skill.ApplyMatter();
        return true;
    }
}
