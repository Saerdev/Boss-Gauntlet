using UnityEngine;
using System.Collections;

public class Space : SkillReference {

    Transcend skill;

    void Awake()
    {
        skill = FindObjectOfType<Transcend>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.space)
            return false;

        skill.ApplySpace();
        return true;
    }
}
