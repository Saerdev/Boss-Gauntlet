using UnityEngine;
using System.Collections;

public class TTime : SkillReference {

    Transcend skill;

    void Awake()
    {
        skill = FindObjectOfType<Transcend>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.time)
            return false;

        skill.ApplyTime();
        return true;
    }
}
