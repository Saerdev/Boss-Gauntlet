using UnityEngine;
using System.Collections;

public class Mind : SkillReference {

    Transcend skill;

    void Awake()
    {
        skill = FindObjectOfType<Transcend>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.mind)
            return false;

        skill.ApplyMind();
        return true;
    }
}
