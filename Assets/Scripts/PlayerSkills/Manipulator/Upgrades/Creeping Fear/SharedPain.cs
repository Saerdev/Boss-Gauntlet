using UnityEngine;
using System.Collections;

public class SharedPain : SkillReference {

    CreepingFear skill;

    void Awake()
    {
        skill = FindObjectOfType<CreepingFear>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.sharedPain)
            return false;

        skill.ApplySharedPain();
        return true;
    }
}
