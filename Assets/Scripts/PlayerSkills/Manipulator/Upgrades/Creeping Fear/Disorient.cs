using UnityEngine;
using System.Collections;

public class Disorient : SkillReference {

    CreepingFear skill;

    void Awake()
    {
        skill = FindObjectOfType<CreepingFear>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.disorient)
            return false;

        skill.ApplyDisorient();
        return true;
    }
}
