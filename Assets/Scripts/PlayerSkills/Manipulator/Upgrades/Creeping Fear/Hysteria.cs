using UnityEngine;
using System.Collections;

public class Hysteria : SkillReference {

    CreepingFear skill;

    void Awake()
    {
        skill = FindObjectOfType<CreepingFear>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.hysteria)
            return false;

        skill.ApplyHysteria();
        return true;
    }
}
