using UnityEngine;
using System.Collections;

public class EdgeOfMadness : SkillReference {

    CreepingFear skill;

    void Awake()
    {
        skill = FindObjectOfType<CreepingFear>();
    }

    public override bool UpgradeSkill()
    {
        if (skill.edgeOfMadness)
            return false;

        skill.ApplyEdgeOfMadness();
        return true;
    }
}
