using UnityEngine;
using System;
using System.Collections;

public class Rewind : Skill, IKeyListener {

    public float delay, explosionRadius = 7, explosionDamage, slowAmount = 0.5f, slowDuration = 2, cooldown;

    [Range(0, 5)]
    public float timeStoreLength = 3;
    private Status[] posList;
    private int i = 0, lastElement;

    public bool slipstream, volatileShift, timeSap, temporalShift;

    private Health health;
    private StatusEffectHandler seHandler;
    private PlayerHealthBar playerHealthBar;
    private float cooldownTimer;
    private bool onCooldown;

    private GameObject model;
    private Animator anim;

    protected override void Awake()
    {
        base.Awake();

        health = GetComponent<Health>();
        seHandler = GetComponent<StatusEffectHandler>();
        playerHealthBar = FindObjectOfType<PlayerHealthBar>();
        model = transform.FindChild("PlayerModel").gameObject;

        anim = GetComponent<Animator>();
        cooldownTimer = -cooldown;
    }

    struct Status {
        public Vector3 pos;
        public float health;

        public Status(Vector3 position, int hp)
        {
            pos = position;
            health = hp;
        }
    }

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.DODGE, this);

        posList = new Status[(int)(timeStoreLength / Time.fixedDeltaTime)];
        lastElement = (int)((timeStoreLength / Time.fixedDeltaTime) - 1);

        SaveManager.eOnSave += SaveUpgrades;
    }

    void Update()
    {
        if (Time.time - cooldownTimer > cooldown) {
            onCooldown = false;
        }
        else {
            onCooldown = true;
        }
    }

    void FixedUpdate()
    {
        posList[i].pos = transform.position;
        posList[i].health = health.currentHealth;

        if (i >= lastElement) {
            i = lastElement;

            Status[] temp = posList;
            for (int j = 0; j < posList.Length - 1; j++) {
                posList[j] = temp[j + 1];
            }
        }
        else
            i++;
    }

    void OnValidate()
    {
        // Round to 2 digits
        timeStoreLength = (float)Math.Round(timeStoreLength, 2);
    }

    public void ApplySlipstream()
    {
        slipstream = true;

        manaCost /= 2;
        timeStoreLength = 1;

        Status[] tempList = new Status[(int)(timeStoreLength / Time.fixedDeltaTime)];
        for (int j = 0; j < tempList.Length; j++) {
            tempList[j] = posList[j];
        }
        posList = tempList;
        lastElement = (int)((timeStoreLength / Time.fixedDeltaTime) - 1);
        i = lastElement;
    }

    public void ApplyVolatileRewind()
    {
        volatileShift = true;
    }

    public void ApplyTimeSap()
    {
        timeSap = true;
    }

    public void ApplyTemporalShift()
    {
        temporalShift = true;
    }

    void Volatility()
    {
        Collider[] targets = Physics.OverlapSphere(transform.position, explosionRadius, 1 << 8);
        foreach (Collider target in targets) {
            target.transform.root.GetComponentInChildren<Health>().TakeDamage(explosionDamage);
        }
    }

    void TimeSap()
    {
        Collider[] targets = Physics.OverlapSphere(transform.position, explosionRadius, 1 << 8);
        foreach (Collider target in targets) {
            target.transform.root.GetComponentInChildren<StatusEffectHandler>().ApplySlow(slowAmount, slowDuration);
        }
    }

    IEnumerator _Rewind(float delay)
    {
        float remainingDelay = delay;
        Vector3 startPos = transform.position;
        Vector3 targetPos = posList[0].pos;

        float currentLerpTime = 0, percentage = 0;

        seHandler.ApplyInvuln(delay);

        model.SetActive(false);
        while (currentLerpTime < delay) {
            transform.position = Vector3.Lerp(startPos, targetPos, percentage);

            currentLerpTime += Time.deltaTime;
            percentage = currentLerpTime / delay;

            yield return null;
        }
        model.SetActive(true);
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (mana.currentMana >= manaCost && action == eGameAction.DODGE && !onCooldown) {
			mana.currentMana -= manaCost;
            cooldownTimer = Time.time;
            anim.SetLayerWeight(1, 1);
            anim.SetTrigger("CastShift");

            if (TurretDeploy.isActive1)
                GetComponent<TurretDeploy>().turret1.GetComponent<StatusEffectHandler>().ApplyInvuln(temporalShift ? 2 : 1);

            if (TurretDeploy.isActive2)
                GetComponent<TurretDeploy>().turret2.GetComponent<StatusEffectHandler>().ApplyInvuln(temporalShift ? 2 : 1);

            if (volatileShift)
                Volatility();

            if (timeSap)
                TimeSap();

            StartCoroutine(_Rewind(delay));
            //transform.position = posList[0].pos;
            health.currentHealth = posList[0].health;
            health.UpdateHealthBar(0);
            playerHealthBar.UpdateBar();

            if (volatileShift)
                Volatility();

            if (timeSap)
                TimeSap();
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    public override void SaveUpgrades()
    {
        SavedSkill savedSkill = new SavedSkill();
        savedSkill.upgrades = new System.Collections.Generic.List<bool>();

        savedSkill.skill = this.GetType().ToString();
        savedSkill.upgrades.Add(slipstream);
        savedSkill.upgrades.Add(volatileShift);
        savedSkill.upgrades.Add(timeSap);
        savedSkill.upgrades.Add(temporalShift);

        SaveManager.Instance.GetListForScene().savedSkills.Add(savedSkill);
    }

    public override void LoadUpgrades(SavedObjectsList localList)
    {
        foreach (SavedSkill skill in localList.savedSkills) {
            if (skill.skill == this.GetType().ToString()) {
                slipstream = skill.upgrades[0];
                volatileShift = skill.upgrades[1];
                timeSap = skill.upgrades[2];
                temporalShift = skill.upgrades[3];
            }
        }
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.DODGE, this);

        SaveManager.eOnSave -= SaveUpgrades;
    }
}
