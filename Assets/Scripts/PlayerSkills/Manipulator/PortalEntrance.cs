using UnityEngine;
using System.Collections;

public class PortalEntrance : MonoBehaviour {

    [HideInInspector]
    public GameObject portalExit;

    public delegate void OnEntered(GameObject obj, GameObject entrance = null);
    public event OnEntered eOnEntered;

    public SphereCollider sphereCol;

    void Awake()
    {
        portalExit = transform.FindChild("PortalExit").gameObject;
        portalExit.SetActive(false);
        sphereCol = GetComponent<SphereCollider>();
        eOnEntered = null;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            if (eOnEntered != null)
                eOnEntered(other.gameObject, gameObject);
            other.transform.position = portalExit.transform.position;
        }
    }

    void OnDestroy()
    {
        eOnEntered = null;
    }
}
