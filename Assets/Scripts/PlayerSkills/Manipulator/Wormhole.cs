using UnityEngine;
using System.Collections;
using System;

public class Wormhole : Skill, IKeyListener {

    public float duration = 10, suctionRadius = 5, suctionStrength = 300, passThroughDamage = 3, supermassiveIncrease = 1.5f;
    public GameObject wormholePrefab;
    private bool entranceIsPlaced, exitIsPlaced;

    private Animator anim;

    [HideInInspector]
    public GameObject entrance, portalExit;

    public delegate void OnCast(Vector3 location);
    public event OnCast eOnCast;

    public bool blackHole, organicDeconstruction, superMassive, exoticMatter;

    // Ult
    public bool isTranscended;

    // Use this for initialization
    void Start () {
        InputManager.Instance.AttachListener(eGameAction.SKILL3, this);
        InputManager.Instance.AttachListener(eGameAction.RIGHTCLICK, this);
        InputManager.Instance.AttachListener(eGameAction.EXIT, this);

        anim = GetComponent<Animator>();

        eOnCast = BaseSkill;

        SaveManager.eOnSave += SaveUpgrades;
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.SKILL3 && mana.currentMana >= manaCost) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << 10, QueryTriggerInteraction.Ignore)) {
                if (eOnCast != null) {
                    eOnCast(hitInfo.point);

                }
            }
        }

        if ((action == eGameAction.RIGHTCLICK || action == eGameAction.EXIT) && entranceIsPlaced && !exitIsPlaced) {
            Destroy(entrance);
            entranceIsPlaced = false;
        }

        return false;
    }

    void BaseSkill(Vector3 location)
    {
        if (!entranceIsPlaced) {
            if (superMassive) {
                Collider[] cols = Physics.OverlapSphere(location, 7, 1 << 0, QueryTriggerInteraction.Collide);
                GameObject wormHole = null;
                foreach (Collider col in cols) {
                    if (col.name == "Wormhole")
                        wormHole = col.gameObject;
                }
                if (wormHole) {
                    mana.currentMana -= manaCost;
                    GameObject wormHoleExit = wormHole.transform.FindChild("PortalExit").gameObject;
                    Vector3 originalPos = wormHoleExit.transform.position;
                    wormHole.transform.localScale *= supermassiveIncrease;
                    wormHoleExit.transform.position = originalPos;
                }
                else {
                    entrance = Instantiate(wormholePrefab, location + Vector3.up, Quaternion.identity) as GameObject;
                    entrance.name = "Wormhole";
                    entrance.GetComponentInChildren<Vortex>().SetValues(blackHole ? suctionRadius : 0, suctionStrength, exoticMatter ? duration + 4 : duration);
                    portalExit = entrance.transform.FindChild("PortalExit").gameObject;
                    entranceIsPlaced = true;
                    exitIsPlaced = false;
                    anim.SetLayerWeight(1, 1);
                    anim.SetTrigger("Cast3");
                }
            }
            else {
                entrance = Instantiate(wormholePrefab, location + Vector3.up, Quaternion.identity) as GameObject;
                entrance.name = "Wormhole";
                entrance.GetComponentInChildren<Vortex>().SetValues(blackHole ? suctionRadius : 0, suctionStrength, exoticMatter ? duration + 4 : duration);
                portalExit = entrance.transform.FindChild("PortalExit").gameObject;
                entranceIsPlaced = true;
                exitIsPlaced = false;
                anim.SetLayerWeight(1, 1);
                anim.SetTrigger("Cast3");
            }
        }
        else {
            mana.currentMana -= manaCost;
            portalExit.transform.position = location;
            portalExit.SetActive(true);
            entrance.GetComponent<SphereCollider>().enabled = true;
            entrance.GetComponentInChildren<Vortex>().isActive = true;

            if (organicDeconstruction)
                entrance.GetComponent<PortalEntrance>().eOnEntered += OrganicDeconstruction;

            if (isTranscended)
                entrance.GetComponentInChildren<PortalEntrance>().eOnEntered += GetComponent<Transcend>().ExpandVortex;
            entranceIsPlaced = false;
            exitIsPlaced = true;
        }
    }

    public void ApplyOrganicDeconstruction()
    {
        organicDeconstruction = true;
    }

    public void ApplySupermassive()
    {
        superMassive = true;
    }

    public void ApplyVortex()
    {
        blackHole = true;
    }

    public void ApplyExoticMatter()
    {
        exoticMatter = true;
    }

    void OrganicDeconstruction(GameObject obj, GameObject prtlEntrance)
    {
        obj.GetComponent<Health>().TakeDamage(passThroughDamage, entrance);
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    public override void SaveUpgrades()
    {
        SavedSkill savedSkill = new SavedSkill();
        savedSkill.upgrades = new System.Collections.Generic.List<bool>();

        savedSkill.skill = this.GetType().ToString();
        savedSkill.upgrades.Add(blackHole);
        savedSkill.upgrades.Add(organicDeconstruction);
        savedSkill.upgrades.Add(superMassive);
        savedSkill.upgrades.Add(exoticMatter);

        SaveManager.Instance.GetListForScene().savedSkills.Add(savedSkill);
    }

    public override void LoadUpgrades(SavedObjectsList localList)
    {
        foreach (SavedSkill skill in localList.savedSkills) {
            if (skill.skill == this.GetType().ToString()) {
                blackHole = skill.upgrades[0];
                organicDeconstruction = skill.upgrades[1];
                superMassive = skill.upgrades[2];
                exoticMatter = skill.upgrades[3];
            }
        }
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SKILL3, this);
        eOnCast -= BaseSkill;

        SaveManager.eOnSave -= SaveUpgrades;
    }
}
