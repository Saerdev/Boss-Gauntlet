using UnityEngine;
using System.Collections;

public class Taunt : MonoBehaviour {

    private SphereCollider col;

    void Awake()
    {
        col = GetComponent<SphereCollider>();
    }

    public void SetRadius(float radius)
    {
        col.radius = radius;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            other.GetComponent<StatusEffectHandler>().ApplyTaunt(transform.root.gameObject);
        }
    }
}
