using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Transcend : Skill, IKeyListener {

    public float duration = 10;
    private bool isActive;

    private Animator anim;

    private Rewind rewind;

    // Project Illusion variables
    public GameObject playerIllusion;
    public int projectIllusionSplitCount = 24;
    public float illusionPulseInterval = 0.5f;

    private ProjectIllusion projIllusion;
    private GameObject illusion, objPool;

    private CreepingFear creepingFear;

    // Black Hole variables
    private Wormhole wormHole;
    public float vortexRangeIncreasePerFeed = 0.3f, vortexDurationIncreasePerFeed = 0.25f;

    public bool matter, mind, space, time;

    void Start () {
        InputManager.Instance.AttachListener(eGameAction.SKILL4, this);

        anim = GetComponent<Animator>();

        rewind = GetComponent<Rewind>();
        projIllusion = GetComponent<ProjectIllusion>();
        creepingFear = GetComponent<CreepingFear>();
        wormHole = GetComponent<Wormhole>();

        playerIllusion.CreatePool((int)(projectIllusionSplitCount * duration));
        objPool = FindObjectOfType<ObjectPool>().gameObject;

        SaveManager.eOnSave += SaveUpgrades;
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.SKILL4 && mana.currentMana >= manaCost) {
            mana.currentMana -= manaCost;
            anim.SetLayerWeight(1, 1);
            anim.SetTrigger("Cast4");

            if (isActive) {
                DisableUlt();
                CancelInvoke("DisableUlt");
            }
            Invoke("DisableUlt", duration);

            HandleRewind();
            HandleProjectIllusion();
            HandleBlackHole();
            HandleCreepingFear();

            isActive = true;
        }

        return false;
    }

    void DisableUlt()
    {
        CancelInvoke("Pulse");
        projIllusion.eOnCast -= SetPulsing;
        projIllusion.matter = false;

        rewind.manaCost = 15;

        wormHole.isTranscended = false;
        foreach (Vortex vortex in FindObjectsOfType<Vortex>()) {
            if (vortex.isActive) {
                vortex.transform.parent.GetComponent<PortalEntrance>().eOnEntered -= ExpandVortex;
            }
        }

        if (mind) {
            creepingFear.damagePerSecond /= 2;
            creepingFear.duration /= 2;
            creepingFear.spreadRadius /= 2;
            creepingFear.timeToSpread *= 2;
            creepingFear.disorientDuration /= 2;
            creepingFear.sharedDmgRadius /= 2;
            creepingFear.sharedDmgRatio /= 2;
            creepingFear.illusionSpawnChance /= 2;
        }

        isActive = false;
    }

    void HandleRewind()
    {
        rewind.manaCost = 0;
    }

    void HandleCreepingFear()
    {
        // Replace existing Creeping Fears with ulted ones
        if (mind) {
            creepingFear.damagePerSecond *= 2;
            creepingFear.duration *= 2;
            creepingFear.spreadRadius *= 2;
            creepingFear.timeToSpread /= 2;
            creepingFear.disorientDuration *= 2;
            creepingFear.sharedDmgRadius *= 2;
            creepingFear.sharedDmgRatio *= 2;
            creepingFear.illusionSpawnChance *= 2;

            foreach (StatusEffectHandler target in FindObjectsOfType<StatusEffectHandler>()) {
                if (target.CompareTag("Enemy")) {
                    List<Health.Dot> dots = target.GetComponent<Health>().dots;
                    for (int i = 0; i < target.GetComponent<Health>().dots.Count; i++) {
                        if (dots[i].skillSource == typeof(CreepingFear)) {
                            dots[i] = new Health.Dot(dots[i].source, dots[i].damage * 2, dots[i].duration * 2, dots[i].skillSource);
                        }
                    }
                    target.GetComponent<Health>().dots = dots;

                    target.disorientDuration *= 2;
                    target.SetHysteriaValues(creepingFear.damagePerSecond * 2, creepingFear.duration * 2, creepingFear.timeToSpread / 2, creepingFear.spreadRadius * 2);
                }
            }
        }
    }

    void HandleBlackHole()
    {
        // Find existing black holes and make them hungry
        foreach(Vortex vortex in FindObjectsOfType<Vortex>()) {
            if (vortex.isActive) {
                vortex.transform.parent.GetComponent<PortalEntrance>().eOnEntered += ExpandVortex;
                //vortex.duration = duration;
            }
        }

        // Cause new black holes to be hungry
        wormHole.isTranscended = true;
    }

    void DestroyProjectiles(GameObject obj, GameObject entrance)
    {
        if (obj.CompareTag("Projectile")) {
            obj.Recycle();
        }
    }

    public void ExpandVortex(GameObject obj, GameObject entrance)
    {
        Vortex vortex = entrance.GetComponentInChildren<Vortex>();
        vortex.sphereCol.radius += vortexRangeIncreasePerFeed;
        vortex.duration += vortexDurationIncreasePerFeed;
    }

    #region Project Illusion
    void HandleProjectIllusion()
    {
        projIllusion.matter = matter;
        if (projIllusion.illusion)
            SetPulsing(Vector3.zero, Vector3.zero);

        // Enhanced Project Illusion
        projIllusion.eOnCast += SetPulsing;
    }

    void SetPulsing(Vector3 targetPos, Vector3 sourcePos)
    {
        CancelInvoke("Pulse");
        InvokeRepeating("Pulse", 0, illusionPulseInterval);
    }

    void Pulse()
    {
        if (projIllusion.illusion) {
            for (int i = 0; i < projectIllusionSplitCount; i++) {
                illusion = playerIllusion.Spawn(projIllusion.illusion.transform.position, Quaternion.identity);
                illusion.transform.Rotate(Vector3.up, i * (360 / projectIllusionSplitCount));
                illusion.GetComponent<PlayerIllusion>().SetValues(projIllusion.passThroughDamage, projIllusion.duration, projIllusion.speed, projIllusion.explosiveIllusion, projIllusion.insaneLaughter, projIllusion.explosionRadius, projIllusion.explosionDamage, 0, matter);
                illusion.transform.SetParent(objPool.transform);    
            }
        }
    }
    #endregion

    public void ApplyMatter()
    {
        matter = true;
    }

    public void ApplyMind()
    {
        mind = true;
    }

    public void ApplySpace()
    {
        space = true;
    }

    public void ApplyTime()
    {
        time = true;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    public override void SaveUpgrades()
    {
        SavedSkill savedSkill = new SavedSkill();
        savedSkill.upgrades = new System.Collections.Generic.List<bool>();

        savedSkill.skill = this.GetType().ToString();
        savedSkill.upgrades.Add(matter);
        savedSkill.upgrades.Add(mind);
        savedSkill.upgrades.Add(space);
        savedSkill.upgrades.Add(time);

        SaveManager.Instance.GetListForScene().savedSkills.Add(savedSkill);
    }

    public override void LoadUpgrades(SavedObjectsList localList)
    {
        foreach (SavedSkill skill in localList.savedSkills) {
            if (skill.skill == this.GetType().ToString()) {
                matter = skill.upgrades[0];
                mind = skill.upgrades[1];
                space = skill.upgrades[2];
                time = skill.upgrades[3];
            }
        }
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SKILL4, this);

        SaveManager.eOnSave -= SaveUpgrades;
    }

}
