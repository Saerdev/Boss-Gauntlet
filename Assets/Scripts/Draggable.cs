using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {

    [HideInInspector]
    public static bool canDrag;
    [HideInInspector]
    public static bool draggingWindow;

    protected Vector3 dragOffset;
    protected GameObject window;

    void Start()
    {
        window = gameObject;
        canDrag = true;
    }

    // Draggable window
    public void OnDrag(PointerEventData eventData)
    {
        if (draggingWindow) {
            GameManager.draggingUI = true;
            draggingWindow = true;
            window.transform.position = Input.mousePosition + dragOffset;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (draggingWindow) {
            GameManager.draggingUI = false;
            draggingWindow = false;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (canDrag) {
            GameManager.draggingUI = true;
            draggingWindow = true;
            dragOffset = window.transform.position - Input.mousePosition;
        }
    }

    // Required for OnPointers
    public void OnPointerClick(PointerEventData eventData) { }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (draggingWindow) {
            GameManager.draggingUI = false;
            draggingWindow = false;
        }
    }
}
