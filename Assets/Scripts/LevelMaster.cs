using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelMaster : MonoBehaviour {

    public GameObject[] enemyTypes;
    private GameObject turret;

    private Dictionary<string, GameObject> classDictionary;

    void Awake()
    {
        classDictionary = new Dictionary<string, GameObject>()
        {
            { "Bunker", Resources.Load("Prefabs/PlayableCharacters/Bunker") as GameObject },
            { "Manipulator", Resources.Load("Prefabs/PlayableCharacters/Manipulator") as GameObject },
            //{ "Demogirl", Resources.Load("Prefabs/PlayableCharacters/Demogirl") as GameObject }
        };

        turret = Resources.Load("Prefabs/Turrets/Turret") as GameObject;
    }

	// Use this for initialization
	void Start () {
        SaveManager.Instance.InitializeSceneList();

        if (SaveManager.Instance.isSceneBeingLoaded || SaveManager.Instance.isSceneBeingTransitioned) {
            SavedObjectsList localList = SaveManager.Instance.GetListForScene();

            if (localList != null) {
                // Load player
                GameObject player = Instantiate(classDictionary[localList.playerClass]);
                // Remove "(Clone)" from end of name
                player.name = player.name.Remove(player.name.Length - 7);

                // Load turrets
                for (int i = 0; i < localList.savedTurrets.Count; i++) {
                    GameObject spawnedTurret = Instantiate(turret, new Vector3(localList.savedTurrets[i].posX,
                                                                               localList.savedTurrets[i].posY,
                                                                               localList.savedTurrets[i].posZ), Quaternion.identity) as GameObject;
                    spawnedTurret.GetComponent<Health>().currentHealth = localList.savedTurrets[i].hp;
                    spawnedTurret.GetComponent<Health>().currentShield = localList.savedTurrets[i].energyShield;
                }

                // Load turret inventories

                // Load enemies
                for (int i = 0; i < localList.savedEnemies.Count; i++) {
                    GameObject spawnedEnemy;
                    foreach(GameObject enemyType in enemyTypes) {
                        if (localList.savedEnemies[i].enemyType == enemyType.name) {
                            spawnedEnemy = Instantiate(enemyType, new Vector3(localList.savedEnemies[i].posX,
                                                                          localList.savedEnemies[i].posY,
                                                                          localList.savedEnemies[i].posZ), Quaternion.identity) as GameObject;
                            spawnedEnemy.GetComponent<Health>().currentHealth = localList.savedEnemies[i].hp;
                            spawnedEnemy.GetComponent<Health>().currentShield = localList.savedEnemies[i].energyShield;

                            break;
                        }
                    }
                }

                // Load inventory
                for (int i = 0; i < localList.savedItems.Count; i++) {
                    if (localList.savedItems[i] != null && Inventory.instance.items[i] == null) {
                        Inventory.instance.AddItem(ItemDatabase.itemDatabase[localList.savedItems[i].itemName], localList.savedItems[i].quantity, localList.savedItems[i].slotIndex);
                    }
                }
                //Inventory.eOnRefreshed();

                // Load skill tree
                foreach(Skill skill in player.GetComponents<Skill>()) {
                    skill.LoadUpgrades(localList);
                }

                PlayerPassiveTree.instance.unallocatedPoints = localList.skillPoints;
            }
            else
                print("Local list was null!");

            //SaveManager.Instance.isSceneBeingLoaded = false;
        }
	}
}
