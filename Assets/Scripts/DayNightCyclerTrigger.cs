using UnityEngine;
using System.Collections;

public class DayNightCyclerTrigger : MonoBehaviour {

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            GameManager.dayNightCycler.enabled = true;
            Destroy(gameObject);
        }
    }
}
