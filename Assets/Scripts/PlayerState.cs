using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerState : MonoBehaviour {

    public static PlayerState Instance;

    public PlayerStatistics localPlayerData = new PlayerStatistics();

    private Player player;
    private Health health;
    private Mana mana;

    void Awake()
    {
        if (Instance == null)
            Instance = this;

        if (Instance != this)
            Destroy(gameObject);

        player = GetComponent<Player>();
        health = GetComponent<Health>();
        mana = GetComponent<Mana>();

    }

    void Start()
    {
        SaveManager.eOnSave += SetSaveData;
        if (SaveManager.Instance.isSceneBeingLoaded) {
            LoadSaveData();
            SaveManager.Instance.isSceneBeingLoaded = false;
        }
    }

    void SetSaveData()
    {
        print("Saved player state");
        localPlayerData.sceneID = SceneManager.GetActiveScene().buildIndex;
        localPlayerData.positionX = transform.position.x;
        localPlayerData.positionY = transform.position.y;
        localPlayerData.positionZ = transform.position.z;
        localPlayerData.camPosX = Camera.main.transform.position.x;
        localPlayerData.camPosY = Camera.main.transform.position.y;
        localPlayerData.camPosZ = Camera.main.transform.position.z;

        localPlayerData.hp = health.currentHealth;
        localPlayerData.mana = mana.currentMana;
        localPlayerData.xp = player.xp;

        SaveManager.Instance.GetListForScene().playerClass = gameObject.name;

        //SaveManager.Instance.SaveData();
    }

    void LoadSaveData()
    {
        localPlayerData = SaveManager.Instance.localCopyOfData;

        transform.position = new Vector3(localPlayerData.positionX,
                                            localPlayerData.positionY,
                                            localPlayerData.positionZ);

        Camera.main.transform.position = new Vector3(localPlayerData.camPosX,
                                                     localPlayerData.camPosY,
                                                     localPlayerData.camPosZ);

        health.currentHealth = localPlayerData.hp;
        mana.currentMana = localPlayerData.mana;
        player.xp = localPlayerData.xp;

        FindObjectOfType<PlayerHealthBar>().UpdateBar();
    }

    void OnDestroy()
    {
        SaveManager.eOnSave -= SetSaveData;
    }
}
