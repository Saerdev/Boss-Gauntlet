using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class SaveManager : PersistentUnitySingleton<SaveManager>, IKeyListener {

    public PlayerStatistics localCopyOfData;
    public bool isSceneBeingLoaded, isSceneBeingTransitioned;

    public string saveName;

    public delegate void SaveDelegate();
    public static event SaveDelegate eOnSave;

    public List<SavedObjectsList> savedLists = new List<SavedObjectsList>();

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.SAVE, this);
        InputManager.Instance.AttachListener(eGameAction.LOAD, this);
    }

    public void InitializeSceneList()
    {
        if (savedLists == null) {
            print("Saved lists was null");
            savedLists = new List<SavedObjectsList>();
        }

        bool found = false;

        // Check if a list of saved items for this level already exists
        for (int i = 0; i < savedLists.Count; i++) {
            if (savedLists[i].sceneID == SceneManager.GetActiveScene().buildIndex) {
                found = true;
            }
        }

        // If not, create it
        if (!found) {
            SavedObjectsList newList = new SavedObjectsList(SceneManager.GetActiveScene().buildIndex);
            savedLists.Add(newList);
        }
    }

    public SavedObjectsList GetListForScene()
    {
        for (int i = 0; i < savedLists.Count; i++) {
            if (savedLists[i].sceneID == SceneManager.GetActiveScene().buildIndex)
                return savedLists[i];
        }

        return null;
    }

    public void FireSaveEvent()
    {
        //GetListForScene().savedEnemies = new List<SavedEnemy>();
        //GetListForScene().savedItems = new List<SavedItem>();

        if (eOnSave != null) {
            eOnSave();
        }
    }

    public void SaveData()
    {
        if (!Directory.Exists("Saves"))
            Directory.CreateDirectory("Saves");

        FireSaveEvent();

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Create("Saves/" + saveName + ".save");
        FileStream saveObjects = File.Create("saves/" + saveName + "Objects.save");

        localCopyOfData = PlayerState.Instance.localPlayerData;

        formatter.Serialize(saveFile, localCopyOfData);
        formatter.Serialize(saveObjects, savedLists);

        saveFile.Close();
        saveObjects.Close();
    }

    public void LoadData()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream saveFile = File.Open("Saves/" + saveName + ".save", FileMode.Open);
        FileStream saveObjects = File.Open("Saves/" + saveName + "Objects.save", FileMode.Open);

        localCopyOfData = (PlayerStatistics)formatter.Deserialize(saveFile);
        savedLists = (List<SavedObjectsList>)formatter.Deserialize(saveObjects);

        saveFile.Close();
        saveObjects.Close();
        isSceneBeingLoaded = true;

        int sceneID = localCopyOfData.sceneID;

        SceneManager.LoadScene(sceneID);
        print("Loaded");
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.SAVE && eOnSave != null) {
            SaveData();

            return true;
        }

        if (action == eGameAction.LOAD) {
            LoadData();
            
            return true;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SAVE, this);
        InputManager.Instance.RemoveListener(eGameAction.LOAD, this);
    }
}
