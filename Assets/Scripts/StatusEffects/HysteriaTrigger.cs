using UnityEngine;
using System.Collections;

public class HysteriaTrigger : MonoBehaviour {

    [HideInInspector]
    public float radius;

    private SphereCollider col;

    void Awake()
    {
        col = GetComponent<SphereCollider>();
    }

    public void SetValues(float radius)
    {
        col.radius = radius;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            other.GetComponent<StatusEffectHandler>().hysteriaContributors.Add(gameObject);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            other.GetComponent<StatusEffectHandler>().hysteriaContributors.Remove(gameObject);
        }
    }

    void OnDestroy()
    {
        foreach (Collider target in Physics.OverlapSphere(transform.position, radius, 1 << 8)) {
            if (target.GetComponent<StatusEffectHandler>().hysteriaContributors.Contains(gameObject))
                target.GetComponent<StatusEffectHandler>().hysteriaContributors.Remove(gameObject);
        }
    }
}
