using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatusEffects : MonoBehaviour {

    public List<StatusEffect> statusEffects = new List<StatusEffect>();

    void Awake()
    {
        statusEffects.Clear();
    }

    public void AddStatus(StatusEffect status)
    {
        foreach(StatusEffect _status in statusEffects) {
            if (_status.ToString() == status.ToString())
                _status.Overlap();
                return;
        }
        statusEffects.Add(status);
        status.Begin();
    }
    
	void Update () {
        for (int i = statusEffects.Count - 1; i >= 0; i--) {
            //print(statusEffects[i].ToString());
            if (statusEffects[i].expired) {
                //print("Removing " + statusEffects[i].ToString());
                statusEffects.RemoveAt(i);
                continue;
            }
            statusEffects[i].Tick();
        }

        //if (Input.GetKeyDown(KeyCode.B))
        //    AddStatus(new Burn(1f, gameObject.GetComponent<Health>()));
	}
}
