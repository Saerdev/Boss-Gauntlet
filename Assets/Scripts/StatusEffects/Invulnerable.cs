using UnityEngine;
using System.Collections;

public class Invulnerable : StatusEffect {

    public Invulnerable(float baseDuration, Health targetHealth) : base(baseDuration, targetHealth)
    {
        name = "Invulnerable";
        this.targetHealth = targetHealth;

        effectType = EffectType.Buff;
        eOnGainedBuff += ApplyInvuln;
        eOnLostBuff += RemoveInvuln;
    }

    void ApplyInvuln()
    {
        invulnerable = true;
        targetHealth.invulnerable = true;
    }

    void RemoveInvuln()
    {
        invulnerable = false;
        targetHealth.invulnerable = false;
    }
}
