﻿using UnityEngine;
using System.Collections;

public class MarkOfProtection : StatusEffect {

    private GameObject rovPrefab;
    //private Color oldColor, newColor;
    private float timeToFullyFormed, fadeTime;

    void Awake()
    {
        
    }

    public MarkOfProtection(float baseDuration, Health targetHealth) : base(baseDuration, targetHealth)
    {
        rovPrefab = Resources.Load("SkillEffects/RoV") as GameObject;

        this.targetHealth = targetHealth;
        StatusEffects effects = targetHealth.GetComponent<StatusEffects>();

        foreach (StatusEffect effect in effects.statusEffects) {
            if (effect.ToString() == this.ToString()) {
                return;
            }
        }

        //oldColor = rovPrefab.GetComponent<Renderer>().sharedMaterial.GetColor("_Color");
        //newColor = new Color(oldColor.r, oldColor.g, oldColor.b, 0);
        //name = "Reversal of Fortune";

        if (targetHealth != null) {
            this.targetHealth = targetHealth;

            effectType = EffectType.Buff;
            //eOnGainedBuff += () => invulnerable = true;
            //eOnLostBuff += () => invulnerable = false;
            eOnLostBuff += RemoveBuff;

            targetHealth.eOnHit += Heal;

            rovPrefab = Object.Instantiate(rovPrefab, targetHealth.gameObject.transform.position, targetHealth.gameObject.transform.rotation) as GameObject;
            //rovPrefab.GetComponent<Renderer>().material = Resources.Load("Materials/RoV") as Material;
            //rovPrefab.transform.localScale *= 1.5f;
        }
    }

    public override void Tick()
    {
        base.Tick();

    }

    void Heal(float amount)
    {
        targetHealth.Heal(amount);

        //remainingDuration = 0;
    }

    void RemoveBuff()
    {
        Debug.Log("Lost MoP");
        targetHealth.eOnHit -= Heal;
        Object.Destroy(rovPrefab);
    }

}
