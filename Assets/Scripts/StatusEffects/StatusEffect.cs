using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class StatusEffect {

    public enum EffectType {
        Buff,
        Debuff
    }

    public string name = "";
    public bool invulnerable;
    public bool active, expired;
    public float baseDuration, remainingDuration;
    protected EffectType effectType;
    protected Health targetHealth;

    public delegate void OnGainedBuff();
    public delegate void OnGainedDebuff();

    public event OnGainedBuff eOnGainedBuff;
    public event OnGainedDebuff eOnGainedDebuff;

    public delegate void OnLostBuff();
    public delegate void OnLostDebuff();

    public event OnLostBuff eOnLostBuff;
    public event OnLostDebuff eOnLostDebuff;

    public StatusEffect(float baseDuration, Health targetHealth)
    {
        StatusEffects effects = targetHealth.GetComponent<StatusEffects>();

        foreach (StatusEffect effect in effects.statusEffects) {
            if (effect.ToString() == this.ToString()) {
                return;
            }
        }

        this.baseDuration = baseDuration;
        this.remainingDuration = baseDuration;
        this.targetHealth = targetHealth;

        //targetHealth.eOnDefeated += End;
    }

    public virtual void Begin()
    {
        if (effectType == EffectType.Buff && eOnGainedBuff != null)
            eOnGainedBuff();

        if (effectType == EffectType.Debuff && eOnGainedDebuff != null)
            eOnGainedDebuff();
    }

    public virtual void Tick()
    {
        remainingDuration -= Time.deltaTime;

        if (remainingDuration <= 0f) {
            End();
            return;
        }
    }

    public virtual void Overlap()
    {
        remainingDuration = baseDuration;
    }

    public virtual void End()
    {
        if (effectType == EffectType.Buff && eOnLostBuff != null)
            eOnLostBuff();

        if (effectType == EffectType.Debuff && eOnLostDebuff != null)
            eOnLostDebuff();

        expired = true;
        active = false;
    }
}