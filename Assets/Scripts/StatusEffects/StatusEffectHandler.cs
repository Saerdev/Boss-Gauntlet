using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StatusEffectHandler : MonoBehaviour {

    private Rigidbody rb;
    private Enemy enemyAI;
    private UnityEngine.AI.NavMeshAgent agent;

    // Hysteria
    public GameObject hysteriaTriggerPrefab;
    private GameObject hysteriaTrigger; 
    private float baseHysteriaDamage, baseHysteriaDuration, baseHysteriaSpreadTime, baseHysteriaSpreadRadius;
    private float hysteriaTimer, hysteriaSpreadTimer;
    private bool hasHysteria;
    //[HideInInspector]
    public List<GameObject> hysteriaContributors = new List<GameObject>();
    private CreepingFear manipulator;

    // Slows
    public float slowTimer, slowModifier, originalSpeed;

    // Invulns
    public bool isInvulnerable;
    public float invulnTimer;

    private bool wasKinematic;
    private bool isRecovering;

    // Disorients
    public bool isDisoriented;
    public float disorientDuration;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        enemyAI = GetComponent<Enemy>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        //if (enemyAI)
        //    rb.isKinematic = true;

        wasKinematic = rb.isKinematic;
    }

    void Start()
    {
        manipulator = FindObjectOfType<Player>().GetComponent<CreepingFear>();

        if (enemyAI)
            originalSpeed = enemyAI.speed;

        if (manipulator)
            SetHysteriaValues(manipulator.damagePerSecond, manipulator.duration, manipulator.timeToSpread, manipulator.spreadRadius);
    }

    void Update()
    {
        HandleHysteria();
        HandleSlows();
        HandleInvuln();
        HandleDisorient();
    }

    void FixedUpdate()
    {
        //if (!isRecovering && rb.IsSleeping()) {
        //    StartCoroutine(Recover());
        //}
    }

    #region Invuln
    public void ApplyInvuln(float duration)
    {
        if (invulnTimer < duration)
            invulnTimer = duration;
    }

    void HandleInvuln()
    {
        if (invulnTimer > 0) {
            isInvulnerable = true;
            invulnTimer -= Time.deltaTime;
        }
        else {
            isInvulnerable = false;   
        }
    }
    #endregion

    #region Taunt
    public void ApplyTaunt(GameObject target)
    {
        enemyAI.GetNewPath(target);
    }
    #endregion

    #region Slows
    // Slows
    void HandleSlows()
    {
        if (slowTimer > 0) {
            slowTimer -= Time.deltaTime;
        }
        else {
            if (enemyAI)
                enemyAI.speed = originalSpeed;
            slowModifier = 1;
        }
    }

    public void ApplySlow(float time, float strength)
    {
        if (slowTimer < time)
            slowTimer = time;

        if (slowModifier > strength)
            slowModifier *= strength;

        enemyAI.speed *= slowModifier;
    }
    #endregion

    #region Knockback
    // Recovering from knockback
    IEnumerator Recover()
    {
        yield return new WaitForFixedUpdate();
        if (enemyAI) {
            enemyAI.GetNewPath();
        }
        rb.isKinematic = wasKinematic;
    }

    // Knockbacks
    public IEnumerator Knockback(Vector3 sourcePos, float force)
    {
        if (enemyAI) {
            if (agent)
                agent.Stop();
            //enemyAI.enabled = false;
        }

        if (rb) {
            wasKinematic = rb.isKinematic;
            rb.isKinematic = false;
            rb.velocity = Vector3.zero;
            yield return new WaitForFixedUpdate();
            rb.AddForce((transform.position - sourcePos).normalized * force, ForceMode.VelocityChange);
            yield return new WaitForSeconds(Random.Range(2, 3));
            if (rb) {
                StartCoroutine(Recover());
            }
        }
    }
    #endregion

    #region Hysteria
    public void ApplyHysteria(float damage, float duration, float spreadTime, float spreadRadius, GameObject triggerPrefab)
    {
        baseHysteriaDamage = damage;
        baseHysteriaSpreadTime = spreadTime;
        baseHysteriaDuration = duration;
        hysteriaSpreadTimer = spreadTime;

        if (!hasHysteria && spreadRadius > 0) {
            hasHysteria = true;
            hysteriaTrigger = Instantiate(triggerPrefab, transform.position, Quaternion.identity) as GameObject;
            hysteriaTrigger.transform.SetParent(transform);
            hysteriaTrigger.name = "HysteriaTrigger";
            hysteriaTrigger.GetComponent<HysteriaTrigger>().radius = spreadRadius;
        }

        if (hysteriaTimer < duration) {
            hysteriaTimer = duration;
        }
    }

    public void SetHysteriaValues(float damage, float duration, float spreadTime, float spreadRadius)
    {
        baseHysteriaDamage = damage;
        baseHysteriaDuration = duration;
        baseHysteriaSpreadTime = spreadTime;
        baseHysteriaSpreadRadius = spreadRadius;
    }

    void HandleHysteria()
    {
        if (hysteriaTimer > 0) {
            hysteriaTimer -= Time.deltaTime;
                
            if (hysteriaTimer <= 0) {
                hasHysteria = false;
                Destroy(hysteriaTrigger);
            }
        }

        if (hysteriaContributors.Count == 0)
            hysteriaSpreadTimer = baseHysteriaSpreadTime;

        if (!hasHysteria && hysteriaContributors.Count > 0 && hysteriaSpreadTimer > 0) {
            hysteriaSpreadTimer -= Time.deltaTime;

            if (hysteriaSpreadTimer <= 0) {
                ApplyHysteria(baseHysteriaDamage, baseHysteriaDuration, baseHysteriaSpreadTime, baseHysteriaSpreadRadius, hysteriaTriggerPrefab);
                if (gameObject.CompareTag("Enemy")) {
                    GetComponent<Health>().TakeDoT(baseHysteriaDamage, baseHysteriaDuration, gameObject);
                }
            }
        }
    }
    #endregion

    #region Disorient
    public void ApplyDisorient(float duration)
    {
        if (disorientDuration < duration)
            disorientDuration = duration;

        isDisoriented = true;

        if (enemyAI) {
            StartCoroutine(enemyAI.Wander());
        }
    }

    void HandleDisorient()
    {
        if (disorientDuration > 0) {
            disorientDuration -= Time.deltaTime;

            if (disorientDuration <= 0)
                isDisoriented = false;
        }
    }
    #endregion
}
