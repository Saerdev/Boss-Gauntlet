using UnityEngine;
using System.Collections;

public class Burn : StatusEffect {

    public const float DAMAGE_PER_SECOND = 200;

    private float damageDealt;

    public Burn(float baseDuration, Health targetHealth) : base(baseDuration, targetHealth) {
        this.targetHealth = targetHealth;
        damageDealt = 0;

        effectType = EffectType.Debuff;
    }

    public override void Tick()
    {
        remainingDuration -= Time.deltaTime;

        if (damageDealt >= DAMAGE_PER_SECOND * baseDuration) {
            End();
            return;
        }
        else if (damageDealt + DAMAGE_PER_SECOND * Time.deltaTime >= DAMAGE_PER_SECOND * baseDuration) {
            targetHealth.TakeDoT(DAMAGE_PER_SECOND * baseDuration - damageDealt);
            targetHealth.currentHealth = Mathf.Round(targetHealth.currentHealth);
            End();
            return;
        }
        
        targetHealth.TakeDoT(DAMAGE_PER_SECOND * Time.deltaTime);
        damageDealt += DAMAGE_PER_SECOND * Time.deltaTime;
    }
}
