using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerLevelsDatabase {
    public static Dictionary<int, int> levelExp = new Dictionary<int, int>() {
        {1, 0 },
        {2, 100 },
        {3, 250 },
        {4, 600 }
    };

    //void Awake()
    //{
    //    levelExp = new Dictionary<int, int>() {
    //        {1, 100 },
    //        {2, 250 },
    //        {3, 600 }
    //    };
    //}
}
