using UnityEngine;
using System.Collections;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class WaveMakeup : ScriptableObject {

    [System.Serializable]
    public class Types {
        public GameObject type;
        public int spawnWeight = 1;
    }

    [SerializeField]
    public AnimationCurve spawnRate;

    [System.Serializable]
    public class WaveComposition {

        [SerializeField]
        public Types[] enemyTypes;
        public int totalQuantity;
    }

    public float spawnsPerSecond = 1;

    [SerializeField]
    //[Header("Waves will be spawned in order of", order = 0)]
    //[Header("the elements (e.g. 3 of element 0", order = 1)]
    //[Header("followed by 2 of element 1, etc.)", order = 2)]
    public WaveComposition waveComposition;


#if UNITY_EDITOR
    [MenuItem("Create/Wave")]
    public static void CreateAsset()
    {
        _CreateAsset();
    }

    public static void _CreateAsset()
    {
        WaveMakeup asset = ScriptableObject.CreateInstance<WaveMakeup>();

        //string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        //if (path == "") {
        //    path = "Assets";
        //}
        //else if (Path.GetExtension(path) != "") {
        //    path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
        //}

        string path = "Assets/Resources/Waves";

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/Wave 1" + ".asset");

        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;

        asset.spawnRate.AddKey(0, 1);
        asset.spawnRate.AddKey(10, 3);
    }
#endif
}
