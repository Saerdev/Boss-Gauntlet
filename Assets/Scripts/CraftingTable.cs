using UnityEngine;
using System.Collections;
using System;

public class CraftingTable : Interactable {

    public GameObject craftingWindow;

    void Awake()
    {
        craftingWindow = GameObject.Find("CraftingWindow");
    }

    // Use this for initialization
    void Start()
    {
        CraftingWindow.isOpen = false;
        craftingWindow.SetActive(false);
    }

    public override void Interact()
    {
        CraftingWindow.isOpen = true;
        craftingWindow.SetActive(true);

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            print("Press 'E' to open crafting window");
        }
    }

    public void OnTriggerExit(Collider other)
    {
        CraftingWindow.isOpen = false;
        craftingWindow.SetActive(false);
    }
}
