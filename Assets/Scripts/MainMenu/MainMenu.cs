using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public static MainMenu instance;

    public GameObject saveSlot;

    private GameObject savedGamesWindow, savedGamesWindowContent;
    public GameObject selectedSlot;
    private string saveName;
    private int savedCount;

    void Awake()
    {
        instance = this;

        savedGamesWindow = GameObject.Find("SavedGamesWindow");
        savedGamesWindowContent = GameObject.Find("SavedGames");
        GetSavedGames();

        if (savedCount > 4) {
            RectTransform curRect = savedGamesWindowContent.GetComponent<RectTransform>();
            savedGamesWindowContent.GetComponent<RectTransform>().offsetMin = new Vector2(curRect.offsetMin.x, curRect.offsetMin.y - (savedCount - 4) * 50);
        }

        savedGamesWindow.SetActive(false);
    }

    public void CharacterSelect()
    {
        SceneManager.LoadScene("CharacterSelect");
    }

    public void ShowSavedGames()
    {
        savedGamesWindow.SetActive(!savedGamesWindow.activeInHierarchy);
    }

    public void GetSavedGames()
    {
        foreach (string file in Directory.GetFiles("Saves")) {
            if (file.EndsWith(".save") && !file.EndsWith("Objects.save")) {
                int first, last;
                first = file.IndexOf("Saves/") + "Saves/".Length + 1;
                last = file.LastIndexOf(".");
                saveName = file.Substring(first, last - first);

                GameObject slot = Instantiate(saveSlot) as GameObject;
                slot.transform.SetParent(savedGamesWindowContent.transform);
                slot.GetComponentInChildren<Text>().text = saveName;
                savedCount++;
            }
        }
    }

    public void LoadGame(GameObject selectedSlot)
    {
        if (this.selectedSlot == selectedSlot) {
            SaveManager.Instance.saveName = selectedSlot.GetComponentInChildren<Text>().text;
            SaveManager.Instance.LoadData();
        }
        else {
            this.selectedSlot = selectedSlot;
        }
    }
}
