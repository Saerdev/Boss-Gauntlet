﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FloatingDamageNumbers : MonoBehaviour {

    public float scrollSpeed = 1;
    public float fadeSpeed = 1;

    private Text text;

    void Start()
    {
        text = GetComponent<Text>();
        Destroy(gameObject, 3);
    }

	void Update () {
        transform.position += Vector3.up * scrollSpeed * Time.deltaTime;
        text.color = Color.Lerp(text.color, new Color(text.color.r, text.color.g, text.color.b, 0), fadeSpeed * Time.deltaTime);
	}

    public void SetColor(Color desiredColor)
    {
        text.color = desiredColor;
    }

}
