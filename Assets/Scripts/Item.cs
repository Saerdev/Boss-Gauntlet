using UnityEngine;
using System.Collections;
using System;

[Serializable]
public struct Ingredient {
    public Item ingredient;
    public int quantity;
    [HideInInspector]
    public bool haveMaterials;

    public Ingredient(Item ingredient, int quant, bool haveMats = false)
    {
        this.ingredient = ingredient;
        quantity = quant;
        haveMaterials = haveMats;
    }
}

[Serializable]
public struct Recipe {
    public int outputQuantity;
    public Ingredient[] ingredients;
}

public class Item : MonoBehaviour, IEquatable<Item> {

    public string itemName;
    public int quantity;
    [Range(0, 1000)]
    public int maxStackSize = 1000;
    public int itemID;
    public Sprite itemIcon;
    public string itemDescription;
    public UpgradeType upgradeType;

    public Recipe recipe;

    public enum ItemType {
        Consumable
    }

    public enum UpgradeType {
        None,
        Weapon,
        Armor,
        Utility
    }

    //public Item(string name, int quantity, int ID, Sprite icon = null, string description = "")
    //{
    //    itemName = name;
    //    this.quantity = quantity;
    //    itemID = ID;
    //    itemIcon = icon;
    //    itemDescription = description;
    //}

    public bool Equals(Item other)
    {
        return this.itemName == other.itemName;
    }
}
