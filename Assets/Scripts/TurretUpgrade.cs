using UnityEngine;
using System.Collections;

public class TurretUpgrade : MonoBehaviour {

    [HideInInspector]
    public TurretAI turretReference;
    [HideInInspector]
    public TurretInvReference lastTurretInvReference;
}
