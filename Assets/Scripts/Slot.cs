using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IKeyListener, IDropHandler, IPointerDownHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {

    public DragHandler item
    {
        get { return gameObject.GetComponentInChildren<DragHandler>(); }
    }

    public void OnDrop(PointerEventData eventData)
    {
        // If slot is empty, put dragged item in
        if (!item && DragHandler.draggedItem) {
            // If placing into turret inventory
            if (transform.parent.name == "TurretInventory") {
                // and item type matches slot type
                if ((transform.name == "WeaponSlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Weapon) ||
                    (transform.name == "ArmorSlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Armor) ||
                    (transform.name == "UtilitySlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Utility)) {
                    TurretInvReference reference = transform.parent.GetComponent<TurretInvReference>();
                    DragHandler.draggedItem.transform.SetParent(transform);
                    DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
                    DragHandler.dragging = false;
                    DragHandler.canvasGroup.blocksRaycasts = true;
                    DragHandler.draggedItem = null;
                    Inventory.eOnRefreshed();
                    reference.turretInventory.Refresh();
                }
                // cancel if item type doesn't match slot type
                else {
                    DragHandler.draggedItem.transform.position = DragHandler.startPos;
                    DragHandler.draggedItem.transform.SetParent(DragHandler.startParent);
                    DragHandler.canvasGroup.blocksRaycasts = true;
                    DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
                    DragHandler.dragging = false;
                    DragHandler.draggedItem = null;
                    return;
                }
            }
            else {
                DragHandler.draggedItem.transform.SetParent(transform);
                if (DragHandler.draggedItem.GetComponentInChildren<TurretUpgrade>()) {
                    DragHandler.draggedItem.GetComponentInChildren<TurretUpgrade>().lastTurretInvReference.turretInventory.Refresh();
                }
                DragHandler.draggedItem = null;
                Inventory.eOnRefreshed();
            }
        }
        // If slot is occupied, swap with dragged item
        else {
            if (DragHandler.draggedItem) {
                if (transform.parent.name == "TurretInventory") {
                    if ((transform.name == "WeaponSlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Weapon) ||
                        (transform.name == "ArmorSlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Armor) ||
                        (transform.name == "UtilitySlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Utility)) {
                        DragHandler.draggedItem.transform.SetParent(transform);
                        DragHandler temp1 = item;
                        temp1.transform.SetParent(DragHandler.startParent);
                        DragHandler.draggedItem = null;
                        Inventory.eOnRefreshed();
                        transform.parent.GetComponent<TurretInvReference>().turretInventory.Refresh();
                    }
                    else {
                        DragHandler.draggedItem.transform.position = DragHandler.startPos;
                        DragHandler.draggedItem.transform.SetParent(DragHandler.startParent);
                        DragHandler.canvasGroup.blocksRaycasts = true;
                        DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
                        DragHandler.dragging = false;
                        DragHandler.draggedItem = null;
                        return;
                    }
                }
                DragHandler.draggedItem.transform.SetParent(transform);
                DragHandler temp = item;
                temp.transform.SetParent(DragHandler.startParent);
                DragHandler.draggedItem = null;
                Inventory.eOnRefreshed();
            }
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        if (ObjectPlacer.instance.usedClick) {
            ObjectPlacer.instance.usedClick = false;
            return true;
        }
        return false;
    }

    // Required for OnPointerDown to work
    public void OnPointerClick(PointerEventData eventData) { }

    public void OnPointerDown(PointerEventData eventData)
    {
        // Left click
        if (eventData.pointerId == -1) {
            // If slot is empty, put dragged item in
            if (!item && DragHandler.draggedItem) {
                if (transform.parent.name == "TurretInventory") {
                    if ((transform.name == "WeaponSlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Weapon) ||
                        (transform.name == "ArmorSlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Armor) ||
                        (transform.name == "UtilitySlot" && DragHandler.draggedItem.GetComponentInChildren<Item>().upgradeType == Item.UpgradeType.Utility)) {
                        TurretInvReference reference = transform.parent.GetComponent<TurretInvReference>();
                        DragHandler.draggedItem.transform.SetParent(transform);
                        DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
                        DragHandler.dragging = false;
                        DragHandler.canvasGroup.blocksRaycasts = true;
                        DragHandler.draggedItem = null;
                        Inventory.eOnRefreshed();
                        reference.turretInventory.Refresh();
                    }
                    else {
                        DragHandler.draggedItem.transform.position = DragHandler.startPos;
                        DragHandler.draggedItem.transform.SetParent(DragHandler.startParent);
                        DragHandler.canvasGroup.blocksRaycasts = true;
                        DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
                        DragHandler.dragging = false;
                        DragHandler.draggedItem = null;
                        return;
                    }
                }
                else {
                    DragHandler.draggedItem.transform.SetParent(transform);
                    DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
                    DragHandler.dragging = false;
                    DragHandler.canvasGroup.blocksRaycasts = true;
                    DragHandler.draggedItem = null;
                    Inventory.eOnRefreshed();
                }
            }
            // If slot is occupied, swap with dragged item
            else {
                if (DragHandler.draggedItem) {
                    DragHandler.draggedItem.transform.SetParent(transform);
                    DragHandler temp = item;
                    temp.transform.SetParent(DragHandler.startParent);
                    DragHandler.dragging = false;
                    DragHandler.canvasGroup.blocksRaycasts = true;
                    DragHandler.draggedItem.GetComponent<LayoutElement>().ignoreLayout = false;
                    DragHandler.draggedItem = null;
                    Inventory.eOnRefreshed();
                }
            }
        }

        // Right click
        if (eventData.pointerId == -2 && !ObjectPlacer.placingObject) {
            if (item) {
                IUsable obj = item.GetComponentInChildren<IUsable>();
                if (obj != null) {
                    ObjectPlacer.instance.usedClick = true;
                    obj.Use();
                }
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!Draggable.draggingWindow)
            Draggable.canDrag = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Draggable.canDrag = true;
    }
}
