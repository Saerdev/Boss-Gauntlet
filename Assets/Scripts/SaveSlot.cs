using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class SaveSlot : MonoBehaviour, IPointerDownHandler{

    public void OnPointerDown(PointerEventData eventData)
    {
        MainMenu.instance.LoadGame(gameObject);
    }
}
