using UnityEngine;
using System.Collections;

public class PlaceableObject : MonoBehaviour, IUsable {

    public GameObject worldModel;

    void Start()
    {
        ObjectPlacer.instance.SetValid(true);

        if (!worldModel) {
            worldModel = gameObject;
        }
    }

    public virtual void Use()
    {
        // Set object to the "Placing" layer so it doesn't collide with anything but the ground
        gameObject.layer = 12;
        ObjectPlacer.instance.PlaceObject(worldModel);
    }

    void OnTriggerEnter(Collider other)
    {
        ObjectPlacer.instance.SetValid(false);
    }

    void OnTriggerExit()
    {
        ObjectPlacer.instance.SetValid(true);
    }

}
