using UnityEngine;
using System.Collections;

public class SinkAndDestroy : MonoBehaviour {

    private Collider[] cols;

    void Awake()
    {
        cols = GetComponentsInChildren<Collider>();
    }

    public void  Fade()
    {
        Invoke("DisableColliders", 5);

        Destroy(gameObject, 7);
    }

    void DisableColliders()
    {
        for (int i = 0; i < cols.Length; i++) {
            cols[i].enabled = false;
        }
    }
}
