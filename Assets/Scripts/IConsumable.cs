using UnityEngine;
using System.Collections;

public interface IConsumable {

    void Use();
}
