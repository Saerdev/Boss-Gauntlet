using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.Collections;

// TODO: Use reflection to get/set references
public class PlayerPassiveTree : MonoBehaviour, IKeyListener {

    public static PlayerPassiveTree instance;

    public static float skillDmgModifier, turretDmgModifier;
    public int unallocatedPoints;

    //private Dictionary<int, TreeNode> skillTree, turretTree = new Dictionary<int, TreeNode>() {
    //    { 0, new TreeNode(0) },
    //    { 1,  new TreeNode(.1f) },
    //    { 2, new TreeNode(.1f) },
    //    { 3, new TreeNode(.1f) },
    //    { 4, new TreeNode(.1f) },
    //    { 5, new TreeNode(.4f) }
    //};

    public Dictionary<string, string[]> classSkillPairs, skillUpgradePairs = new Dictionary<string, string[]>();
    private Dictionary<string, Type> upgradeReferencePairs;
    public Dictionary<string, Sprite> skillIcons, upgradeIcons;

	public Dictionary<string, string> skillTooltips, upgradeDescriptions;

    private int skillTreePointer, turretTreePointer;

    private GameObject window, skillSelectionIndicator, upgradeSelectionIndicator;
    private GameObject player;

    private Text skillTab1, skillTab2, skillTab3, skillTab4, skillTab5, points, description,
                 upgradeName1, upgradeName2, upgradeName3, upgradeName4;
    private Image skillIcon, upgradeIcon1, upgradeIcon2, upgradeIcon3, upgradeIcon4;

    public static GameObject selectedSkill, selectedUpgrade;
    private int tabNumber;

    private string playerClass;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        Player.eOnLevelUp += GiveSkillPoint;

        InputManager.Instance.AttachListener(eGameAction.SKILLS, this);
        InputManager.Instance.AttachListener(eGameAction.EXIT, this);

        window = transform.GetChild(0).gameObject;

        PopulateDictionaries();
        GetReferences();
        SetReferences();

        window.gameObject.SetActive(false);
        upgradeSelectionIndicator.SetActive(false);

        SaveManager.eOnSave += SaveSkillPoints;
    }

    void PopulateDictionaries()
	{
		classSkillPairs = new Dictionary<string, string[]> () {
			{ "Bunker", new string[] { "Bunker", "Tether", "Minefield", "EMA", "Nuke" } },
			{ "Manipulator", new string[] { "Rewind", "Project Illusion", "Creeping Fear", "Wormhole", "Transcend" } }
		};

		skillUpgradePairs = new Dictionary<string, string[]> () {
			// Bunker
			{ "Bunker", new string[] { "Volatility", "", "", "" } },
			{ "Tether", new string[] { "", "", "", "", } },
			{ "Minefield", new string[] { "Cripple", "", "", "" } },
			{ "EMA", new string[] { "", "", "", "" } },
			{ "Nuke", new string[] { "", "", "", "" } },

			// Manipulator
			{ "Rewind", new string[] { "Slipstream", "Volatile Shift", "Time Sap", "Temporal Shift" } },
			{ "Project Illusion", new string[] { "Explosive Illusion", "Smooth Operator", "Insane Laughter", "Smoke and Mirrors" } },
			{ "Creeping Fear", new string[] { "Disorient", "Hysteria", "Edge of Madness", "Shared Pain" } },
			{ "Wormhole", new string[] { "Organic Deconstruction", "Supermassive", "Black Hole", "Exotic Matter" } },
			{ "Transcend", new string[] { "Matter", "Mind", "Space", "Time" } },
		};

        upgradeDescriptions = new Dictionary<string, string>()
        {
            // Manipulator - Rewind
            { "Slipstream", "Your rewind costs 50% less mana but only returns you 1 second into the past." },
            { "Volatile Shift", "Your rewind rips time apart at your location, dealing AoE damage at both your current and past location." },
            { "Time Sap", "Distort the time around you when you Rewind, causing enemies around you to be slowed at both your current and previous position." },
            { "Temporal Shift", "Rewind affects your turrets for 1 extra second and grants them 100% increased attack speed for 2 seconds." },

            // Manipulator - Project Illusion
            { "Explosive Illusion", "Your illusion explodes at the end of its duration, dealing <x> damage to enemies around it." },
            { "Smooth Operator" , "Your illusion now lasts 1 second, and you lose the ability to swap positions, but you can now use the skill without waiting for it to expire." },
            { "Insane Laughter" , "Your illusion laughs insanely, taunting nearby enemies into attacking it for its duration." },
            { "Smoke and Mirrors" , "Your illusions are 50% more effective.\n\n Base - 50% more pass through damage.\n\nExplosive Illusion - 50% increased damage + Radius\n\nInsane Laughter - 50% increased taunt radius\n\nSmooth Operator - 50% reduced mana cost." },

            // Manipulator - Creeping Fear
            { "Disorient" , "Creeping Fear now disorients enemies on application, causing them to stumble around for 2.5 seconds." },
            { "Hysteria" , "Creeping Fear can now spread to unaffected enemies if they stay near an affected enemy for 2.5 seconds." },
            { "Edge of Madness" , "Enemies killed while affected by Creeping Fear have a 7% chance of releasing an Illusion." },
            { "Shared Pain" , "Enemies hit by a turret while under the effects of Creeping Fear share 25% of that damage to nearby enemies." },

            // Manipulator - Wormhole
            { "Organic Deconstruction" , "Enemies take damage every time they pass through the Wormhole." },
            { "Supermassive" , "Creating a Wormhole on top of another Wormhole creates a supermassive wormhole, increasing the area of the entrance." },
            { "Black Hole" , "Your Wormhole now has a gravity well, pulling any enemies in range to the center." },
            { "Exotic Matter" , "Wormholes last an additional 4 second and retain temporary properties until expiration." },

            // Manipulator - Transcend
            { "Matter" , "While Transcend is active, your Illusions apply Creeping Fear when they pass through an enemy." },
            { "Mind" , "While under the affects of Transcend, Creeping Fear has doubled effectiveness. This includes augments." },
            { "Space" , "While under the affects Transcend, Wormhole also destroys projectiles that enter its gravity well." },
            { "Time" , "While Transcend is active, your Rewind does not cost mana." },
        };

		upgradeReferencePairs = new Dictionary<string, Type> () {
            // Manipulator - Rewind
            { "Slipstream", typeof(Slipstream) },
            { "Volatile Shift", typeof(VolatileShift) },
            { "Time Sap", typeof(TimeSap) },
            { "Temporal Shift", typeof(TemporalShift) },

			// Manipulator - Project Illusion
			{ "Explosive Illusion", typeof(ExplosiveIllusion) },
            { "Smooth Operator", typeof(SmoothOperator) },
			{ "Insane Laughter",  typeof(InsaneLaughter) },
            { "Smoke and Mirrors", typeof(SmokeAndMirrors) },

            // Manipulator - Creeping Fear
            { "Disorient", typeof(Disorient) },
            { "Hysteria", typeof(Hysteria) },
            { "Edge of Madness", typeof(EdgeOfMadness) },
            { "Shared Pain" , typeof(SharedPain) },

            // Manipulator - Wormhole
            { "Organic Deconstruction", typeof(OrganicDeconstruction) },
            { "Supermassive", typeof(Supermassive) },
			{ "Black Hole", typeof(BlackHole) },
            { "Exotic Matter", typeof(ExoticMatter) },

            // Manipualtor - Transcend
            { "Matter" , typeof(Matter) },
            { "Mind" , typeof(Mind) },
            { "Space" , typeof(Space) },
            { "Time", typeof(TTime) }
		};

		skillTooltips = new Dictionary<string, string> () {
			// Manipulator
			{ "Rewind", "Energy Cost: 15 \n\nRevert back to your position 2 seconds ago. Also sets your health to its value 2 seconds ago." },
			{ "Project Illusion", "Energy Cost: 8 \n\nCreate an illusion that travels forward for 1 second, dealing 10 damage to enemies that it passes through. Activate the skill again while its active to swap positions with the illusion." },
			{ "Creeping Fear", "Energy Cost: 5 \n\nCause enemies in the affected area to lose 1 health per second." }, 
			{ "Wormhole", "Energy Cost: 20 \n\nPlace a wormhole that transports enemies from the entrance to it's exit." },
			{ "Transcend", "Energy Cost: 40 \n\nWhile Transcend is active, your other skills have additional effects. Rewind costs no energy. Project Illusion pulses 24 Illusions every half second in a nova. Creeping Fear has double efficacy. Wormhole gains a suction effect on both ends that grows as more enemies enter the field." },
		};

        skillIcons = new Dictionary<string, Sprite>()
        {
            { "Placeholder", Resources.Load<Sprite>("SkillIcons/Placeholder") },

            // Manipulator
            { "Rewind", Resources.Load<Sprite>("SkillIcons/Rewind") },
            { "Project Illusion", Resources.Load<Sprite>("SkillIcons/Project Illusion") },
			{ "Creeping Fear", Resources.Load<Sprite>("SkillIcons/Creeping Fear") },
            { "Wormhole", Resources.Load<Sprite>("SkillIcons/Wormhole") },
			{ "Transcend", Resources.Load<Sprite>("SkillIcons/Transcend") },
        };

        upgradeIcons = new Dictionary<string, Sprite>()
        {
            // Manipulator - Rewind
            { "Slipstream", Resources.Load<Sprite>("SkillIcons/UpgradeIcons/Slipstream") },
            { "Volatile Shift", Resources.Load<Sprite>("SkillIcons/UpgradeIcons/Volatile Shift") },
			{ "Time Sap", Resources.Load<Sprite>("SkillIcons/UpgradeIcons/Time Sap") },


            // Manipulator - Transcend
            { "Matter", Resources.Load<Sprite>("SkillIcons/UpgradeIcons/Matter") },
            { "Mind", Resources.Load<Sprite>("SkillIcons/UpgradeIcons/Mind") },
            { "Space", Resources.Load<Sprite>("SkillIcons/UpgradeIcons/Space") },
            { "Time", Resources.Load<Sprite>("SkillIcons/UpgradeIcons/Time") },

        };
	}

    void GetReferences()
    {
        player = FindObjectOfType<Player>().gameObject;
        playerClass = player.name;

        skillTab1 = window.transform.FindChild("SkillTab1").GetComponentInChildren<Text>();
        skillTab2 = window.transform.FindChild("SkillTab2").GetComponentInChildren<Text>();
        skillTab3 = window.transform.FindChild("SkillTab3").GetComponentInChildren<Text>();
        skillTab4 = window.transform.FindChild("SkillTab4").GetComponentInChildren<Text>();
        skillTab5 = window.transform.FindChild("SkillTab5").GetComponentInChildren<Text>();

        skillSelectionIndicator = window.transform.FindChild("SkillSelectionIndicator").gameObject;
        upgradeSelectionIndicator = window.transform.FindChild("UpgradeSelectionIndicator").gameObject;

        points = window.transform.FindChild("SkillPointsText").GetComponentInChildren<Text>();
        skillIcon = window.transform.FindChild("SkillIcon").GetComponent<Image>();

        upgradeIcon1 = window.transform.FindChild("UpgradeIcon1").GetComponent<Image>();
        upgradeIcon2 = window.transform.FindChild("UpgradeIcon2").GetComponent<Image>();
        upgradeIcon3 = window.transform.FindChild("UpgradeIcon3").GetComponent<Image>();
        upgradeIcon4 = window.transform.FindChild("UpgradeIcon4").GetComponent<Image>();

        upgradeName1 = upgradeIcon1.GetComponentInChildren<Text>();
        upgradeName2 = upgradeIcon2.GetComponentInChildren<Text>();
        upgradeName3 = upgradeIcon3.GetComponentInChildren<Text>();
        upgradeName4 = upgradeIcon4.GetComponentInChildren<Text>();

        // Reflection
        //for (int i = 1; i < 5; i++) {
        //    this.GetType().GetField("upgradeName" + i).SetValue( = Type.GetType("upgradeIcon" + i).GetProperty("GetComponentInChildren<Text>()");
        //}

        if (playerClass == null) {
            print("Class not found!");
        }

        skillTab1.text = classSkillPairs[playerClass][0];
        skillTab2.text = classSkillPairs[playerClass][1];
        skillTab3.text = classSkillPairs[playerClass][2];
        skillTab4.text = classSkillPairs[playerClass][3];
        skillTab5.text = classSkillPairs[playerClass][4];

        description = window.transform.FindChild("UpgradeDescription").GetComponent<Text>();

        UpdatePointsText();
    }

    void SetReferences()
    {
        if (skillIcons.ContainsKey(classSkillPairs[playerClass][tabNumber]))
            skillIcon.sprite = skillIcons[classSkillPairs[playerClass][tabNumber]];
        else
            skillIcon.sprite = skillIcons["Placeholder"];

        upgradeIcon1.name = skillUpgradePairs[classSkillPairs[playerClass][tabNumber]][0];
        upgradeIcon2.name = skillUpgradePairs[classSkillPairs[playerClass][tabNumber]][1];
        upgradeIcon3.name = skillUpgradePairs[classSkillPairs[playerClass][tabNumber]][2];
        upgradeIcon4.name = skillUpgradePairs[classSkillPairs[playerClass][tabNumber]][3];

        if (upgradeIcons.ContainsKey(upgradeIcon1.name))
            upgradeIcon1.sprite = upgradeIcons[upgradeIcon1.name];
        else
            upgradeIcon1.sprite = skillIcons["Placeholder"];
        if (upgradeIcons.ContainsKey(upgradeIcon2.name))
            upgradeIcon2.sprite = upgradeIcons[upgradeIcon2.name];
        else
            upgradeIcon2.sprite = skillIcons["Placeholder"];
        if (upgradeIcons.ContainsKey(upgradeIcon3.name))
            upgradeIcon3.sprite = upgradeIcons[upgradeIcon3.name];
        else
            upgradeIcon3.sprite = skillIcons["Placeholder"];
        if (upgradeIcons.ContainsKey(upgradeIcon4.name))
            upgradeIcon4.sprite = upgradeIcons[upgradeIcon4.name];
        else
            upgradeIcon4.sprite = skillIcons["Placeholder"];

        upgradeName1.text = upgradeIcon1.name;
        upgradeName2.text = upgradeIcon2.name;
        upgradeName3.text = upgradeIcon3.name;
        upgradeName4.text = upgradeIcon4.name;

        description.text = "";

        switch (tabNumber) {
            case 0: skillSelectionIndicator.transform.position = skillTab1.transform.position;
                break;
            case 1:
                skillSelectionIndicator.transform.position = skillTab2.transform.position;
                break;
            case 2:
                skillSelectionIndicator.transform.position = skillTab3.transform.position;
                break;
            case 3:
                skillSelectionIndicator.transform.position = skillTab4.transform.position;
                break;
            case 4:
                skillSelectionIndicator.transform.position = skillTab5.transform.position;
                break;
        }

        // Remove previous skill references
        if (upgradeIcon1.gameObject.GetComponent<SkillReference>())
            Destroy(upgradeIcon1.gameObject.GetComponent<SkillReference>());

        if(upgradeIcon2.gameObject.GetComponent<SkillReference>())
            Destroy(upgradeIcon2.gameObject.GetComponent<SkillReference>());

        if(upgradeIcon3.gameObject.GetComponent<SkillReference>())
            Destroy(upgradeIcon3.gameObject.GetComponent<SkillReference>());

        if(upgradeIcon4.gameObject.GetComponent<SkillReference>())
            Destroy(upgradeIcon4.gameObject.GetComponent<SkillReference>());

        // Add matching references for upgrades to scripts
        upgradeIcon1.gameObject.AddComponent(upgradeReferencePairs[upgradeIcon1.name]);
        upgradeIcon2.gameObject.AddComponent(upgradeReferencePairs[upgradeIcon2.name]);
        upgradeIcon3.gameObject.AddComponent(upgradeReferencePairs[upgradeIcon3.name]);
        upgradeIcon4.gameObject.AddComponent(upgradeReferencePairs[upgradeIcon4.name]);
    }

    public void SelectSkill(int num)
    {
        tabNumber = num;
        selectedUpgrade = null;
        SetReferences();
        upgradeSelectionIndicator.SetActive(false);
    }

    public void SelectUpgrade(GameObject upgrade)
    {
        selectedUpgrade = upgrade;
        description.text = upgradeDescriptions[upgrade.name];
        upgradeSelectionIndicator.transform.position = upgrade.transform.position;
        upgradeSelectionIndicator.SetActive(true);
    }

    public void AllocatePoint()
    {
        if (unallocatedPoints > 0 && selectedUpgrade) {
            // Only subtract points if the skill wasn't already learned
            if (selectedUpgrade.GetComponent<SkillReference>().UpgradeSkill()) {
                unallocatedPoints--;
                UpdatePointsText();
            }
        }
    }

    public void GiveSkillPoint()
    {
        unallocatedPoints++;
        UpdatePointsText();
    }

    void UpdatePointsText()
    {
        points.text = "Points: " + unallocatedPoints.ToString();
    }

    void OnDestroy()
    {
        Player.eOnLevelUp -= GiveSkillPoint;

        InputManager.Instance.RemoveListener(eGameAction.SKILLS, this);
        InputManager.Instance.RemoveListener(eGameAction.EXIT, this);

        SaveManager.eOnSave -= SaveSkillPoints;
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.SKILLS) {
            if (window.activeInHierarchy && GameManager.eOnClosedWindow != null)
                GameManager.eOnClosedWindow(window);

            window.SetActive(!window.activeInHierarchy);

            if (window.activeInHierarchy)
                GameManager.AddOpenWindow(window);
            else
                GameManager.RemoveOpenWindow(window);

            return true;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void SaveSkillPoints()
    {
        SaveManager.Instance.GetListForScene().skillPoints = unallocatedPoints;
    }
}

//public class TreeNode {
//    public float modifier;
//    public bool isAllocated;

//    public TreeNode(float mod)
//    {
//        modifier = mod;
//    }
//}