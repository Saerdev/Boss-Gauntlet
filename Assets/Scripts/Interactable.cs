using UnityEngine;
using System.Collections;

public class Interactable : MonoBehaviour {

    [HideInInspector]
    public bool inRange;

    public Item harvestedItem;
    public int quantityPerUse, totalQuantity;

    public virtual void Interact() {
        if (harvestedItem) {
            Inventory.instance.AddItem(harvestedItem, quantityPerUse);
            totalQuantity--;

            if (totalQuantity == 0)
                GameManager.interactableObjects.Remove(this);
                Destroy(gameObject);
        }
    }
}
