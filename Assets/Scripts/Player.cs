using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[SelectionBase]
public class Player : MonoBehaviour, IKeyListener {

    public static Rigidbody rb;
    public float moveSpeed = 10;
    [HideInInspector]
    public float moveSpeedMultiplier;
    private bool canMove;

    private Animator anim;

    public GameObject nexusShotPrefab;
    public Transform shotSpawnLocation;
    public float range = 15;
    public float fireRate = 0.5f;
    public int shotDamage = 20;
    public float shotSpeed = 10;
    public float shotLifetime = 3;
    public float blastRadius = 5;
    public bool autoFire;
    //public bool piercingShots;
    //public LayerMask validTargets;

    public static RaycastHit cursorPos;

    private Vector3 mousePos;
    [HideInInspector]
    public Vector3 moveDir;

    private GameObject nexusTurret;
    private float lastFire;
    private NexusShot nexusShot;

    private Transform shotPool;

    public float xp;
    public int level = 1;
    private Image xpBar;

    public delegate void OnLooted(Item item);
    public static event OnLooted eOnLooted;

    public delegate void OnLevelUp();
    public static event OnLevelUp eOnLevelUp;

    void Awake()
    { 
        xpBar = GameObject.Find("XPBar").GetComponent<Image>();
        xpBar.transform.parent.GetComponent<GridLayoutGroup>().padding.left = Screen.width / 10;
        xpBar.transform.parent.GetComponent<GridLayoutGroup>().spacing = new Vector2(Screen.width / 10, 0);
    }

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.LEFTCLICK, this);
        InputManager.Instance.AttachListener(eGameAction.RIGHTCLICK, this);
        InputManager.Instance.AttachListener(eGameAction.INTERACT, this);
        InputManager.Instance.AttachListener(eGameAction.AUTOFIRE, this);

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        nexusShotPrefab.CreatePool(10);

        shotPool = GameObject.Find("ObjectPool").transform;

        lastFire = fireRate + 1;
        canMove = true;
        moveSpeedMultiplier = 1;

        UpdateXPBar();
    }

    void Update()
    {
        FaceCursor();

        if (canMove)
            Movement();

        HandleFiring();

        HandleSkills();
    }

    void Movement()
    {
        moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        moveDir.y = rb.velocity.y;
        rb.velocity = new Vector3(moveDir.x * moveSpeed, moveDir.y, moveDir.z * moveSpeed);
        moveDir = transform.InverseTransformDirection(moveDir);
        anim.SetFloat("zVel", moveDir.z);
        anim.SetFloat("xVel", moveDir.x);
    }

    void HandleFiring()
    {
        if (autoFire && !DragHandler.draggedItem && !BuildingManager.placingBuilding && !ObjectPlacer.placingObject && lastFire > fireRate) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayHit;
            // Do nothing if mouse is over a UI button-
            if (!GameManager.eventSystem.IsPointerOverGameObject() && !GameManager.draggingUI && Physics.Raycast(ray, out rayHit, Mathf.Infinity, (1 << 10))) {// | (1 << 5))) {
                BasicProjectile basicProjectile = nexusShotPrefab.Spawn(shotPool, transform.position + Vector3.up * 2 + transform.forward, Quaternion.identity).GetComponent<BasicProjectile>();
                basicProjectile.SetValues((rayHit.point - transform.position).normalized, shotSpeed, shotDamage, shotLifetime);

                lastFire = 0;
            }
        }

        lastFire += Time.deltaTime;
    }

    void HandleSkills()
    {
        // Reversal of Fortune
        //if (Input.GetKeyDown(KeyCode.Alpha1)) {
        //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;
        //    Collider[] targets;

        //    if (Physics.Raycast(ray, out hit, Mathf.Infinity, validTargets)) {
        //        targets = Physics.OverlapSphere(hit.point, 3, validTargets);

        //        foreach(Collider target in targets) {
        //            if (target.gameObject.layer != 10)
        //                target.transform.root.GetComponent<StatusEffects>().AddStatus(new MarkOfProtection(2, target.transform.root.GetComponent<Health>()));
        //        }
        //    }
        //}
    }

    void FaceCursor()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << 10)) {
            Vector3 lookPos = hit.point + Vector3.up * transform.position.y;
            lookPos.y = transform.position.y;

            rb.MoveRotation(Quaternion.LookRotation(lookPos - transform.position, Vector3.up));
        }
    }

    public void DisableMovement(float duration)
    {
        StartCoroutine(Root(duration));
    }

    IEnumerator Root(float duration)
    {
        canMove = false;
        yield return new WaitForSeconds(duration);
        canMove = true;
    }

    public void GiveXP(int amount)
    {
        xp += amount;

        if (level < PlayerLevelsDatabase.levelExp.Count) {
            UpdateXPBar();
            if (xp >= PlayerLevelsDatabase.levelExp[level + 1]) {
                if (eOnLevelUp != null)
                    eOnLevelUp();
                level++;
            }
        }
    }

    void UpdateXPBar()
    {
        xpBar.transform.localScale = new Vector3((xp - PlayerLevelsDatabase.levelExp[level]) / (PlayerLevelsDatabase.levelExp[level + 1] - PlayerLevelsDatabase.levelExp[level]), 1, 1);
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.AUTOFIRE)
            autoFire = !autoFire;

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        if (action == eGameAction.LEFTCLICK && !DragHandler.draggedItem && !BuildingManager.placingBuilding && !ObjectPlacer.placingObject && lastFire > fireRate) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayHit;
            // Do nothing if mouse is over a UI button-
            if (!GameManager.eventSystem.IsPointerOverGameObject() && !GameManager.draggingUI && Physics.Raycast(ray, out rayHit, Mathf.Infinity, (1 << 10))) {// | (1 << 5))) {
                BasicProjectile basicProjectile = nexusShotPrefab.Spawn(shotPool, transform.position + Vector3.up * 2 + transform.forward, Quaternion.identity).GetComponent<BasicProjectile>();
                basicProjectile.SetValues((rayHit.point - transform.position).normalized, shotSpeed, shotDamage, shotLifetime);

                lastFire = 0;
                return true;
            }
        }

        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.LEFTCLICK, this);
        InputManager.Instance.RemoveListener(eGameAction.RIGHTCLICK, this);
        InputManager.Instance.RemoveListener(eGameAction.INTERACT, this);
        InputManager.Instance.RemoveListener(eGameAction.AUTOFIRE, this);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Loot")) {
            if (eOnLooted != null)
                eOnLooted(other.transform.root.GetComponent<Item>());

            //Inventory.instance.AddItem(other.transform.root.GetComponent<Item>(), other.transform.root.GetComponent<Item>().quantity);

            Destroy(other.transform.root.gameObject);
        }
    }
}
