using UnityEngine;
using System.Collections.Generic;
using MovementEffects;

public class AoEFieldBomb : MonoBehaviour {

    public GameObject aoeField;
    private float damage, radius, duration, firingAngle, gravity;
    private LayerMask validTargets;

    private Vector3 targetPos, sourcePos;

    public void SetValues(Vector3 targetPos, Vector3 sourcePos, float firingAngle, float gravity, float damagePerSec, float fieldRadius, float duration, LayerMask validTargets)
    {
        damage = damagePerSec;
        radius = fieldRadius;
        this.duration = duration;
        this.validTargets = validTargets;
        this.targetPos = targetPos;
        this.sourcePos = sourcePos;
        this.firingAngle = firingAngle;
        this.gravity = gravity;

        Timing.RunCoroutine(_Update());
    }

    IEnumerator<float>  _Update()
    {
        Vector3 hitDestination = targetPos;
        float distance = Vector3.Distance(sourcePos, hitDestination);
        float initialVelocity = distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(initialVelocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(initialVelocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = distance / Vx;

        // Rotate projectile to face the target.
        transform.rotation = Quaternion.LookRotation(hitDestination - transform.position);

        float elapse_time = 0;

        while (elapse_time < flightDuration) {
            if (!gameObject)
                yield break;

            transform.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

            elapse_time += Time.deltaTime;

            yield return 0f;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground")) {
            Invoke("Activate", 0.5f);    
        }
    }

    void Activate()
    {
        GameObject field = (GameObject)Instantiate(aoeField, transform.position, aoeField.transform.rotation);
        field.GetComponent<AoEField>().SetValues(damage, radius, duration);

        Destroy(gameObject);
    }
}
