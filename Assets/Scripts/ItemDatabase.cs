using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class ItemDatabase : MonoBehaviour {

    public static ItemDatabase instance;
    public static Dictionary<string, Item> itemDatabase = new Dictionary<string, Item>();

    void Awake()
    {
        instance = this;

        foreach (Item item in Resources.LoadAll<Item>("Items")) {
            if (!itemDatabase.ContainsKey(item.itemName))
                itemDatabase.Add(item.itemName, item);
        }
    }

    //void OnValidate()
    //{
    //    foreach (Item item in Resources.LoadAll<Item>("Items")) {
    //        if (!itemDatabase.ContainsKey(item.itemName))
    //            itemDatabase.Add(item.itemName, item);
    //    }
    //    print(itemDatabase["Crystal"]);
    //}
}
