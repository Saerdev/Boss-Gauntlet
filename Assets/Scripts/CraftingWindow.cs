using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class CraftingWindow : MonoBehaviour, IKeyListener {

    public static CraftingWindow instance;
    private GameObject craftingSlotPrefab;
    public static CraftingSlot selectedSlot;
    public static bool isOpen;

    private int recipeCount;
    private GameObject content;

    public delegate void OnCrafted(GameObject item);
    public static OnCrafted eOnCrafted;

    private List<CraftingSlot> slots = new List<CraftingSlot>();

    void Awake()
    {
        craftingSlotPrefab = Resources.Load("Prefabs/UI/CraftingSlot") as GameObject;
    }

	// Use this for initialization
	void Start () {
        instance = this;

        InputManager.Instance.AttachListener(eGameAction.CRAFTING, this);
        InputManager.Instance.AttachListener(eGameAction.EXIT, this);
        //window = transform.GetChild(0).gameObject;

        
        isOpen = true;

        content = GameObject.Find("Content");
        foreach(CraftingSlot slot in content.GetComponentsInChildren<CraftingSlot>()) {
            slots.Add(slot);
        }

        foreach (KeyValuePair<string, Item> item in ItemDatabase.itemDatabase) {
            if (ItemDatabase.itemDatabase[item.Key].recipe.outputQuantity > 0) {
                AddRecipe(ItemDatabase.itemDatabase[item.Key]);
                recipeCount++;
            }
        }

        //AddRecipe(ItemDatabase.itemDatabase["Turret"]);

        if (recipeCount > 5) {
            RectTransform curRect = content.GetComponent<RectTransform>();
            content.GetComponent<RectTransform>().offsetMin = new Vector2(curRect.offsetMin.x, curRect.offsetMin.y - (recipeCount - 5) * 71);
        }

        //AddRecipe(ItemDatabase.itemDatabase["Ore"]);
        //AddRecipe(ItemDatabase.itemDatabase["Crystal"]);
        //AddRecipe(ItemDatabase.itemDatabase["ExplodingShot"]);
        isOpen = false;
        gameObject.SetActive(false);
    }

    public void Craft()
    {
        if (selectedSlot && selectedSlot.HasMaterials()) {
            if (eOnCrafted != null)
                eOnCrafted(selectedSlot.result.gameObject);

            foreach(Ingredient ingredient in selectedSlot.ingredients) {
                Inventory.instance.RemoveItem(ingredient.ingredient, ingredient.quantity);
            }
            if (selectedSlot.result.CompareTag("Turret")){
                if (!TurretDeploy.isUnlocked1)
                    TurretDeploy.instance.UnlockTurret(1);
                else {
                    TurretDeploy.instance.UnlockTurret(2);
                }
            }
            else
            StartCoroutine(DelayedAdd());
        }
        else if (selectedSlot && !selectedSlot.HasMaterials()) {
            print("Not enough materials!");
        }
    }

    // Wait for end of frame to allow removing items from inventory to finish before adding new ones
    IEnumerator DelayedAdd()
    {
        yield return new WaitForEndOfFrame();
        Inventory.instance.AddItem(selectedSlot.result, selectedSlot.result.recipe.outputQuantity);
        //if (selectedSlot.result.GetComponent<TurretInventory>()) {
        //    TurretInventory.globalID++;
        //}
    }

    public void AddRecipe(Item item)
    {
        slots.Add(null);

        // Check if recipe already exists in table
        foreach (CraftingSlot cSlot in slots) {
            if (cSlot && cSlot.result.itemName == item.itemName) {
                print("Crafting table already contains " + item.itemName);
                return;
            }
        }

        GameObject slot;
        // Add recipe to last slot
        for (int i = 0; i < slots.Count; i++) {
            if (!slots[i]) {
                slot = Instantiate(craftingSlotPrefab) as GameObject;
                slot.transform.SetParent(content.transform);
                slots[i] = slot.GetComponent<CraftingSlot>();
                slots[i].result = item;
                slots[i].name = item.itemName;
                return;
            }
        }
    }

    public void SelectSlot(CraftingSlot slot)
    {
        if (selectedSlot)
            selectedSlot.ChangeState(selectedSlot.HasMaterials() ? CraftingSlot.State.haveMaterials : CraftingSlot.State.unlocked);
        selectedSlot = slot;
        selectedSlot.ChangeState(CraftingSlot.State.selected);
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.CRAFTING) {
            if (gameObject.activeInHierarchy && GameManager.eOnClosedWindow != null)
                GameManager.eOnClosedWindow(gameObject);

            gameObject.SetActive(!gameObject.activeInHierarchy);

            if (gameObject.activeInHierarchy)
                GameManager.AddOpenWindow(gameObject);
            else
                GameManager.RemoveOpenWindow(gameObject);

            return true;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.CRAFTING, this);
        InputManager.Instance.RemoveListener(eGameAction.EXIT, this);
    }
}
