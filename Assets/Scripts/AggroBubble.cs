using UnityEngine;
using System.Collections;

public class AggroBubble : MonoBehaviour {

    private SphereCollider col;

    void Start()
    {
        col = GetComponent<SphereCollider>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Spawner")) {
            other.GetComponent<Spawner>().isActive = true;
        }

        if (other.CompareTag("Enemy")) {
            Enemy enemy = other.GetComponent<Enemy>();
            enemy.AddTarget(transform.root.gameObject);
            //health.eOnDefeated += enemy.RemoveTarget;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Spawner")) {
            other.GetComponent<Spawner>().isActive = false;
        }
    }

    public void OnDisable()
    {
        Collider[] cols = Physics.OverlapSphere(transform.position, col.radius, -1, QueryTriggerInteraction.Collide);
        for (int i = 0; i < cols.Length; i++) {
            if (cols[i].CompareTag("Spawner")) {
                cols[i].GetComponent<Spawner>().isActive = false;
            }
        }
    }
}
