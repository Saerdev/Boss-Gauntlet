using UnityEngine;
using System.Collections;

public class Shatter : MonoBehaviour {

    public GameObject fracturedCopy;
    [HideInInspector]
    private bool canShatter;

    public float camShakeAmt, camShakeDuration;

    private GameObject copy;

    private Rigidbody[] rbs;
    private UnityEngine.AI.NavMeshObstacle obstacle;

    void Awake()
    {
        obstacle = GetComponent<UnityEngine.AI.NavMeshObstacle>();
        rbs = fracturedCopy.GetComponentsInChildren<Rigidbody>();
    }

    public void DisableObstacle()
    {
        obstacle.enabled = false;
        canShatter = true;
    }

    public void _Shatter()
    {
        Destroy(gameObject);
        copy = Instantiate(fracturedCopy, transform.position, transform.rotation) as GameObject;

        for (int i = 0; i < rbs.Length; i++) {
            rbs[i].isKinematic = false;
        }

        copy.GetComponent<SinkAndDestroy>().Fade();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (canShatter && other.CompareTag("Enemy")) {
            _Shatter();
            CamShake.instance.ShakeCam(camShakeAmt, camShakeDuration);
        }
    }
}
