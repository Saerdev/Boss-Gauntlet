using UnityEngine;
using System.Collections;

public class BasicProjectile : MonoBehaviour {

    private Rigidbody rb;
    private float speed, damage, lifetime;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void SetValues(Vector3 normalizedDirection, float speed, float damage, float lifetime)
    {
        this.speed = speed;
        this.damage = damage;
        this.lifetime = lifetime;

        transform.rotation = Quaternion.LookRotation(Vector3.down, normalizedDirection);
        rb.velocity = normalizedDirection * speed;

        Invoke("Recycle", lifetime);
    }

    void Recycle()
    {
        gameObject.Recycle();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy")) {
            other.GetComponent<Health>().TakeDamage(damage);
            CancelInvoke("Recycle");
            gameObject.Recycle();
        }
    }
}
