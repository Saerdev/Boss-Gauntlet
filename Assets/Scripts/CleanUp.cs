using UnityEngine;
using System.Collections;

public class CleanUp : MonoBehaviour {
    public void OnEnable()
    {
        Invoke("Clean", 3.5f);
    }

    void Clean()
    {
        gameObject.Recycle();
    }
}
