using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public string text;

    public void OnPointerEnter(PointerEventData eventData)
    {
        TooltipManager.instance.ShowTooltip(text, eventData.pointerEnter);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        TooltipManager.instance.HideTooltip();
    }
}
