using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ObjectPlacer : MonoBehaviour, IKeyListener {

    public static ObjectPlacer instance;

    public LayerMask validLayers;

    private Camera mainCam;

    [HideInInspector]
    public GameObject placedObj;

    [HideInInspector]
    public static bool placingObject;

    public bool validPosition;
    private static RaycastHit rayHit;

    public Material placementMat;
    private List<Material> originalmats = new List<Material>();
    private MeshRenderer[] mRenderers;
    private List<bool> isTrigger = new List<bool>();

    private int originalLayer;

    private UnityEngine.AI.NavMeshObstacle obstacle;

    // OnPointer events don't consume Input.GetKey events, so this is used as a flag to prevent overlaps
    public bool usedClick;

    void Awake()
    {
        instance = this;

        mainCam = Camera.main;
    }

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.LEFTCLICK, this);
        InputManager.Instance.AttachListener(eGameAction.RIGHTCLICK, this);
    }

    void Update()
    {
        if (placingObject && placedObj) {
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out rayHit, Mathf.Infinity, validLayers)) {
                if (placedObj.CompareTag("Turret")) {
                    placedObj.transform.position = rayHit.point + Vector3.up * 3.5f;
                }
                else
                    placedObj.transform.position = rayHit.point;
            }
        }
    }

    public void SetValid(bool validity)
    {
        validPosition = validity;

        //if (validPosition) {
        //    for (int i = 0; i < mRenderers.Length; i++) {
        //        mRenderers[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        //        mRenderers[i].material.color = Color.green;
        //    }
        //}
        //else {
        //    for (int i = 0; i < mRenderers.Length; i++) {
        //        mRenderers[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        //        mRenderers[i].material.color = Color.red;
        //    }
        //}
    }

    // Upon using object
    public void PlaceObject(GameObject obj)
    {
        if (!placingObject) {
            placingObject = true;

            placedObj = Instantiate(obj);

            // Change its tag so enemies don't aggro it
            originalLayer = placedObj.layer;
            placedObj.layer = 12;

            // Disable all scripts
            MonoBehaviour[] comps = placedObj.GetComponentsInChildren<MonoBehaviour>();
            for (int j = 0; j < comps.Length; j++) {
                comps[j].enabled = false;
            }

            obstacle = placedObj.transform.root.GetComponentInChildren<UnityEngine.AI.NavMeshObstacle>();
            if (obstacle)
                obstacle.enabled = false;

            // Change name
            placedObj.name = obj.name;
            placedObj.transform.localScale = Vector3.one;

            // Change all colliders to triggers temporarily
            Collider[] cols = placedObj.GetComponentsInChildren<Collider>();
            isTrigger.Clear();
            for (int i = 0; i < cols.Length; i++) {
                isTrigger.Add(cols[i].isTrigger);
                cols[i].isTrigger = true;
            }
            mRenderers = placedObj.GetComponentsInChildren<MeshRenderer>();
            for (int i = 0; i < mRenderers.Length; i++) {
                originalmats.Add(mRenderers[i].material);
            }

            validPosition = true;
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        // Place down object
        if (action == eGameAction.LEFTCLICK && placingObject && validPosition) {

            Inventory.instance.RemoveItem(placedObj.GetComponentInChildren<Item>());

            MonoBehaviour[] comps = placedObj.GetComponentsInChildren<MonoBehaviour>();
            for (int j = 0; j < comps.Length; j++) {
                comps[j].enabled = true;
            }

            Collider[] cols = placedObj.GetComponentsInChildren<Collider>();
            for (int i = 0; i < cols.Length; i++) {
                cols[i].isTrigger = isTrigger[i];
            }


            //for (int i = 0; i < mRenderers.Length; i++) {
            //    mRenderers[i].material = originalmats[i];
            //}
            placedObj.layer = originalLayer;
            if (obstacle)
                obstacle.enabled = true;
            placedObj = null;
            return true;
        }

        if (action == eGameAction.RIGHTCLICK && placingObject && !usedClick) {
            Destroy(placedObj);
            placingObject = false;
            placedObj = null;
            return true;
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        if (action == eGameAction.LEFTCLICK && placingObject && validPosition) {
            placingObject = false;

            return true;
        }

        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.LEFTCLICK, this);
        InputManager.Instance.RemoveListener(eGameAction.RIGHTCLICK, this);
    }
}
