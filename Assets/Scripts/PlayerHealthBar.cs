using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour {

    public static PlayerHealthBar instance;

    private Health health;

    private Image healthBar;
    private Text healthText;

    void Awake()
    {
        if (instance == null)
            instance = this;

        if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        health = FindObjectOfType<Player>().GetComponent<Health>();
        healthBar = transform.GetChild(0).GetComponent<Image>();
        healthText = transform.GetChild(1).GetComponent<Text>();

        health.eOnDefeated += UpdateBarAfterDeath;
        health.eOnDamaged += UpdateBar;
    }

    public void UpdateBar(float damage = 0, GameObject target = null, GameObject source = null, System.Type skill = null)
    {
        healthBar.transform.localScale = new Vector3(health.currentHealth / health.maxHealth, 1, 1);
        healthText.text = health.currentHealth.ToString("N0") + " / " + health.maxHealth.ToString("N0");
    }

    void UpdateBarAfterDeath(GameObject entity)
    {
        healthBar.transform.localScale = new Vector3(health.currentHealth / health.maxHealth, 1, 1);
        healthText.text = health.currentHealth.ToString("N0") + " / " + health.maxHealth.ToString("N0");
    }

    void OnDestroy()
    {
        health.eOnDefeated -= UpdateBarAfterDeath;
        health.eOnDamaged -= UpdateBar;
    }
}
