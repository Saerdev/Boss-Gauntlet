using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(Action))]
public class ActionDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property,GUIContent label)
    {
        if (property.objectReferenceValue != null) {
            SerializedObject propObj = new SerializedObject(property.objectReferenceValue);
            SerializedProperty curWeight = propObj.FindProperty("currentWeight");
            SerializedProperty totalPotentialWeight = propObj.FindProperty("totalPotentialWeight");

            EditorGUI.PropertyField(new Rect(position.x, position.y, position.width * 0.65f, position.height),
                                    property, GUIContent.none);
            EditorGUI.LabelField(new Rect(position.width, position.y, position.width, position.height),
                                    totalPotentialWeight.floatValue.ToString("0.#"));
            EditorGUI.LabelField(new Rect(position.width - 30, position.y, position.width, position.height),
                                 curWeight.floatValue.ToString("0.#"));
        }
    }
}
