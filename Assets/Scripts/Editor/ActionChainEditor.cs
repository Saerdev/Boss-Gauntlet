using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

//[CustomEditor(typeof(ActionChain), true)]
public class ActionChainEditor : Editor {
    private ReorderableList list;

    private void OnEnable()
    {
        list = new ReorderableList(serializedObject,
                serializedObject.FindProperty("actions"),
                true, true, true, true);

        list.drawHeaderCallback = (Rect rect) => {
            EditorGUI.LabelField(rect, "Actions");
            EditorGUI.LabelField(new Rect(rect.width - 50, rect.y, rect.width, rect.height), "Cur  Total");
        };

        list.onAddCallback = (ReorderableList l) =>
        {
            var index = l.serializedProperty.arraySize;
            l.serializedProperty.arraySize++;
            l.index = index;
            var element = l.serializedProperty.GetArrayElementAtIndex(index);
            EmptyAction emptyAction = CreateInstance<EmptyAction>();
            element.objectReferenceValue = emptyAction;
            element.objectReferenceInstanceIDValue = emptyAction.GetInstanceID();
        };

        list.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            //rect.y += 2;
            //EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight),
            //  element.FindPropertyRelative("currentWeight").ToString());
            EditorGUI.PropertyField(rect, element);
            //EditorGUI.PropertyField(
            //    new Rect(rect.x + 60, rect.y, rect.width - 60 - 30, EditorGUIUtility.singleLineHeight),
            //    element.FindPropertyRelative("actionName"), GUIContent.none);
            //EditorGUI.PropertyField(
            //    new Rect(rect.x + rect.width - 30, rect.y, 30, EditorGUIUtility.singleLineHeight),
            //    element.FindPropertyRelative("actionName"), GUIContent.none);

        };
    }

    //private void DrawHeader(Rect rect)
    //{
    //    GUI.Label(rect, "Actions");
    //}

    //private void DrawElement(Rect rect, int index, bool active, bool focused)
    //{
    //    Action action = brain.actions[index];

    //    EditorGUI.BeginChangeCheck();
    //    action.actionName = EditorGUI.PropertyField(new Rect(rect.x, rect.y, 18, rect.height), )
    //}

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        DrawDefaultInspector();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }
}
