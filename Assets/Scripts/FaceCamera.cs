using UnityEngine;
using System.Collections;

public class FaceCamera : MonoBehaviour {

    private new Transform transform;
    private Transform mainCam;

    // Use this for initialization
    void Start()
    {
        transform = GetComponent<Transform>();
        mainCam = Camera.main.transform;
        //transform.LookAt(Camera.main.transform);
        //transform.rotation = Quaternion.Inverse(mainCam.rotation);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //if (!CompareRotations(transform.rotation, Camera.main.transform.rotation)) {
        transform.rotation = mainCam.rotation;    
        //transform.rotation = Quaternion.Inverse(mainCam.rotation);
        //}
    }

    bool CompareRotations(Quaternion a, Quaternion b)
    {
        return (a == b ? true : false);
    }
}
