using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TurretDeploy : MonoBehaviour, IKeyListener {

    public static TurretDeploy instance;

    public GameObject turretPrefab;
    public static bool isUnlocked1, isUnlocked2;
    public static bool isActive1, isActive2;
    public LayerMask obstaclesMask;
    [HideInInspector]
    public GameObject turret1, turret2;
    private TurretStats turret1Stats, turret2Stats;

    public delegate void OnDeployedTurret(int turret);
    public static OnDeployedTurret eOnDeployedTurret;

    public Color locked, available, deployed;
    private Image turret1UI, turret2UI;

    public bool devOverride;

    struct TurretStats {
        public float hp;
        public float shield;

        public TurretStats(float health, float shield)
        {
            hp = health;
            this.shield = shield;
        }
    }

    void Awake()
    {
        if (instance == null)
            instance = this;

        if (instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.TURRET1, this);
        InputManager.Instance.AttachListener(eGameAction.TURRET2, this);

        turret1UI = GameObject.Find("TurretIcon1Background").GetComponent<Image>();
        turret2UI = GameObject.Find("TurretIcon2Background").GetComponent<Image>();

        if (isUnlocked1)
            if (isActive1)
                turret1UI.color = deployed;
            else
                turret1UI.color = available;
        else
            turret1UI.color = locked;

        if (isUnlocked2)
            if (isActive2)
                turret2UI.color = deployed;
            else
                turret2UI.color = available;
        else
            turret2UI.color = locked;

        turret1Stats = new TurretStats(turretPrefab.GetComponent<Health>().maxHealth, turretPrefab.GetComponent<Health>().maxShield);
        turret2Stats = new TurretStats(turretPrefab.GetComponent<Health>().maxHealth, turretPrefab.GetComponent<Health>().maxShield);

        SaveManager.eOnSave += SaveTurretPositions;
    }

    void LockTurret(GameObject obj)
    {
        if (obj.name == "Turret 1") {
            isActive1 = false;
            isUnlocked1 = false;
            turret1UI.color = locked;
        }

        if (obj.name == "Turret 2") {
            isActive2 = false;
            isUnlocked2 = false;
            turret2UI.color = locked;
        }
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.TURRET1 && (isUnlocked1 || devOverride)) {
            if (!isActive1) {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;

                if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << 10, QueryTriggerInteraction.Ignore)) {
                    UnityEngine.AI.NavMeshHit navHit;
                    Collider[] obstacles = Physics.OverlapSphere(hitInfo.point, 1.4f, obstaclesMask.value, QueryTriggerInteraction.Ignore);
                    if (obstacles.Length == 0 && UnityEngine.AI.NavMesh.SamplePosition(hitInfo.point, out navHit, 0.5f, UnityEngine.AI.NavMesh.AllAreas)) {
                        turret1 = Instantiate(turretPrefab, hitInfo.point + Vector3.up * 3.5f, Quaternion.identity) as GameObject;
                        turret1.GetComponent<TurretAI>().deployedIndex = 1;
                        turret1.GetComponent<TurretInventory>().ID = 1;
                        Health health = turret1.GetComponent<Health>();
                        health.currentHealth = turret1Stats.hp;
                        health.currentShield = turret1Stats.shield;
                        health.eOnDefeated += LockTurret;
                        turret1.name = "Turret 1";
                        isActive1 = true;
                        turret1UI.color = deployed;

                        if (eOnDeployedTurret != null)
                            eOnDeployedTurret(1);
                    }
                }
            }
            else {
                isActive1 = false;
                turret1Stats = new TurretStats(turret1.GetComponent<Health>().currentHealth, turret1.GetComponent<Health>().currentShield);
                turret1UI.color = available;
                Destroy(turret1);
            }
        }

        if (action == eGameAction.TURRET2 && (isUnlocked2 || devOverride)) {
            if (!isActive2) {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;

                if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, 1 << 10, QueryTriggerInteraction.Ignore)) {
                    UnityEngine.AI.NavMeshHit navHit;
                    Collider[] obstacles = Physics.OverlapSphere(hitInfo.point, 1.4f, obstaclesMask.value, QueryTriggerInteraction.Ignore);
                    if (obstacles.Length == 0 && UnityEngine.AI.NavMesh.SamplePosition(hitInfo.point, out navHit, 0.5f, UnityEngine.AI.NavMesh.AllAreas)) {
                        turret2 = Instantiate(turretPrefab, hitInfo.point + Vector3.up * 3.5f, Quaternion.identity) as GameObject;
                        turret2.GetComponent<TurretAI>().deployedIndex = 2;
                        turret2.GetComponent<TurretInventory>().ID = 2;
                        Health health = turret2.GetComponent<Health>();
                        health.currentHealth = turret2Stats.hp;
                        health.currentShield = turret2Stats.shield;
                        health.eOnDefeated += LockTurret;
                        turret2.name = "Turret 2";
                        isActive2 = true;
                        turret2UI.color = deployed;

                        if (eOnDeployedTurret != null)
                            eOnDeployedTurret(2);
                    }
                }
            }
            else {
                isActive2 = false;
                turret2Stats = new TurretStats(turret2.GetComponent<Health>().currentHealth, turret2.GetComponent<Health>().currentShield);
                turret2UI.color = available;
                Destroy(turret2);

            }
        }

        return false;
    }

    public void UnlockTurret(int num)
    {
        if (num == 1) {
            isUnlocked1 = true;
            turret1UI.color = available;
        }

        if (num == 2) {
            isUnlocked2 = true;
            turret2UI.color = available;
        }
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void SaveTurretPositions()
    {
        if (isActive1) {
            SaveHelper(1, turret1);
        }

        if (isActive2) {
            SaveHelper(2, turret2);
        }
    }

    void SaveHelper(int index, GameObject turret)
    {
        SavedTurret savedTurret = new SavedTurret();
        savedTurret.turretIndex = index;
        savedTurret.hp = turret.GetComponent<Health>().currentHealth;
        savedTurret.energyShield = turret.GetComponent<Health>().currentShield;
        savedTurret.posX = turret.transform.position.x;
        savedTurret.posY = turret.transform.position.y;
        savedTurret.posZ = turret.transform.position.z;

        SaveManager.Instance.GetListForScene().savedTurrets.Add(savedTurret);
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.TURRET1, this);
        InputManager.Instance.RemoveListener(eGameAction.TURRET2, this);

        SaveManager.eOnSave -= SaveTurretPositions;
    }
}
