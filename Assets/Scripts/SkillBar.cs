using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SkillBar : MonoBehaviour {

    private Image[] skills;
    private string playerClass;

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        skills = GetComponentsInChildren<Image>();

        playerClass = FindObjectOfType<Player>().name;
		//print (PlayerPassiveTree.Instance.classSkillPairs[playerClass][1]);

        for (int i = 1; i < skills.Length; i++) {
            if (skills[i].GetComponent<Tooltip>()) {
                skills[i].GetComponent<Tooltip>().text = PlayerPassiveTree.instance.skillTooltips[PlayerPassiveTree.instance.classSkillPairs[playerClass][i - 1]];
                if (PlayerPassiveTree.instance.skillIcons.ContainsKey(PlayerPassiveTree.instance.classSkillPairs[playerClass][i - 1]))
                    skills[i].sprite = PlayerPassiveTree.instance.skillIcons[PlayerPassiveTree.instance.classSkillPairs[playerClass][i - 1]];
                else
                    skills[i].sprite = PlayerPassiveTree.instance.skillIcons["Placeholder"];
            }
        }
    }
}
