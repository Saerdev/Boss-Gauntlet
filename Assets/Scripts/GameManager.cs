using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour, IKeyListener {

    public static GameManager instance;

    public GameObject healthBarCanvas;

    public static bool quittingGame;

    public static EventSystem eventSystem;

    private GameObject menu;

    public static List<Interactable> interactableObjects = new List<Interactable>();
    public static List<GameObject> openWindows = new List<GameObject>();

    public delegate void OnOpenedWindow(GameObject window);
    public static OnOpenedWindow eOnOpenedWindow;

    public delegate void OnClosedWindow(GameObject window);
    public static OnClosedWindow eOnClosedWindow;

    public static bool draggingUI;

    public GameObject playerClass;
    public Player player;
    public Vector3 spawnLocation;
    public bool isNewGame;

    public static DayNightCycler dayNightCycler;

    private GameObject foregroundUICanvas;
    public GameObject lootText;

    private Scene currentScene;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        quittingGame = false;

        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneLoaded += CheckIfNewGame;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        currentScene = scene;
        if (scene.name != "MainMenu" && scene.name != "CharacterSelect") {
            eventSystem = FindObjectOfType<EventSystem>();
            dayNightCycler = FindObjectOfType<DayNightCycler>();
            foregroundUICanvas = GameObject.Find("ForegroundUI");

            player = FindObjectOfType<Player>();
            Player.eOnLooted += SpawnLootText;

            menu = GameObject.Find("EscapeMenu");
            if (menu) {
                menu.transform.GetChild(0).FindChild("Main Menu").GetComponent<Button>().onClick.RemoveAllListeners();
                menu.transform.GetChild(0).FindChild("Main Menu").GetComponent<Button>().onClick.AddListener(Button_MainMenu);

                menu.transform.GetChild(0).FindChild("Exit Button").GetComponent<Button>().onClick.RemoveAllListeners();
                menu.transform.GetChild(0).FindChild("Exit Button").GetComponent<Button>().onClick.AddListener(Button_Exit);

                menu.SetActive(false);
            }

            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("ExplosionMarker")) {
                GlobalBlackboard.explosionCenters.Add(obj);
            }

            GlobalBlackboard.player = FindObjectOfType<Player>();
        }
    }

    void Start()
    {
        InputManager.Instance.AttachListener(eGameAction.EXIT, this);
    }

    void Update()
    {
        // Reload current scene
        if (Input.GetKeyDown(KeyCode.F3)) {
            SaveManager.Instance.isSceneBeingLoaded = false;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    void CheckIfNewGame(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "World 1" && isNewGame) {
            GameObject player = Instantiate(playerClass, spawnLocation, Quaternion.identity) as GameObject;
            // Remove "(clone)" from name
            player.name = player.name.Remove(player.name.Length - 7);
            isNewGame = false;
        }
    }

    public static void AddOpenWindow(GameObject window)
    {
        openWindows.Add(window);
        if (eOnOpenedWindow != null)
            eOnOpenedWindow(window);
    }

    public static void RemoveOpenWindow(GameObject window)
    {
        openWindows.Remove(window);
    }

    public void OnApplicationQuit()
    {
        if (!Application.isEditor)
            SaveManager.Instance.SaveData();
        quittingGame = true;
    }

    void SpawnLootText(Item item)
    {
        GameObject loot = Instantiate(lootText);
        loot.GetComponent<Text>().text += "[<color=magenta>" + item.itemName + "</color>]";
        loot.transform.SetParent(foregroundUICanvas.transform);
        //loot.GetComponent<RectTransform>().anchoredPosition = 
        Destroy(loot, 2.5f);
    }

    public static float DistanceSqrd(Vector3 a, Vector3 b)
    {
        return (a - b).sqrMagnitude;
    }

    //void LockupPrevention()
    //{
    //    //Lock-up prevention
    //    if ((1f / Time.deltaTime) < 60) {
    //        print("FPS dropped below threshold (at " + 1f / Time.deltaTime + ")");
    //        UnityEditor.EditorApplication.isPlaying = false;
    //        Application.Quit();
    //    }
    //}

    private void AttachHealthBar(GameObject obj)
    {
        GameObject canvas = (GameObject)Instantiate(healthBarCanvas, obj.transform.position + Vector3.up * 5, healthBarCanvas.transform.rotation);
        canvas.transform.SetParent(obj.transform.root);
        canvas.name = "HealthBarCanvas";
    }

    public void Button_MainMenu()
    {
        SaveManager.Instance.SaveData();
        SceneManager.LoadScene("MainMenu");
    }

    public void Button_Exit()
    {
        print("Exiting");
        //SaveManager.Instance.SaveData();
        Application.Quit();
    }

    public bool OnKeyDown(eGameAction action)
    {
        if (action == eGameAction.EXIT) {
            if (openWindows.Count > 0) {
                if (eOnClosedWindow != null)
                    eOnClosedWindow(openWindows[openWindows.Count - 1]);

                openWindows[openWindows.Count - 1].SetActive(false);
                openWindows.RemoveAt(openWindows.Count - 1);


                return true;
            }
            else if (currentScene.name == "CharacterSelect") {
                SceneManager.LoadScene("MainMenu");
            }
            else { 
                menu.SetActive(!menu.activeInHierarchy);

                return true;
            }
        }

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.EXIT, this);
    }
}
