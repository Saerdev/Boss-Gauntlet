using UnityEngine;
using System.Collections;

public class SuicideBomber : Enemy {
    
    protected override void DamageTarget(float damage)
    {
        base.DamageTarget(damage);

        Destroy(gameObject);
    }
}
