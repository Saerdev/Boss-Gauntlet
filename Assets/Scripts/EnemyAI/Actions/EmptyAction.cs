using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "EmptyAction", menuName = "Action/EmptyAction", order = 10)]
public class EmptyAction : Action {
    public override float CalculateWeight(Brain brain)
    {
        return 0;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        return 0;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        yield return 0f;
    }
}
