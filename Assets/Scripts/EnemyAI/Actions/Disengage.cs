using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Disengage", menuName = "Action/Disengage", order = 0)]
public class Disengage : Action {

//    public Threshold belowHealth;
    public Bool explosionOnField, losToAnyExplosion;

	public Curve health;

    public override void Execute(Brain brain)
    {
        brain.topBrain.engageState = Brain.EngageState.Disengaging;

        brain.engageBrain.enabled = false;
        brain.disengageBrain.enabled = true;
    }

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

//        if (brain.health.currentHealth < belowHealth.threshold)
//            curWeight += belowHealth.flatWeight;

		float min = 0, max = 0;

		GetMinMaxInCurve (health, out min, out max);

		curWeight += Mathf.Clamp (health.curve.Evaluate (brain.health.currentHealth / brain.health.maxHealth * 100), min, max);

        if (GlobalBlackboard.explosionCenters.Count > 0)
            curWeight += explosionOnField.flatWeight;

        RaycastHit hit;
        foreach (GameObject explosion in GlobalBlackboard.explosionCenters) {
            if (Physics.Raycast(brain.transform.position + Vector3.up, explosion.transform.position - brain.transform.position, out hit, Mathf.Infinity, losToAnyExplosion.losMask, QueryTriggerInteraction.Collide)) {
                if (hit.collider.CompareTag("ExplosionMarker")) {
                    curWeight += losToAnyExplosion.flatWeight;
                    break;
                }
            }
        }

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

		totalWeight += GetMaxWeightInCurve (health);
        totalWeight += losToAnyExplosion.flatWeight;
        totalWeight += explosionOnField.flatWeight;

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        yield return 0f;
    }
}
