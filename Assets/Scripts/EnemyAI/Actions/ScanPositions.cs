using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ScanPositions : Action {

    public float samplingRange = 12f;
    public float samplingDensity = 1.5f;
    [ReadOnly]

    public GameObject scanPointMarker;
    private GameObject markersParent;

    public override void Execute(Brain brain)
    {
        float halfSamplingRange = samplingRange * 0.5f;
        Vector3 pos = brain.transform.position;

        brain.topBrain.sampledPositions.Clear();

        for (float x = -halfSamplingRange; x < halfSamplingRange; x += samplingDensity) {
            for (float z = -halfSamplingRange; z < halfSamplingRange; z += samplingDensity) {
                Vector3 p = new Vector3(pos.x + x, 0f, pos.z + z);

                UnityEngine.AI.NavMeshHit hit;
                if (UnityEngine.AI.NavMesh.SamplePosition(p, out hit, samplingDensity * 0.5f, UnityEngine.AI.NavMesh.AllAreas)) {
                    brain.topBrain.AddSampledPosition(hit.position + Vector3.up);
                }
            }
        }
    }

    protected override void OnValidate()
    {
        base.OnValidate();

        GlobalBlackboard.sampleCount = (int)Mathf.Pow(samplingRange / samplingDensity, 2);
    }

    public override float CalculateWeight(Brain brain)
    {
        return 1f;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        return 0f;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        yield return 0f;
    }
}
