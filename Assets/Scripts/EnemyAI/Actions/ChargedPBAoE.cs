using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MovementEffects;

[CreateAssetMenu(fileName = "ChargedPBAoE", menuName = "Action/ChargedPBAoE")]
public class ChargedPBAoE : Action {

    public GameObject skillFX;

    public float chargeTime, damage, radius, cooldown, chargeCamShakeAmt, explosionCamShakeAmt, explosionCamShakeDuration;
    public LayerMask validTargets, losMask;

    public Curve proximityToPlayer, health;

    private float cooldownTimer;

    public override void Init(Brain brain = null)
    {
        cooldownTimer = -cooldown;
    }

    public override float CalculateWeight(Brain brain)
    {
        if (Time.time - cooldownTimer > cooldown) {
            onCooldown = false;
        }
        else {
            onCooldown = true;
            return 0f;
        }

        float curWeight = 0;

        float min, max;
        GetMinMaxInCurve(proximityToPlayer, out min, out max);

        curWeight += proximityToPlayer.curve.Evaluate(Mathf.Clamp(Vector3.Distance(brain.transform.position, brain.target.transform.position), min, max));
        currentWeight += health.curve.Evaluate(Mathf.Clamp(brain.health.currentHealth / brain.health.maxHealth * 100, min, max));

        currentWeight = curWeight;

        return curWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        cooldownTimer = Time.time;
        brain.canCancel = false;
        brain.agent.Stop();
        brain.anim.SetBool("Run", false);
        brain.anim.SetTrigger("Stretch");
        CamShake.instance.ShakeCam(chargeCamShakeAmt, chargeTime);

        GameObject skillFx = (GameObject)Instantiate(skillFX, brain.transform.position + Vector3.up * 5, brain.transform.rotation);
        Destroy(skillFx, chargeTime + 3);
        yield return Timing.WaitForSeconds(chargeTime);
        CamShake.instance.ShakeCam(explosionCamShakeAmt, explosionCamShakeDuration);

        Collider[] cols = Physics.OverlapSphere(brain.transform.position, radius, validTargets);

        for (int i = 0; i < cols.Length; i++) {
            if (cols[i].CompareTag("Player")) {
                RaycastHit hit;
                if (Physics.Raycast(brain.transform.position, cols[i].transform.position - brain.transform.position, out hit, radius, losMask, QueryTriggerInteraction.Ignore)) {
                    if (!hit.collider.CompareTag("Player"))
                        continue;
                }
            }
            cols[i].GetComponent<Health>().TakeDamage(damage);
        }
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

        totalWeight += GetMaxWeightInCurve(proximityToPlayer);
        totalWeight += GetMaxWeightInCurve(health);

        return totalWeight;
    }
}
