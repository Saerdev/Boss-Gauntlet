using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "LeapAttack", menuName = "Action/LeapAttack")]
public class LeapAttack : Action {

    public float damage, leapRange = 16, leapSpeed = 40, damageRadius = 4;
    public LayerMask validTargets, losMask;

    public Curve proximityToTarget, selfHealth;
    public Bool hasLosToTarget, targetFleeing;

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

        float min, max;
        GetMinMaxInCurve(proximityToTarget, out min, out max);
        curWeight +=  proximityToTarget.curve.Evaluate(Mathf.Clamp(Vector3.Distance(brain.transform.position, brain.target.transform.position), min, max));

        RaycastHit hit;
        if (Physics.Raycast(brain.transform.position, brain.target.transform.position - brain.transform.position, out hit, 100, losMask, QueryTriggerInteraction.Ignore)) {
            if (hit.collider.CompareTag("Player"))
                curWeight += hasLosToTarget.flatWeight;
        }

        GetMinMaxInCurve(selfHealth, out min, out max);
        curWeight += selfHealth.curve.Evaluate(Mathf.Clamp(brain.health.currentHealth / brain.health.maxHealth * 100, min, max));

        // Target fleeing check
        if (Vector3.Angle(brain.targetRb.velocity, brain.transform.position - brain.target.transform.position) > 110)
            curWeight += targetFleeing.flatWeight;

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

        totalWeight += GetMaxWeightInCurve(proximityToTarget);
        totalWeight += GetMaxWeightInCurve(selfHealth);
        totalWeight += hasLosToTarget.flatWeight;
        totalWeight += targetFleeing.flatWeight;

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        //attacking = true;
        brain.isExecutingAction = true;
        brain.canCancel = false;

        brain.anim.SetBool("Run", true);
        brain.anim.SetTrigger("LeapAttack");

        // Randomly lead the target
        //if (Random.Range(0, 2) == 1)
        if (brain.topBrain.engageState == Brain.EngageState.Engaging && brain.target) {
            Rigidbody targetRb = null;
            targetRb = brain.target.GetComponent<Rigidbody>();
            if (targetRb != null) {
                brain.agent.stoppingDistance = 0;

                // Check if fracturable obstacle is in the way
                RaycastHit hit;
                if (Physics.CapsuleCast(brain.transform.position, brain.transform.position + Vector3.up * brain.agent.height, brain.agent.radius, (brain.target.transform.position + targetRb.velocity) - brain.transform.position, out hit, 100, 1 << 16, QueryTriggerInteraction.Ignore)) {
                    if (hit.collider.CompareTag("Fractured")) {
                        hit.collider.transform.root.GetComponent<Shatter>().DisableObstacle();
                    }
                }

                brain.SetDestination(brain.target.transform.position + targetRb.velocity);
            }
        }

        yield break;
    }

    
}
