using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Idle", menuName = "Action/Idle")]
public class Idle : Action {

    public Bool isDisengaging, hasReachedTarget;

    public override void Execute(Brain brain)
    {
        //brain.SetDestination(brain.transform.position);
        brain.anim.SetBool("Run", false);
    }

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

        if (brain.topBrain.engageState == Brain.EngageState.Disengaging)
            curWeight += isDisengaging.flatWeight;

        if (brain.agent.remainingDistance < brain.agent.stoppingDistance)
            curWeight += hasReachedTarget.flatWeight;

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

        totalWeight += isDisengaging.flatWeight;
        totalWeight += hasReachedTarget.flatWeight;

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        yield return 0f;
    }
}
