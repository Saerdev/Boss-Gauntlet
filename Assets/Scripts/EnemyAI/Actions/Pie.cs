using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MovementEffects;

[CreateAssetMenu(fileName = "Pie", menuName = "Action/Pie")]
public class Pie : Action {

    public GameObject pieTelegraph;
    public float damage;
    //[Tooltip("Time for pie animation to finish")]
    //public float animCompleteTime;
    //[Tooltip("Time until damage is dealt (independent of animation)")]
    //public float detonationDelay;

    public AnimationCurve animationTime;

    [Tooltip("Percentage of max health")]
    public Curve underHealth;

    private Animator anim;
    private int useCount;

    public override void Init(Brain brain = null)
    {
        useCount = 0;
    }

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

        float healthPercentage = brain.health.currentHealth / brain.health.maxHealth * 100;
        curWeight += underHealth.curve.Evaluate(healthPercentage);

        currentWeight = curWeight;

        return curWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        brain.canCancel = false;
        brain.agent.Stop();

        GameObject telegraph = Instantiate(pieTelegraph, brain.transform.position, pieTelegraph.transform.rotation) as GameObject;
        telegraph.transform.Rotate(Vector3.forward, UnityEngine.Random.Range(0, 360));

        float animTime = animationTime.Evaluate(Mathf.Clamp(useCount, 0, GetMaxWeightInCurve(animationTime)));
        telegraph.GetComponentInChildren<Animator>().SetFloat("AnimSpeed", 1 / animTime);

        yield return Timing.WaitForSeconds(animTime + 0.1f);
        PieTrigger trigger = telegraph.GetComponentInChildren<PieTrigger>();
        trigger.SetValues(damage);
        trigger.trigger.SetActive(true);

        useCount++;

        brain.canCancel = true;
        brain.agent.Resume();
    }

    protected override float CalculateTotalPotentialWeight()
    {
        return totalPotentialWeight;
    }
}
