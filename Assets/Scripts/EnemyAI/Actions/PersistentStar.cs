using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MovementEffects;

[CreateAssetMenu(fileName = "PersistentStar", menuName = "Action/PersistentStar")]
public class PersistentStar : Action {

    public GameObject persistentPrefab;

    public float damagePerSec, cooldown, fieldRadius, duration, firingAngle, distance, gravity, projectileCount;
    public LayerMask validTargets;

    private float cooldownTimer;

    public override void Init(Brain brain = null)
    {
        cooldownTimer = -cooldown;
    }

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 500;

        if (Time.time - cooldownTimer > cooldown)
            onCooldown = false;
        else {
            onCooldown = true;
            return 0;
        }

        currentWeight = curWeight;

        return curWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        cooldownTimer = Time.time;

        brain.canCancel = false;
        brain.agent.Stop();
        brain.anim.SetBool("Run", false);

        brain.anim.SetTrigger("TossBomb");

        yield return Timing.WaitForSeconds(0.5f);

        for (int i = 0; i < projectileCount; i++) {
            // Evenly spaced on the circumference of a circle
            float angle = 360f / projectileCount * i;

            Vector3 hitDestination;
            hitDestination.x = brain.transform.position.x + distance * Mathf.Sin(angle * Mathf.Deg2Rad);
            hitDestination.z = brain.transform.position.z + distance * Mathf.Cos(angle * Mathf.Deg2Rad);
            hitDestination.y = brain.transform.position.y;

            GameObject bomb = (GameObject)Instantiate(persistentPrefab, brain.transform.position + Vector3.up * 4, Quaternion.identity);

            bomb.GetComponent<AoEFieldBomb>().SetValues(hitDestination, brain.transform.position, firingAngle, gravity, damagePerSec, fieldRadius, duration, validTargets);
        }

        yield return 0f;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        return 0f;
    }
}
