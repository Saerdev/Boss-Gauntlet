using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Run", menuName = "Action/Run")]
public class Run : Action {

    public Threshold underRangeToTarget;
    public Bool hasNotReachedTarget;

    public override void Execute(Brain brain)
    {
        brain.anim.SetBool("Run", true);
    }

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

        if (brain.agent.remainingDistance < underRangeToTarget.threshold) {
            curWeight += underRangeToTarget.flatWeight;
        }

        if (brain.agent.remainingDistance > brain.agent.stoppingDistance) {
            curWeight += hasNotReachedTarget.flatWeight;
        }

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

        totalWeight += underRangeToTarget.flatWeight;
        totalWeight += hasNotReachedTarget.flatWeight;

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        yield return 0f;
    }
}
