using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MovementEffects;

[CreateAssetMenu(fileName = "PersistentAoE", menuName = "Action/PersistentAoE")]
public class PersistentAoE : Action {

    public GameObject aoePrefab;
    public float damagePerSec, cooldown, fieldRadius, duration, firingAngle, gravity;
    public bool requireLosToCast;
    public LayerMask validTargets;

    public Curve proximityToTarget;
	public Bool targetFleeing, losToPlayer;

	public Curve health;

    private float cooldownTimer;

    public override void Init(Brain brain)
    {
        cooldownTimer = -cooldown;
    }

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

        if (Time.time - cooldownTimer > cooldown)
            onCooldown = false;
        else {
            onCooldown = true;
            return 0;
        }

        float min = 0, max = 0;

        GetMinMaxInCurve(proximityToTarget, out min, out max);

        curWeight += proximityToTarget.curve.Evaluate(Mathf.Clamp(Vector3.Distance(brain.transform.position, brain.target.transform.position), min, max));

		GetMinMaxInCurve(health, out min, out max);

		curWeight += health.curve.Evaluate(Mathf.Clamp(brain.health.currentHealth / brain.health.maxHealth * 100, min, max));

        // Target fleeing check
		if (Vector3.Angle(brain.targetRb.velocity, brain.transform.position - brain.target.transform.position) > 110)
            curWeight += targetFleeing.flatWeight;

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

		totalWeight += GetMaxWeightInCurve (health);
        totalWeight += GetMaxWeightInCurve(proximityToTarget);
        totalWeight += targetFleeing.flatWeight;

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        RaycastHit hit;
        //if (Time.time - cooldownTimer > cooldown) {
        //    if (requireLosToCast) {
        //        if (Physics.Raycast(brain.transform.position, brain.target.transform.position + brain.targetRb.velocity - brain.transform.position, out hit, 100, losToPlayer.losMask, QueryTriggerInteraction.Ignore)) {
        //            if (hit.collider.CompareTag("Player")) {
        //                cooldownTimer = Time.time;
        //            }
        //            else
        //                yield break;
        //        }
        //        else {
        //            cooldownTimer = Time.time;
        //        }
        //    }
        //    else {
        //        cooldownTimer = Time.time;
        //    }
        //}
        //else
        //    yield break;

        brain.canCancel = false;
        brain.agent.Stop();
        brain.anim.SetBool("Run", false);

        // Face target
        while (Vector3.Angle(brain.transform.forward, brain.target.transform.position - brain.transform.position) > 10) {
            brain.transform.rotation = Quaternion.Slerp(brain.transform.rotation, Quaternion.LookRotation(brain.target.transform.position - brain.transform.position), Time.deltaTime * 5f);
            brain.anim.SetTrigger("Turn");
            yield return 0f;
        }

        brain.anim.SetTrigger("TossBomb");

        yield return Timing.WaitForSeconds(0.5f);

        Vector3 hitDestination = brain.target.transform.position + brain.target.GetComponent<Rigidbody>().velocity * (firingAngle/gravity);
        GameObject bomb = (GameObject)Instantiate(aoePrefab, brain.transform.position + Vector3.up * 4, Quaternion.identity);
        bomb.GetComponent<AoEFieldBomb>().SetValues(hitDestination, brain.transform.position, firingAngle, gravity, damagePerSec, fieldRadius, duration, validTargets);
    }
}
