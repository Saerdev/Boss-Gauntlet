using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MovementEffects;

[CreateAssetMenu(fileName = "New Heal", menuName = "Action/Heal", order = 1)]
public class Heal : Action {
    public GameObject skillFX;
    public float healAmount = 15, castTime, coolDown;
    private float coolDownTimer;

    [Header("Distance squared")]
    public Curve proximityToPlayer, health;

    private ParticleSystem[] particles;

    public override void Init(Brain brain)
    {
        coolDownTimer = -coolDown;
    }

    public override float CalculateWeight(Brain brain)
    {
        if (Time.time - coolDownTimer > coolDown) {
            onCooldown = false;
        }
        else {
            onCooldown = true;
            return 0;
        }

        float curWeight = 0;

        float dist = (brain.transform.position - brain.target.transform.position).sqrMagnitude;
        curWeight += proximityToPlayer.curve.Evaluate(dist);

		float min = 0, max = 0;

		GetMinMaxInCurve(health, out min, out max);

		curWeight += health.curve.Evaluate(Mathf.Clamp(brain.health.currentHealth / brain.health.maxHealth * 100, min, max));


        // Only attempt to heal if above 33% hp
        if (brain.health.currentHealth > (1f / 3f) * brain.health.maxHealth)
            currentWeight += health.curve.Evaluate(brain.health.currentHealth);

		currentWeight = curWeight;


            return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        return GetMaxWeightInCurve(proximityToPlayer) + GetMaxWeightInCurve(health);
    }

    protected override void OnValidate()
    {
        base.OnValidate();

        proximityToPlayer.maxWeight = GetMaxWeightInCurve(proximityToPlayer);
        health.maxWeight = GetMaxWeightInCurve(health);
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        if (!onCooldown) {
            coolDownTimer = Time.time;
        }
        else {
            yield break;
        }


        brain.isExecutingAction = true;
        brain.canCancel = false;
        brain.isActionInterruptible = interruptible;
        brain.agent.Stop();
        brain.anim.SetTrigger("Flex");
        GameObject fx = Instantiate(skillFX, brain.transform.position, Quaternion.identity) as GameObject;
        fx.transform.SetParent(brain.transform);
        particles = fx.GetComponentsInChildren<ParticleSystem>();
        yield return Timing.WaitForSeconds(castTime);
        brain.health.Heal(healAmount);
        yield return Timing.WaitForSeconds(3);
        Destroy(fx);
    }

    public override void Interrupt(Brain brain)
    {
        Timing.KillAllCoroutines();
        if (particles.Length > 0) {
            for (int i = 0; i < particles.Length; i++) {
                particles[i].Stop();
            }
        }
        brain.GetComponent<Boss1>().AE_AttackEnd();
    }
}
