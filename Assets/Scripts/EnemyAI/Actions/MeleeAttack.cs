using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MovementEffects;

[CreateAssetMenu(fileName = "MeleeAttack", menuName = "Action/MeleeAttack")]
public class MeleeAttack : Action {

    public float attackRange, damage;
    public Threshold underRangeToTarget;

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

        if (brain.topBrain.engageState == Brain.EngageState.Engaging) {
            if (Vector3.Distance(brain.transform.position, brain.target.transform.position) < underRangeToTarget.threshold)
                curWeight += underRangeToTarget.flatWeight;
        }
        else if (Vector3.Distance(brain.transform.position, brain.targetPos) < underRangeToTarget.threshold)
            curWeight += underRangeToTarget.flatWeight;

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

        totalWeight += underRangeToTarget.flatWeight;

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        //if (brain.topBrain.engageState != Brain.EngageState.Engaging) {
        //    yield break;
        //}

        brain.canCancel = false;
        brain.anim.SetBool("Run", true);

        while (!IsWithinAttackRange(brain)) {
            brain.SetDestination(brain.target.transform.position);
            yield return 0f;
        }

        //brain.agent.Stop();
        //brain.canCancel = false;
        //brain.anim.SetLayerWeight(0, 0);

        //brain.anim.SetTrigger("Attack");
        //brain.anim.SetBool("Run", false);

        // Rotate to face player when in range but not mid-attack
        while (Vector3.Angle(brain.transform.forward, brain.target.transform.position - brain.transform.position) > 15) {
            //if (brain.canCancel)
            brain.SetDestination(brain.target.transform.position);
            brain.transform.rotation = Quaternion.Lerp(brain.transform.rotation, Quaternion.LookRotation(brain.target.transform.position - brain.transform.position), Time.deltaTime * 10);
            yield return 0f;
        }

            brain.agent.Stop();
            brain.canCancel = false;
            brain.anim.SetLayerWeight(0, 0);

            brain.anim.SetTrigger("Attack");
            brain.anim.SetBool("Run", false);
    }

    protected bool IsWithinAttackRange(Brain brain)
    {
        if (brain.topBrain.engageState == Brain.EngageState.Engaging && brain.target) {
            if (Vector3.Distance(brain.transform.position, brain.target.transform.position) <= brain.agent.stoppingDistance) {
                brain.agent.avoidancePriority--;
                return true;
            }
            else {
                brain.agent.avoidancePriority++;
                return false;
            }
        }

        return false;
    }

    //void Attack(Brain brain)
    //{
    //    if (IsWithinAttackRange(brain)) {
    //        brain.transform.rotation = Quaternion.LookRotation(brain.target.transform.position - brain.transform.position);
    //        brain.canCancel = false;
    //        brain.agent.Stop();

    //        brain.anim.SetTrigger("Attack");
    //        brain.anim.SetBool("Run", false);
    //    }
    //}
}
