using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Engage", menuName = "Action/Engage", order = 0)]
public class Engage : Action {

//    public Threshold overHealth;
    public Bool noLosToAnyExplosion;

	public Curve health;

    public override void Execute(Brain brain)
    {
        brain.topBrain.engageState = Brain.EngageState.Engaging;

        brain.disengageBrain.enabled = false;
        brain.engageBrain.enabled = true;
    }

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

//        if (brain.health.currentHealth > overHealth.threshold)
//            curWeight += overHealth.flatWeight;

		float min = 0, max = 0;

		GetMinMaxInCurve (health, out min, out max);

		curWeight += Mathf.Clamp (health.curve.Evaluate (brain.health.currentHealth / brain.health.maxHealth * 100), min, max);

        RaycastHit hit;
        bool hasLoSToExplosion = false;
        foreach(GameObject explosion in GlobalBlackboard.explosionCenters) {
            if (explosion) {
                if (Physics.Raycast(brain.transform.position, explosion.transform.position - brain.transform.position, out hit, Mathf.Infinity, noLosToAnyExplosion.losMask)) {
                    if (hit.collider.CompareTag("ExplosionMarker")) {
                        hasLoSToExplosion = true;
                        break;
                    }
                }
            }
        }

        if (!hasLoSToExplosion)
            curWeight += noLosToAnyExplosion.flatWeight;

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

		totalWeight += GetMaxWeightInCurve (health);
        totalWeight += noLosToAnyExplosion.flatWeight;

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        yield return 0f;
    }
}
