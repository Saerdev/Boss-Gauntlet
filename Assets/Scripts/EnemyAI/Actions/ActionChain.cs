using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using MovementEffects;

[CreateAssetMenu(fileName = "ActionChain", menuName = "Action/ActionChain", order = 2)]
public class ActionChain : Action {
    //[HideInInspector]
    public List<Action> actions = new List<Action>();

    public Bool targetFleeing, targetReached, hasLosToTarget;
    public Curve proximityToTarget, health;
    public LayerMask losMask;

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

        if (Vector3.Angle(brain.targetRb.velocity, brain.transform.position - brain.target.transform.position) > 110)
            curWeight += targetFleeing.flatWeight;

        if (brain.agent.remainingDistance <= brain.agent.stoppingDistance)
            curWeight += targetReached.flatWeight;

        RaycastHit hit;
        if (Physics.Raycast(brain.transform.position, brain.target.transform.position - brain.transform.position, out hit, 300, losMask, QueryTriggerInteraction.Ignore)) {
            if (hit.collider.CompareTag("Player")) {
                curWeight += hasLosToTarget.flatWeight;
            }
        }

        float min, max;
        GetMinMaxInCurve(proximityToTarget, out min, out max);

        curWeight += proximityToTarget.curve.Evaluate(Mathf.Clamp(Vector3.Distance(brain.transform.position, brain.target.transform.position), min, max));

        GetMinMaxInCurve(health, out min, out max);
        curWeight += health.curve.Evaluate(Mathf.Clamp(brain.health.currentHealth / brain.health.maxHealth * 100, min, max));

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

        totalWeight += GetMaxWeightInCurve(proximityToTarget);

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        brain.inActionChain = true;
        for (int i = 0; i < actions.Count; i++) {
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(actions[i]._Execute(brain)));
            while (!brain.canCancel)
                yield return 0f;
        }

        brain.inActionChain = false;
    }
}
