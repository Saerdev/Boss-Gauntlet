using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "New Chase", menuName = "Action/Chase")]
public class Chase : Action {

    public Threshold overPreferredDistance;
    public RangeThreshold healthWithinThreshold;
    public Curve proximityToPlayer;

    private Vector3 destination;

    public override float CalculateWeight(Brain brain)
    {
        currentWeight = 0;

        if ((brain.transform.position - brain.target.transform.position).sqrMagnitude > overPreferredDistance.threshold)
            currentWeight += overPreferredDistance.flatWeight;

        if (brain.health.currentHealth < healthWithinThreshold.threshold.rangeEnd && brain.health.currentHealth > healthWithinThreshold.threshold.rangeStart)
            currentWeight += healthWithinThreshold.flatWeight;

        float dist = (brain.transform.position - brain.target.transform.position).sqrMagnitude;
        currentWeight += proximityToPlayer.curve.Evaluate(dist);

        return currentWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float total = overPreferredDistance.flatWeight + healthWithinThreshold.flatWeight + GetMaxWeightInCurve(proximityToPlayer);

        return total;
    }

    protected override void OnValidate()
    {
        base.OnValidate();

        proximityToPlayer.maxWeight = GetMaxWeightInCurve(proximityToPlayer);
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        brain.agent.SetDestination(brain.target.transform.position);
        yield return 0f;
    }
}
