using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "TacticalMove", menuName = "Action/TacticalMove", order = 1)]
public class TacticalMove : Action {

    public float moveSpeed;
    public Curve proximityToSelf, proximityToSpawnLocation, proximityToExplosion, proximityToTarget;
    public Threshold overRangeToTarget;
    public Bool lineOfSightToPlayer, noLineOfSightToAnyExplosion;
    private Vector3 dest;

    public Dictionary<Vector3, float> posTable = new Dictionary<Vector3, float>();

    private ScanPositions scanPositions;
    //private List<GameObject> markers = new List<GameObject>();
    public GameObject scanPointMarker;
    //private List<Text> markerTexts = new List<Text>();

    private GameObject markersParent;

    public override void Execute(Brain brain)
    {
        posTable.Clear();

        //if (!markersParent && GlobalBlackboard.sampleCount > 0) {
        //    markersParent = new GameObject("Markers");

        //    markers.Clear();
        //    markerTexts.Clear();
        //    for (int i = 0; i < GlobalBlackboard.sampleCount; i++) {
        //        GameObject obj = Instantiate(scanPointMarker);

        //        markerTexts.Add(obj.transform.GetChild(0).GetComponentInChildren<Text>());
        //        markers.Add(obj);
        //        obj.transform.SetParent(markersParent.transform);
        //    }
        //}

        for (int i = 0; i < brain.topBrain.sampledPositions.Count; i++) {
            float pWeight = CalculatePosWeight(brain.topBrain, brain.topBrain.sampledPositions[i]);
            posTable.Add(brain.topBrain.sampledPositions[i], pWeight);
            //markers[i].transform.position = brain.sampledPositions[i];
            //markerTexts[i].text = pWeight.ToString("0");
        }

        //for (int i = 0; i < markers.Count; i++) {
        //    if (i > brain.sampledPositions.Count) {
        //        markers[i].transform.position = Vector3.down * 500;
        //    }
        //}

        float posWeight = 0;
        dest = brain.transform.position;
        foreach (KeyValuePair<Vector3, float> entry in posTable) {
            if (entry.Value > posWeight) {
                posWeight = entry.Value;
                dest = entry.Key;
            }
        }

        if (brain.disengageBrain.canCancel && brain.engageBrain.canCancel)
            brain.SetDestination(dest);
    }

    float CalculatePosWeight(Brain brain, Vector3 point)
    {
        float posWeight = 0;

        float min, max;
        float curWeight = 0;

        GetMinMaxInCurve(proximityToSelf, out min, out max);

        curWeight = proximityToSelf.curve.Evaluate(Mathf.Clamp(Vector3.Distance(point, brain.transform.position), min, max));
        posWeight += curWeight;

        GetMinMaxInCurve(proximityToTarget, out min, out max);

        curWeight = proximityToTarget.curve.Evaluate(Mathf.Clamp(Vector3.Distance(point, brain.target.transform.position), min, max));
        posWeight += curWeight;

        GetMinMaxInCurve(proximityToSpawnLocation, out min, out max);

        curWeight = proximityToSpawnLocation.curve.Evaluate(Mathf.Clamp(Vector3.Distance(point, brain.spawnLocation), min, max));
        posWeight += curWeight;

        GetMinMaxInCurve(proximityToExplosion, out min, out max);

        Vector3 nearestExplosion = brain.transform.position;
        float dist = 10000;
        foreach (GameObject obj in GlobalBlackboard.explosionCenters) {
            if (obj) {
                float distCheck = Vector3.SqrMagnitude(obj.transform.position - brain.transform.position);
                if (distCheck < dist) {
                    dist = distCheck;
                    nearestExplosion = obj.transform.position;
                }
            }
        }

        curWeight = proximityToExplosion.curve.Evaluate(Mathf.Clamp(Vector3.Distance(point, nearestExplosion), min, max));
        posWeight += curWeight;

        if (Vector3.Distance(point, brain.target.transform.position) > overRangeToTarget.threshold) {
            curWeight = overRangeToTarget.flatWeight;
            posWeight += curWeight;
        }

        RaycastHit hit;
        if (Physics.Raycast(point, brain.target.transform.position - point, out hit, Mathf.Infinity, lineOfSightToPlayer.losMask)) {
            curWeight = lineOfSightToPlayer.flatWeight;
            posWeight += curWeight;
        }

        foreach (GameObject obj in GlobalBlackboard.explosionCenters) {
            if (obj) {
                if (Physics.Raycast(point, obj.transform.position - point, out hit, Mathf.Infinity, noLineOfSightToAnyExplosion.losMask, QueryTriggerInteraction.Collide)) {
                    if (!hit.collider.CompareTag("ExplosionMarker")) {
                        curWeight = noLineOfSightToAnyExplosion.flatWeight;
                        posWeight += curWeight;
                    }
                }
            }
        }

        return posWeight;
    }

    public override float CalculateWeight(Brain brain)
    {
        return 1f;
    }

    protected override void OnValidate()
    {
        base.OnValidate();

        proximityToSelf.maxWeight = GetMaxWeightInCurve(proximityToSelf);
        proximityToSpawnLocation.maxWeight = GetMaxWeightInCurve(proximityToSpawnLocation);
        proximityToTarget.maxWeight = GetMaxWeightInCurve(proximityToTarget);
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        yield return 0f;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float pWeight = 0;

        pWeight += GetMaxWeightInCurve(proximityToSelf);
        pWeight += GetMaxWeightInCurve(proximityToTarget);
        pWeight += GetMaxWeightInCurve(proximityToSpawnLocation);
        pWeight += GetMaxWeightInCurve(proximityToExplosion);
        pWeight += overRangeToTarget.flatWeight;
        pWeight += lineOfSightToPlayer.flatWeight;
        pWeight += noLineOfSightToAnyExplosion.flatWeight;

        return pWeight;
    }
}
