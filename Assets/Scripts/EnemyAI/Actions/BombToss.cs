using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using MovementEffects;

[CreateAssetMenu(fileName = "AreaBlast", menuName = "Action/AreaBlast")]
public class BombToss : Action {

    public GameObject timeBomb;
    public float firingAngle, gravity, cooldown, damage, detonateDelay, explosionRadius, camShakeAmount;
    public LayerMask validTargets;
    private float cooldownTimer;
    public bool requireLosToCast = true;

    public Threshold overRangeToTarget, overHealth;
    public Curve proximityToTarget;
    public Bool targetFleeing, losToPlayer;

    public override void Init(Brain brain)
    {
        cooldownTimer = -cooldown;
    }

    public override float CalculateWeight(Brain brain)
    {
        float curWeight = 0;

        if (Time.time - cooldownTimer > cooldown)
            onCooldown = false;
        else {
            onCooldown = true;
            return 0;
        }
        if (GameManager.DistanceSqrd(brain.transform.position, brain.target.transform.position) > overRangeToTarget.threshold * overRangeToTarget.threshold) {
            curWeight += overRangeToTarget.flatWeight;
        }

        RaycastHit hit;
        if (Physics.Raycast(brain.transform.position, brain.target.transform.position - brain.transform.position, out hit, 100, losToPlayer.losMask, QueryTriggerInteraction.Ignore)) {
            if (hit.collider.CompareTag("Player"))
                curWeight += losToPlayer.flatWeight;
        }


        if (brain.health.currentHealth > overHealth.threshold) {
            curWeight += overHealth.flatWeight;
        }

        currentWeight = curWeight;

        return curWeight;
    }

    protected override float CalculateTotalPotentialWeight()
    {
        float totalWeight = 0;

        totalWeight += overRangeToTarget.flatWeight;
        totalWeight += overHealth.flatWeight;
        totalWeight += GetMaxWeightInCurve(proximityToTarget);
        totalWeight += targetFleeing.flatWeight;
        totalWeight += losToPlayer.flatWeight;

        return totalWeight;
    }

    public override IEnumerator<float> _Execute(Brain brain)
    {
        //RaycastHit hit;
        //if (Time.time - cooldownTimer > cooldown) {
        //    if (requireLosToCast) {
        //        if (Physics.Raycast(brain.transform.position, brain.target.transform.position + brain.targetRb.velocity - brain.transform.position, out hit, 100, losToPlayer.losMask, QueryTriggerInteraction.Ignore)) {
        //            if (hit.collider.CompareTag("Player")) {
        //                cooldownTimer = Time.time;
        //            }
        //            else
        //                yield break;
        //        }
        //        else {
        //            cooldownTimer = Time.time;
        //        }
        //    }
        //    else {
        //        cooldownTimer = Time.time;
        //    }
        //}
        //else {
        //    yield break;
        //}

        brain.canCancel = false;
        //brain.anim.SetBool("Run", true);
        //brain.agent.SetDestination(brain.target.transform.position);
        //brain.agent.Resume();
        //yield return Timing.WaitForSeconds(0.2f);

        brain.agent.Stop();
        brain.anim.SetBool("Run", false);

        // Face target
        while (Vector3.Angle(brain.transform.forward, brain.target.transform.position - brain.transform.position) > 10) {
            brain.transform.rotation = Quaternion.Slerp(brain.transform.rotation, Quaternion.LookRotation(brain.target.transform.position - brain.transform.position), Time.deltaTime * 5f);
            brain.anim.SetTrigger("Turn");
            yield return 0f;
        }

        brain.anim.SetTrigger("TossBomb");

        yield return Timing.WaitForSeconds(.6f);

        Vector3 hitDestination = brain.target.transform.position + brain.target.GetComponent<Rigidbody>().velocity;
        float distance = Vector3.Distance(brain.transform.position, hitDestination);
        float initialVelocity = distance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / gravity);

        // Extract the X  Y componenent of the velocity
        float Vx = Mathf.Sqrt(initialVelocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(initialVelocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad);

        // Calculate flight time.
        float flightDuration = distance / Vx;

        GameObject bomb = (GameObject)Instantiate(timeBomb, brain.transform.position + Vector3.up * 4, Quaternion.identity);
        bomb.GetComponent<TimeBomb>().SetValues(detonateDelay, damage, explosionRadius, camShakeAmount, validTargets);

        GlobalBlackboard.explosionCenters.Add(bomb);

        // Rotate projectile to face the target.
        bomb.transform.rotation = Quaternion.LookRotation(hitDestination - bomb.transform.position);

        float elapse_time = 0;

        while (elapse_time < flightDuration) {
            bomb.transform.Translate(0, (Vy - (gravity * elapse_time)) * Time.deltaTime, Vx * Time.deltaTime);

            elapse_time += Time.deltaTime;

            yield return 0f;
        }
    }
}
