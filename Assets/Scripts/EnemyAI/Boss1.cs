using UnityEngine;
using System.Collections;

public class Boss1 : MonoBehaviour, IAnimEventCatcher {

    public float distanceToTarget;

    private Brain brain;
    private Enemy enemy;
//#pragma warning disable 0414 // assigned but not used
    //private Health health;
    public LeapAttack leapAttack;
    public MeleeAttack meleeAttack;
    private Collider col;
    private Animator anim;

    void Awake()
    {
        brain = GetComponent<Brain>();
        enemy = GetComponent<Enemy>();
        col = transform.FindChild("Model").GetComponent<Collider>();
        anim = GetComponent<Animator>();
        //health = GetComponent<Health>();
    }

    void FixedUpdate()
    {
        distanceToTarget = Vector3.Distance(transform.position, brain.target.transform.position);
    }

    public void AE_LeapSpeed()
    {
        col.enabled = false;
        brain.agent.speed = leapAttack.leapSpeed;
    }

    public void AE_StopMovement()
    {
        brain.agent.Stop();
        brain.anim.SetBool("Run", false);
    }

    public void AE_LeapAttackHit()
    {
        // Jump attack
        Collider[] cols = Physics.OverlapSphere(brain.transform.position, leapAttack.damageRadius, leapAttack.validTargets);
        for (int i = 0; i < cols.Length; i++) {
            Health hp = cols[i].GetComponent<Health>();
            if (hp != null) {
                hp.TakeDamage(leapAttack.damage);
            }
        }
    }

    public void AE_MeleeSwipeHit()
    {
        if (brain.target && ((brain.target.transform.position - transform.position).sqrMagnitude < meleeAttack.attackRange * meleeAttack.attackRange) && Vector3.Angle(transform.forward, brain.target.transform.position - transform.position) < 90)
            brain.target.GetComponent<Health>().TakeDamage(meleeAttack.damage);
    }

    public virtual void AE_Stun()
    {
        brain.agent.Stop();
        brain.engageBrain.canCancel = false;
        brain.disengageBrain.canCancel = false;
    }

    public virtual void AE_AttackEnd()
    {
        brain.anim.SetBool("Run", false);
        brain.agent.ResetPath();
        brain.engageBrain.canCancel = true;
        brain.disengageBrain.canCancel = true;
        brain.agent.speed = enemy.speed;
        col.enabled = true;
        brain.agent.stoppingDistance = enemy.range;
    }

    private void AE_Resume()
    {
        brain.agent.Resume();
        brain.anim.SetBool("Run", true);
        brain.isExecutingAction = false;
        brain.canCancel = true;
    }

    public virtual void AE_AnimSpeed(float speed)
    {
        anim.SetFloat("AnimSpeed", speed);
    }
}
