using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using MovementEffects;

[Serializable]
public abstract class Action : ScriptableObject, IInterruptible {

    public bool onCooldown;

    public string actionName
    {
        get { return this.GetType().ToString(); }
        set { actionName = value; }
    }

    [ReadOnly] public float totalPotentialWeight;
    [ReadOnly] public float currentWeight;

    public bool interruptible;
    [HideInInspector] public bool canCancel = true;

    public virtual void Init(Brain brain = null) { }

    public abstract float CalculateWeight(Brain brain);

    public virtual void Execute(Brain brain) { Timing.RunCoroutine(_Execute(brain)); }

    public abstract IEnumerator<float> _Execute(Brain brain);

    protected abstract float CalculateTotalPotentialWeight();

    protected virtual void OnValidate()
    {
        totalPotentialWeight = CalculateTotalPotentialWeight();
    }

    protected virtual float GetMaxWeightInCurve(Curve curve)
    {
        float highestValue = 0;
        foreach (Keyframe key in curve.curve.keys) {
            if (key.value > highestValue)
                highestValue = key.value;
        }

        return highestValue;
    }

    protected virtual float GetMaxWeightInCurve(AnimationCurve curve)
    {
        float highestValue = 0;
        foreach (Keyframe key in curve.keys) {
            if (key.time > highestValue)
                highestValue = key.time;
        }

        return highestValue;
    }

    protected void GetMinMaxInCurve(Curve curve, out float min, out float max)
    {
        min = 0;
        max = 0;

        foreach (Keyframe kf in curve.curve.keys) {
            if (kf.time < min)
                min = kf.time;

            if (kf.time > max)
                max = kf.time;
        }
    }

    public virtual void Interrupt(Brain brain)
    {
    }
}

[Serializable]
public struct Scale {
    public float limit, maxWeight;
}

[Serializable]
public struct Threshold {
    public float threshold, flatWeight;
    [ReadOnly]
    public float currentWeight;
}

[Serializable]
public struct RangeThreshold {
    [MinMaxRange(0, 100)]
    public MinMaxRange threshold;
    public float flatWeight;
    [ReadOnly]
    public float currentWeight;
}

[Serializable]
public struct Curve {
    public AnimationCurve curve;
    [ReadOnly]
    public float maxWeight;
    [ReadOnly]
    public float currentWeight;
}

[Serializable]
public struct Bool {
    public float flatWeight;
    [ReadOnly]
    public float currentWeight;
    public LayerMask losMask;
}