using UnityEngine;
using System.Collections;

public class PieTrigger : MonoBehaviour {
    private float damage;

    public GameObject trigger;

    void Awake()
    {
        trigger = transform.GetChild(0).gameObject;
    }

    public void SetValues(float damage)
    {
        this.damage = damage;
        Invoke("_Destroy", 0.1f);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player")) {
            col.GetComponent<Health>().TakeDamage(damage);
            Destroy(transform.parent.gameObject);
        }
    }

    void _Destroy()
    {
        if (gameObject)
            Destroy(transform.parent.gameObject);
    }
}
