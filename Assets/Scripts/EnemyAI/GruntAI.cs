using UnityEngine;
using System.Collections;

public class GruntAI : Enemy {

    public float jumpAttackRange = 12, jumpSpeed = 18, jumpAttackDamageRadius = 3;

    //private float originalSpeed;
    private OddmentTable<State> stateTable = new OddmentTable<State>();
    private State state;
    private bool attackQueued;
    private Collider col;

    protected override void Awake()
    {
        base.Awake();

        //originalSpeed = speed;
        col = GetComponent<Collider>();

        stateTable.Add(State.Idle, 5);
        stateTable.Add(State.Attack, 45);
        stateTable.Add(State.JumpAttack, 45);
    }

    private enum State {
        Idle,
        Attack,
        JumpAttack
    }

    protected override void Attack()
    {
        if (!attacking && !attackQueued && target && (transform.position - destination).sqrMagnitude <= jumpAttackRange * jumpAttackRange) {

            state = stateTable.Pick();

            if (state == State.Idle) {
                anim.SetBool("Run", false);
                anim.SetTrigger("Flex");
                agent.Stop();
                attacking = true;
                return;
            }

            if (state == State.Attack) {
                StartCoroutine(QueueNormalAttack());
                return;
            }

            if (state == State.JumpAttack) {

                attacking = true;

                anim.SetLayerWeight(1, 1);
                anim.SetTrigger("JumpAttack");
                // Randomly lead the target
                //if (Random.Range(0, 2) == 1)
                    destination = target.transform.position + target.GetComponent<Rigidbody>().velocity;
                agent.SetDestination(destination);
                agent.stoppingDistance = 0;
            }
        }
    }

    // AnimationEvent
    public void AE_LeapSpeed()
    {
        agent.speed = jumpSpeed;
        col.enabled = false;
    }

    IEnumerator QueueNormalAttack()
    {
        attackQueued = true;
        yield return new WaitUntil(() => IsWithinAttackRange());
        attackQueued = false;
        attacking = true;
        anim.SetTrigger("Attack");
        anim.SetBool("Run", false);
    }

    private void AE_Resume()
    {
        col.enabled = true;
        if (target) {
            agent.ResetPath();
            agent.Resume();
            agent.stoppingDistance = range;
            agent.speed = speed;
            anim.SetBool("Run", true);
            attacking = false;
        }
    }

    public void AE_StopMovement()
    {
        agent.Stop();
    }

    public override void AE_AttackHit()
    {
        // Jump attack
        if (target && state == State.JumpAttack && (transform.position - target.transform.position).sqrMagnitude <= jumpAttackDamageRadius * jumpAttackDamageRadius)
            DamageTarget(damage);

        // Regular attack
        if (target && state == State.Attack && ((target.transform.position - transform.position).sqrMagnitude < range * range) && Vector3.Angle(transform.forward, target.transform.position - transform.position) < 90)
            DamageTarget(damage);
    }

}
