using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[SelectionBase]
[RequireComponent(typeof(Health), typeof(UnityEngine.AI.NavMeshAgent), typeof(LootTable))]
public class Enemy : MonoBehaviour {

    public int goldValue = 1;
    public int xpValue = 1;

    [HideInInspector]
    public string enemyName;
    [HideInInspector]
    public Vector3 start, destination, destinationCheck, exit;
    public float speed = 5.0f;
    public float range;

    //Attack variables
    public float damage = 10;
    public float attackCooldown = 0.5f;
    //[HideInInspector]
    public bool attacking;

    public float wanderRadius = 3;

    protected bool isRecovering;

    protected OddmentTable<GameObject> lootTable;

    protected Health health;

    protected Animator anim;

    public delegate void OnDestroyed(GameObject obj);
    public event OnDestroyed eOnDestroyed;

    public delegate void OnAttack(float damage);
    public event OnAttack eOnAttack;

    protected Rigidbody rb;

    public UnityEngine.AI.NavMeshAgent agent;
    protected UnityEngine.AI.NavMeshObstacle obstacle;
    protected UnityEngine.AI.NavMeshHit hit;

    protected Vector3 prevPos;
    protected float curSpeed;
    protected Vector3 curMove;

    public List<GameObject> targets = new List<GameObject>();
    public GameObject target;
    protected Collider targetCol;
    protected bool withinRangeOfTarget;
    protected Health targetHealth;
    protected float targetRadius;

    public LayerMask layerMask;

    protected GameObject player;
    protected Player playerScript;

    protected StatusEffectHandler status;

    // Required to avoid errors about setting navagent destination after agent is destroyed
    protected bool update;

    protected virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        obstacle = GetComponent<UnityEngine.AI.NavMeshObstacle>();
        lootTable = GetComponent<LootTable>().lootTable;
        anim = GetComponent<Animator>();

        health = GetComponent<Health>();
        status = GetComponent<StatusEffectHandler>();
        
        eOnDestroyed = null;

        update = true;
        //target = player;
    }

    protected virtual void Start()
    {
        player = FindObjectOfType<Player>().gameObject;
        if (player)
            playerScript = player.GetComponent<Player>();

        SaveManager.eOnSave += SetSaveData;

        health.eOnDefeated += DropGold;
        health.eOnDefeated += DropItem;
        health.eOnDefeated += GiveXP;

        eOnAttack += DamageTarget;

        agent.speed = speed;
        destination = exit;
        agent.stoppingDistance = range;
        //InvokeRepeating("CheckIfStuck", 2, 1);
        attacking = false;
        anim.SetLayerWeight(1, 1);

    }

    public void SetValues(Vector3 destination)
    {
        this.destination = destination;
    }

    protected virtual void Update()
    {
        //agent.speed = speed;
        //if (Vector3.Distance(destination, transform.position) > 3) {
        //    Quaternion newRot = Quaternion.LookRotation(destination - transform.position, Vector3.up);
        //    if (float.IsNaN(newRot.x) && float.IsNaN(newRot.y) && float.IsNaN(newRot.z))
        //        transform.rotation = newRot;
        //}

        //transform.eulerAngles = new Vector3(0, transform.rotation.y, 0);

        //if (agent.pathPending) {
        //    agent.velocity = transform.forward * agent.speed;
        //}

    }

    protected virtual void FixedUpdate()
    {
        if (!target && targets.Count > 0 && !status.isDisoriented) {
            GetNewPath();
        }
        else if (targets.Count == 0) {
            anim.SetBool("Run", false);
            agent.Stop();
        }

        if (target && (target.CompareTag("Player") || target.CompareTag("Decoy")))
            destination = target.transform.position;

        if (!attacking && target) {
            //destination = target.transform.position;
            agent.SetDestination(destination);
            anim.SetBool("Run", true);
            //transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position);
        }

        Attack();
    }

    void CheckIfStuck()
    {
        if (!attacking && agent.velocity.sqrMagnitude <= 1) {
            UnityEngine.AI.NavMeshHit navHit;
            if (targetCol && UnityEngine.AI.NavMesh.SamplePosition(targetCol.ClosestPointOnBounds(transform.position), out navHit, 3, UnityEngine.AI.NavMesh.AllAreas)){
                destination = navHit.position + Vector3.up * agent.height;
                agent.SetDestination(destination);
            }
        }
    }

    public virtual void GetNewPath(GameObject newTarget = null)
    {
        // Target closest enemy
        if (!newTarget && targets.Count > 0) {
            targets.RemoveAll(x => x == null);
            target = targets.OrderBy(x => GameManager.DistanceSqrd(x.transform.position, transform.position)).ToArray()[0];
        }
        else if (newTarget)
            target = newTarget;

        if (target) {
            targetHealth = target.transform.root.GetComponentInChildren<Health>();
            targetHealth.eOnDestroyed += RemoveTarget;
            //targetHealth.eOnDefeated += GetNewPath;
        }

        UnityEngine.AI.NavMeshHit navHit;
        if (target && UnityEngine.AI.NavMesh.SamplePosition(target.transform.position, out navHit, 4, UnityEngine.AI.NavMesh.AllAreas)) {
            destination = navHit.position;
            if (agent) {
                agent.SetDestination(destination);
                agent.Resume();
            }
            targetCol = target.transform.root.GetComponentInChildren<Collider>();
        }

    }

    protected bool IsWithinAttackRange()
    {
        if (target) {
            if ((transform.position - destination).sqrMagnitude <= range * range) {
                agent.avoidancePriority--;
                return true;
            }
            else {
                agent.avoidancePriority++;
                return false;
            }
        }

        return false;
    }

    protected virtual void Attack()
    {
        if (!attacking && target && IsWithinAttackRange() && Vector3.Angle(transform.forward, target.transform.position - transform.position) < 90) {

            attacking = true;

            anim.SetTrigger("Attack");
            anim.SetBool("Run", false);
        }
    }

    public virtual void AE_AttackHit()
    {
        if (target && IsWithinAttackRange() && Vector3.Angle(transform.forward, target.transform.position - transform.position) < 90)
            DamageTarget(damage);
    }

    public virtual void AE_AttackEnd()
    {
        attacking = false;
        //agent.speed = speed;
    }

    public virtual IEnumerator Wander()
    {
        target = null;

        Vector3 newDestination;
        UnityEngine.AI.NavMeshHit navHit;
        do {
            newDestination = transform.position + new Vector3(Random.insideUnitSphere.x, 0, Random.insideUnitSphere.z) * wanderRadius;
        } while (!UnityEngine.AI.NavMesh.SamplePosition(newDestination, out navHit, 1, -1));

        if (update)
            agent.SetDestination(newDestination);

        yield return new WaitUntil(() => update ? agent.remainingDistance <= agent.stoppingDistance + 0.1f : true);

        if (status.isDisoriented && target == null)
            StartCoroutine(Wander());
    }

    protected virtual void DamageTarget(float damage)
    {
        if (target) {
            targetHealth.TakeDamage(damage);
        }
    }

    public void AddTarget(GameObject entity)
    {
        targets.Add(entity);
        //GetNewPath();
    }

    public void RemoveTarget(GameObject entity)
    {
        targets.Remove(entity);
        if (target == entity) {
            target = null;
        }
    }

    void DropGold(GameObject nothing)
    {
        BuildingManager.gold += goldValue;
        BuildingManager.UpdateGoldText();
    }

    void DropItem(GameObject nothing)
    {
        if (lootTable.Count != 0) {
            GameObject droppedItem = lootTable.Pick();

            if (droppedItem)
                Instantiate(droppedItem, transform.position, Quaternion.identity);
        }
    }

    void GiveXP(GameObject source)
    {
        if (player)
            playerScript.GiveXP(xpValue);
    }

    public void SetSaveData()
    {
        SavedEnemy enemy = new SavedEnemy();
        enemy.enemyType = gameObject.name;
        enemy.posX = transform.position.x;
        enemy.posY = transform.position.y;
        enemy.posZ = transform.position.z;

        enemy.hp = health.currentHealth;
        enemy.energyShield = health.currentShield;

        SaveManager.Instance.GetListForScene().savedEnemies.Add(enemy);
    }

    protected virtual void OnDisable()
    {
        update = false;
    }

    protected virtual void OnDestroy()
    {
        if (health != null) {
            health.eOnDefeated -= DropGold;
            health.eOnDefeated -= DropItem;
            health.eOnDefeated -= GiveXP;
        }

        if (!GameManager.quittingGame) {
            if (eOnDestroyed != null) {
                eOnDestroyed(gameObject);
            }
        }

        SaveManager.eOnSave -= SetSaveData;

        if (target != null) {
            targetHealth.eOnDefeated -= GetNewPath;
        }

    }
}
