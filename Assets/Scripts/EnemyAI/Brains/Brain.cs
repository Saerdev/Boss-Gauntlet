using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Brain : MonoBehaviour, IInterruptible {

    [HideInInspector] public Brain brain;

    [SerializeField] protected BrainType brainType;
    public float tickRate;
    protected float lastTickTime;

    // Component references
    [HideInInspector] public UnityEngine.AI.NavMeshAgent agent;
    [HideInInspector] public Enemy enemy;
    [HideInInspector] public Health health;
    [HideInInspector] public Animator anim;
    [HideInInspector] public GameObject target;
    [HideInInspector] public Rigidbody targetRb;
    // Used if there is no target
    [HideInInspector] public Vector3 targetPos;
    [HideInInspector] public Vector3 spawnLocation;
    [HideInInspector] public TopBrain topBrain;
    [HideInInspector] public EngageBrain engageBrain;
    [HideInInspector] public DisengageBrain disengageBrain;
    [HideInInspector] public TacticalBrain tacticalBrain;

    [HideInInspector] public List<Vector3> sampledPositions = new List<Vector3>();

    // Actions
    [ReadOnly] public Action currentAction;
    [ReadOnly] public float currentActionWeight;
    [HideInInspector] public List<Action> actions = new List<Action>();
    private List<Action> topActions = new List<Action>();
    private OddmentTable<Action> actionTable = new OddmentTable<Action>();

    protected float highestWeight;
    protected int actionIndex;

    [HideInInspector] public bool isExecutingAction;        
    [HideInInspector] public bool isActionInterruptible;    // Whether the the current action can be interrupted by an incoming interrupt skill
    [HideInInspector] public bool canCancel = true;         // Used by Brain to determine whether to continue weighing actions to perform
    [HideInInspector] public bool inActionChain;

    // Only set in TopBrain
    [HideInInspector] public EngageState engageState;

    [HideInInspector] public TacticalMove move;

    protected enum BrainType {
        HighestWeight,
        FirstWeight
    }

    // Only set in TopBrain
    public enum EngageState {
        Engaging,
        Disengaging
    }

    protected virtual void Awake()
    {
        enemy = GetComponent<Enemy>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        health = GetComponent<Health>();
        anim = GetComponent<Animator>();


        topBrain = GetComponent<TopBrain>();
        engageBrain = GetComponent<EngageBrain>();
        disengageBrain = GetComponent<DisengageBrain>();
        tacticalBrain = GetComponent<TacticalBrain>();

        engageBrain.enabled = false;
        disengageBrain.enabled = false;

        lastTickTime = -tickRate;
    }

    void Start()
    {
        target = GameManager.instance.player.gameObject;
        targetRb = target.GetComponent<Rigidbody>();

        spawnLocation = transform.position;

        foreach (Action a in actions) {
            a.Init(this);
            if (a.GetType() == typeof(TacticalMove)) {
                move = (TacticalMove)a;
            }
        }
    }

    protected virtual void Update()
    {
        if (Time.time - lastTickTime > tickRate && engageBrain.canCancel && disengageBrain.canCancel) {
            lastTickTime = Time.time;
            Tick();
        }
    }

    protected virtual void Tick()
    {
        actionIndex = -1;

        float actionWeight;
        highestWeight = 0;

        if (!inActionChain) 
            currentAction = null;

        for (int i = 0; i < actions.Count; i++) {
            actionWeight = actions[i].CalculateWeight(this);

            if (brainType == BrainType.FirstWeight && actionWeight > 0 && !actions[i].onCooldown && !inActionChain) {
                currentAction = actions[i];
                actions[i].Execute(this);
                return;
            }

            if (actionWeight > 0 && !actions[i].onCooldown && !inActionChain) {
                if (actionWeight > highestWeight) {
                    highestWeight = actionWeight;
                    actionIndex = i;
                    topActions.Clear();
                    topActions.Add(actions[i]);
                }
                else if (actionWeight == highestWeight) {
                    topActions.Add(actions[i]);
                }
            }
        }

        if (actionIndex != -1) {
            if (topActions.Count <= 1) {
                currentAction = actions[actionIndex];
                currentActionWeight = actions[actionIndex].currentWeight;
                actions[actionIndex].Execute(this);
            }
            else {
                for (int i = 0; i < topActions.Count; i++) {
                    actionTable.Add(topActions[i], 1);
                }
                Action chosenAction = actionTable.Pick();
                currentAction = chosenAction;
                currentActionWeight = chosenAction.currentWeight;
                currentAction.Execute(this);
            }
        }
    }

    // Sampled positions are added from the Scanner
    public void AddSampledPosition(Vector3 pos)
    {
        sampledPositions.Add(pos);
    }

    public virtual void SetDestination(Vector3 dest)
    {
        agent.SetDestination(dest);
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        if (move) {
            foreach (KeyValuePair<Vector3, float> pos in move.posTable) {
                Gizmos.color = Color.white;
                Gizmos.DrawSphere(pos.Key + Vector3.down, 1);
                //DrawString(pos.Value.ToString("0"), pos.Key, Color.white);
                UnityEditor.Handles.Label(pos.Key + Vector3.up * 2, pos.Value.ToString("0"));
            }
        }
    }

    static void DrawString(string text, Vector3 worldPos, Color? colour = null)
    {
        UnityEditor.Handles.BeginGUI();
        if (colour.HasValue) GUI.color = colour.Value;
        var view = UnityEditor.SceneView.currentDrawingSceneView;
        Vector3 screenPos = view.camera.WorldToScreenPoint(worldPos);
        Vector2 size = GUI.skin.label.CalcSize(new GUIContent(text));
        GUI.Label(new Rect(screenPos.x - (size.x / 2), -screenPos.y + view.position.height + 4, size.x, size.y), text);
        UnityEditor.Handles.EndGUI();
    }
#endif

    public virtual void Interrupt(Brain brain)
    {
        if ((engageBrain.currentAction != null && engageBrain.currentAction.interruptible) || (disengageBrain.currentAction != null && disengageBrain.currentAction.interruptible)) {
            anim.SetTrigger("Interrupt");

            if (engageBrain.currentAction != null)
                engageBrain.currentAction.Interrupt(this);

            if (disengageBrain.currentAction != null)
                disengageBrain.currentAction.Interrupt(this);
        }
    }
}
