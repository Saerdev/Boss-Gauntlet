using UnityEngine;
using System.Collections;

public class SummonerAI : Enemy {

    public bool idle, summoning;

    public GameObject[] summonList;

    protected override void Start()
    {
        base.Start();

        agent.speed = speed;
        agent.stoppingDistance = range;

        target = gameObject;
    }

    protected override void Update()
    {
        if (IsWithinAttackRange() && !idle && !summoning) {
            int choice = Random.Range(0, 3);

            switch (choice) {
                case 0: StartCoroutine(Idle());
                    break;
                case 1: StartCoroutine(Summon());
                    break;
                case 2: GetNewPath();
                    break;
            }
        }
    }

    public override void GetNewPath(GameObject target = null)
    {
        destination = new Vector3(transform.position.x + Random.Range(-15, 15), transform.position.y, transform.position.z + Random.Range(-15, 15));
        UnityEngine.AI.NavMeshHit navHit;
        if (UnityEngine.AI.NavMesh.SamplePosition(destination, out navHit, Mathf.Infinity, UnityEngine.AI.NavMesh.AllAreas)) {
            destination = navHit.position + Vector3.up * agent.height;
            agent.SetDestination(destination);
        }
    }

    IEnumerator Idle()
    {
        idle = true;
        yield return new WaitForSeconds(3);
        idle = false;
    }
    IEnumerator Summon()
    {
        summoning = true;
        yield return new WaitForSeconds(3);
        Enemy enemyScript;
        GameObject summonedEnemy;
        summonedEnemy = (GameObject)Instantiate(summonList[Random.Range(0, summonList.Length)], transform.position + Vector3.forward, Quaternion.identity);
        enemyScript = summonedEnemy.GetComponent<Enemy>();
        enemyScript.GetNewPath();
        summoning = false;
    }
}
