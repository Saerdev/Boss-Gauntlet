using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "GlobalBlackboard", menuName = "Global Blackboard", order = 0)]
public class GlobalBlackboard : ScriptableObject {

    public static Player player;
    public static List<GameObject> explosionCenters = new List<GameObject>();
    public static int sampleCount;
}
