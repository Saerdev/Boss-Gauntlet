using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TurretInventory : MonoBehaviour {

    [HideInInspector]
    public GameObject inventoryPrefab;
    private GameObject inventory, inventories;
    public List<Item> items = new List<Item>();
    private List<Slot> slots = new List<Slot>();
    public static int globalID = 1;
    public int ID;

    private GameObject icon;

    //private bool isOpen;
    private GameObject window;

    private TurretAI turretReference;

    void Awake()
    {
        inventories = GameObject.Find("Inventories");
        inventoryPrefab = Resources.Load("Prefabs/UI/TurretInventory") as GameObject;
        turretReference = GetComponent<TurretAI>();
    }

    // Use this for initialization
    void Start()
    {
        //TurretInventory[] turInvs = inventories.GetComponentsInChildren<TurretInventory>(true);
        //for (int i = 0; i < turInvs.Length; i++) {
        //    if (turInvs[i])
        //}

        TurretInvReference[] refs = inventories.GetComponentsInChildren<TurretInvReference>(true);
        for (int i = 0; i < refs.Length; i++) {
            if (refs[i].turretID == ID) {
                refs[i].turretInventory = this;
                inventory = refs[i].gameObject;
                if (transform.root == transform) {
                    inventory.SetActive(true);
                }
                else {
                    inventory.SetActive(false);
                }
                window = inventory;
                break;
            }
        }

        if (!inventory) {
            inventory = Instantiate(inventoryPrefab);
            inventory.transform.SetParent(inventories.transform);
            inventory.GetComponent<TurretInvReference>().turretInventory = this;
            inventory.name = "TurretInventory";
            inventory.GetComponent<TurretInvReference>().turretID = ID;
            window = inventory;
            if (transform.root == transform) {
                inventory.SetActive(true);
            }
            else
                inventory.SetActive(false);
        }

        if (ID == 2) {
            inventory.transform.position += Vector3.right * 250;
        }

        while (items.Count < 3)
            items.Add(null);

        while (slots.Count < 3)
            slots.Add(null);

        Slot[] temp = window.GetComponentsInChildren<Slot>();
        for (int i = 0; i < temp.Length; i++) {
            slots[i] = temp[i];
        }

        //isOpen = gameObject.activeInHierarchy;
        //if (window)
        //    window.SetActive(true);
        Refresh();
    }

    public void Refresh()
    {
        Text quantityText;
        int i = 0;

        // Cycle through slots
        foreach (Slot slot in slots) {

            // Check for icon
            if (slot.GetComponentInChildren<DragHandler>()) {
                items[i] = slot.GetComponentInChildren<Item>();

                icon = slot.GetComponentInChildren<DragHandler>().gameObject;

                // Quantity text
                quantityText = icon.GetComponentInChildren<Text>();
                if (items[i].quantity > 1) {
                    quantityText.text = items[i].quantity.ToString();
                    quantityText.color = Color.red;
                }
                else
                    quantityText.color = Color.clear;

                icon.GetComponent<Image>().sprite = items[i].itemIcon;
                icon.GetComponent<Image>().color = Color.white;
                icon.name = items[i].itemName;
            }
            else
                items[i] = null;
            i++;
        }

        // Replace turret's attack with weapon upgrade if it exists
        if (items[0]) {
            WeaponUpgrade upgrade = items[0].GetComponent<WeaponUpgrade>();
            upgrade.turretReference = turretReference;
            turretReference.SetValues(upgrade.damage, upgrade.shotSpeed, upgrade.fireRate, upgrade.fireRateCap, upgrade.range);
        }

        // Utility slot
        if (items[2]) {
            items[2].GetComponent<UtilityUpgrade>().turretReference = turretReference;
            turretReference.eOnAttack -= turretReference.Attack;
            turretReference.eOnAttack += items[2].GetComponent<UtilityUpgrade>().Attack;
        }
        else {
            turretReference.eOnAttack = turretReference.Attack;
        }
    }

    public void SetSaveData()
    {
        for (int i = 0; i < items.Count; i++) {
            SavedTurretUpgrade savedItem = new SavedTurretUpgrade();
            if (items[i]) {
                savedItem.itemName = items[i].itemName;
                savedItem.turretID = ID;
                savedItem.slotIndex = i;
                SaveManager.Instance.GetListForScene().savedTurretUpgrades.Add(savedItem);
            }
        }
    }

    void OnDestroy()
    {
        if (inventory && transform.root == transform)
            inventory.SetActive(false);
    }
}
