using UnityEngine;
using System.Collections;

public class AoEField : MonoBehaviour {

    private float damage, radius, duration;

    private ParticleSystem particle;
    private GameObject player;

    public void SetValues(float damage, float radius, float duration)
    {
        this.damage = damage;
        this.radius = radius;
        this.duration = duration;

        Invoke("Destroy", duration);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) {
            player = other.gameObject;
            other.GetComponent<Health>().TakeDoT(damage, 60, gameObject, this.GetType());
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) {
            other.GetComponent<Health>().RemoveDoT(gameObject, this.GetType());
        }
    }

    void Destroy()
    {
        if (player)
            player.GetComponent<Health>().RemoveDoT(gameObject, this.GetType());

        Destroy(gameObject);
    }
}
