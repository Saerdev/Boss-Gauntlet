using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[SelectionBase]
public class Health : MonoBehaviour {

    public float currentHealth;
    [SerializeField]
    public float maxHealth = 100;
    public float currentShield;
    public float maxShield;
    public float shieldRechargeDelay = 2;
    [Header("per second")]
    public float shieldRechargeRate = 25;
    [HideInInspector]
    public float forcedShieldRechargeTime;
    public float shield;

    public Color healthDamaged = new Color(250 / 255f, 10 / 255f, 22 / 255f, 160 / 255f), 
                 energyShieldDamaged = new Color(25 / 255f, 255 / 255f, 255 / 255f, 160 / 255f), 
                 shieldDamaged = Color.yellow;

    [HideInInspector]
    public float lastDamageTime;

    public GameObject healthBarCanvas;
    private GameObject floatingDamageText;

    private float height;
    private Vector3 offset;
    //private float width;

    private RectTransform healthBarScale, energyShieldBarScale;
    private float maxX;

    private StatusEffectHandler statusEffectHandler;
    public bool invulnerable;
    [HideInInspector]
    public StatusEffects statusEffects;

    private float dotTimer, dotDamage;
    public List<Dot> dots = new List<Dot>();

    private Animator anim;
    private Rigidbody rb;
    private Collider[] ragdollColliders;

    private UnityEngine.AI.NavMeshAgent agent;

    public delegate void OnHit(float damage);
    public event OnHit eOnHit;

    public delegate void OnDamaged(float damage, GameObject target = null, GameObject source = null, System.Type skill = null);
    public event OnDamaged eOnDamaged;

    public delegate void OnDefeated(GameObject entity);
    public event OnDefeated eOnDefeated;

    public delegate void OnDestroyed(GameObject obj);
    public event OnDestroyed eOnDestroyed;

    private DamagedLayer damagedLayer;

    public struct Dot {
        public GameObject source;
        public float damage, duration;
        public System.Type skillSource;

        public Dot(GameObject src, float dmg, float duration, System.Type skill)
        {
            source = src;
            damage = dmg;
            this.duration = duration;
            skillSource = skill;
        }
    }

    enum DamagedLayer {
        health,
        energyShield,
        tempShield
    }

    void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        ragdollColliders = GetComponentsInChildren<Collider>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        //for (int i = 1; i < ragdollRbs.Length; i++) {
        //    ragdollRbs[i].isKinematic = true;
        //}

        //for (int i = 1; i < ragdollColliders.Length; i++) {
        //    ragdollColliders[i].enabled = false;
        //}

        height = GetComponentInChildren<Collider>().bounds.extents.y + 2;
        //width = GetComponent<Collider>().bounds.extents.x;
        //statusEffects = GetComponent<StatusEffects>();
        statusEffectHandler = GetComponent<StatusEffectHandler>();
        floatingDamageText = Resources.Load<GameObject>("Prefabs/UI/FloatingDamageNumber");
        currentHealth = maxHealth;
        currentShield = maxShield;

        floatingDamageText.CreatePool(15);
    }

    // Use this for initialization
    void Start()
    {
        eOnDamaged += FloatingDamageText;
        eOnDamaged += UpdateHealthBar;
        // Create healthbar if one doesn't already exist on the prefab
        //if (gameObject.CompareTag("Structure") || gameObject.CompareTag("Player") && gameObject.name != "Nexus") {
        GameObject canvas = (GameObject)Instantiate(healthBarCanvas, transform.position + Vector3.up * height, Quaternion.identity);// + Vector3.left * width / 1.75f, Quaternion.identity);// Inverse(Camera.main.transform.rotation));
        canvas.transform.SetParent(transform);
        canvas.transform.position = transform.position + Vector3.up * height;
        canvas.name = "HealthBarCanvas";
        healthBarScale = transform.FindChild("HealthBarCanvas").FindChild("HealthBar").GetComponent<Image>().rectTransform;
        energyShieldBarScale = transform.FindChild("HealthBarCanvas").FindChild("EnergyShieldBar").GetComponent<Image>().rectTransform;
        //transform.FindChild("HealthBarCanvas").FindChild("HealthBarBackground").GetComponent<Image>().rectTransform.localScale = new Vector3(healthBarScale.localScale.x * width, healthBarScale.localScale.y, healthBarScale.localScale.z);
        //healthBarScale.localScale = new Vector3(healthBarScale.localScale.x * width, healthBarScale.localScale.y, healthBarScale.localScale.z);
        //}
        //else {
        //    healthBarScale = transform.FindChild("HealthBarCanvas").FindChild("HealthBar").GetComponent<Image>().rectTransform;
        //    energyShieldBarScale = transform.FindChild("HealthBarCanvas").FindChild("EnergyShieldBar").GetComponent<Image>().rectTransform;
        //}
        healthBarCanvas = transform.FindChild("HealthBarCanvas").gameObject;
        if (gameObject.CompareTag("Player"))
            healthBarCanvas.transform.position = transform.position + Vector3.up * 5;

        if (agent)
            healthBarCanvas.transform.position = transform.position + Vector3.up * agent.height;

        healthBarCanvas.transform.position = transform.position + Vector3.up * ragdollColliders[0].bounds.size.y;

        offset = healthBarCanvas.transform.position - transform.position;

        maxX = healthBarScale.localScale.x;
        //yield return new WaitForEndOfFrame();
        UpdateHealthBar(0);
        eOnDamaged(0);

        if (currentHealth <= 0)
            Die(false);
    }

    void Update()
    {
        if ((currentShield < maxShield && Time.time - lastDamageTime > shieldRechargeDelay) || forcedShieldRechargeTime > 0) {
            currentShield += shieldRechargeRate * Time.deltaTime;

            forcedShieldRechargeTime -= Time.deltaTime;

            if (currentShield > maxShield)
                currentShield = maxShield;

            UpdateHealthBar(0);
        }

        if (dots.Count > 0)
            HandleDots();
    }

    public void TakeDamage(float damage, GameObject source = null, System.Type skill = null)
    {

        if (eOnHit != null)
            eOnHit(damage);

        if (!CheckInvuln(damage)) {
            if (shield > 0) {
                shield -= damage;
                damagedLayer = DamagedLayer.tempShield;

                if (shield < 0)
                    shield = 0;
            }
            else if (currentShield > 0) {
                currentShield -= damage;
                damagedLayer = DamagedLayer.energyShield;
            }
            else {
                currentHealth -= damage;
                damagedLayer = DamagedLayer.health;
            }

            if (eOnDamaged != null)
                eOnDamaged(damage, gameObject, source, skill);

            lastDamageTime = Time.time;
        }

        if (currentHealth <= 0) {
            Die();
        }
    }

    public void Die(bool playDeathAnim = true)
    {
        currentHealth = 0;
        UpdateHealthBar(0);


        if (GetComponent<Enemy>()) {
            Enemy enemy = GetComponent<Enemy>();
            enemy.enabled = false;
            enemy.agent.enabled = false;

            SkinnedMeshRenderer smr = GetComponentInChildren<SkinnedMeshRenderer>();
            smr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            smr.receiveShadows = false;
            smr.skinnedMotionVectors = false;
        }

        if (GetComponent<Player>()) {
            GetComponent<Player>().enabled = false;
            GetComponent<Mana>().currentMana = 0;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            foreach (Collider col in GetComponentsInChildren<Collider>())
                col.enabled = false;
        }

        foreach (MonoBehaviour script in GetComponentsInChildren<MonoBehaviour>())
            if (script != this)
                script.enabled = false;

        if (eOnDefeated != null) {
            eOnDefeated(gameObject);
            eOnDefeated = null;
        }
        if (eOnDestroyed != null) {
            eOnDestroyed(gameObject);
            eOnDestroyed = null;
        }

        anim.SetLayerWeight(1, 0);
        if (playDeathAnim)
            anim.SetTrigger("Die");
        else {
            //anim.SetTrigger("Die");
            anim.Play("Die", 0, 1);
        }

        Invoke("DisableAnimator", 3);
        ragdollColliders[0].enabled = false;
        healthBarCanvas.SetActive(false);
        rb.velocity = Vector3.zero;

        if (gameObject.CompareTag("Turret")) {
            Destroy(gameObject);
        }
        else
            Destroy(gameObject, 60);
    }

    void DisableAnimator()
    {
        anim.enabled = false;
    }

    public void TakeDoT(float damage = 0, float duration = 0, GameObject source = null, System.Type skillSource = null)
    {
        bool found = false;
        // Check for existing DoT from the same source
        for (int i = 0; i < dots.Count; i++) {
            // if it exists, refresh the duration
            if (skillSource != null && dots[i].skillSource == skillSource) {
                found = true;
                Dot temp = dots[i];
                temp.duration = duration;
                dots[i] = temp;
                break;
            }
        }

        // If it doesn't exist, add it to the DoTs list
        if (!found) {
            dots.Add(new Dot(source, damage, duration, skillSource));
        }
    }

    public void RemoveDoT(GameObject source, System.Type skillSource = null)
    {
        for (int i = 0; i < dots.Count; i++) {
            if (dots[i].source == source) {
                Dot temp = dots[i];
                temp.duration = 0;
                dots[i] = temp;
                return;
            }
        }
    }

    void HandleDots()
    {
        for (int i = 0; i < dots.Count; i++) {
            if (dots[i].duration > 0) {
                if (!CheckInvuln()) {
                    if (shield > 0)
                        shield -= dots[i].damage * Time.deltaTime;
                    else if (currentShield > 0)
                        currentShield -= dots[i].damage * Time.deltaTime;
                    else
                        currentHealth -= dots[i].damage * Time.deltaTime;

                    lastDamageTime = Time.time;
                    UpdateHealthBar(0);
                }
                Dot temp = dots[i];
                temp.duration -= Time.deltaTime;
                dots[i] = temp;

                if (dots[i].duration <= 0) {
                    if (dots[i].source == Player.rb.gameObject)
                        eOnDamaged -= Player.rb.GetComponent<CreepingFear>().SharePain;
                    dots.RemoveAt(i);
                }
            }
            else {
                dots.RemoveAt(i);
            }
        }

        if (currentHealth <= 0) {
            Die();
        }
    }

    public void Heal(float amount)
    {
        currentHealth += amount;

        FloatingHealText(amount);

        if (currentHealth >= maxHealth)
            currentHealth = maxHealth;

        UpdateHealthBar(0);
    }

    public void UpdateHealthBar(float nothing, GameObject target = null, GameObject source = null, System.Type skill = null)
    {
        healthBarScale.localScale = new Vector3(((float)currentHealth / maxHealth) * maxX, healthBarScale.localScale.y, healthBarScale.localScale.z);
        if (maxShield > 0) {
            energyShieldBarScale.localScale = new Vector3((float)(currentShield / maxShield) * maxX, energyShieldBarScale.localScale.y, energyShieldBarScale.localScale.z);
        }
        else {
            energyShieldBarScale.localScale = Vector3.zero;
        }

        PlayerHealthBar.instance.UpdateBar();
    }

    bool CheckInvuln(float damage = 0)
    {
        return statusEffectHandler.isInvulnerable;
        //foreach (StatusEffect status in statusEffects.statusEffects) {
        //    if (status.invulnerable) {
        //        return true;
        //    }
        //}
    }

    void FloatingDamageText(float damage, GameObject target = null, GameObject source = null, System.Type skill = null)
    {
        if (damage > 0) {
            GameObject floatingText = floatingDamageText.Spawn(transform.position + offset, floatingDamageText.transform.rotation);
            //GameObject floatingText = (GameObject)Instantiate(floatingDamageText, transform.position, floatingDamageText.transform.rotation);
            if (damagedLayer == DamagedLayer.tempShield) {
                floatingText.GetComponent<Text>().color = shieldDamaged;
                floatingText.GetComponent<Text>().text = "-" + damage.ToString();
            }
            else if (damagedLayer == DamagedLayer.energyShield) {
                floatingText.GetComponent<Text>().color = energyShieldDamaged;
                floatingText.GetComponent<Text>().text = "-" + damage.ToString();
            }
            else {
                floatingText.GetComponent<Text>().color = healthDamaged;
                floatingText.GetComponent<Text>().text = damage.ToString();
            }
            if (floatingText)
                floatingText.transform.SetParent(healthBarCanvas.transform);
        }
        //floatingText.transform.position = transform.position;
    }

    void FloatingHealText(float heal)
    {
        GameObject floatingText = (GameObject)Instantiate(floatingDamageText, transform.position + offset, floatingDamageText.transform.rotation);
        floatingText.GetComponent<Text>().text = heal.ToString();
        floatingText.GetComponent<Text>().color = Color.green;
        floatingText.transform.SetParent(healthBarCanvas.transform);
    }

    void OnDestroy()
    {
        eOnDamaged -= UpdateHealthBar;
        eOnDamaged -= FloatingDamageText;

        if (eOnDestroyed != null) {
            eOnDestroyed(gameObject);
            eOnDestroyed = null;
        }

        if (GetComponent<Enemy>()) {
            eOnDefeated -= GetComponent<Enemy>().GetNewPath;
        }
    }
}
