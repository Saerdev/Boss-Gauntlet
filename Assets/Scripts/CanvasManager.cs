using UnityEngine;
using System.Collections;

public class CanvasManager : MonoBehaviour {

    private Canvas[] canvases;
    public static CanvasManager instance;

    void Awake()
    {
        instance = this;
        canvases = FindObjectsOfType<Canvas>();
    }

	public void SetBackCanvases(Canvas exception)
    {
        for (int i = 0; i < canvases.Length; i++) {
            if (canvases[i] != exception) {
                canvases[i].sortingOrder = -1;
                print("Set " + canvases[i] + " sorting order");
            }
        }
    }
}
