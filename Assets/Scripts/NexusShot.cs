using UnityEngine;
using System.Collections;

public class NexusShot : MonoBehaviour {

    public GameObject explosionPrefab;
    public float cameraShakeAmount = 1;

    public int damage;
    private float lifetime;
    //private bool pierce;
    public float blastRadius;

    private Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        explosionPrefab.CreatePool(5);
    }

    public void SetValues(float speed, int damage, float lifetime, bool pierce, Vector3 destination, float blastRadius)
    {
        this.damage = damage;
        //this.pierce = pierce;
        this.blastRadius = blastRadius;

        Vector3 dir = destination - transform.position;
        rb.velocity = dir.normalized * speed;

        Invoke("CleanUp", lifetime);
    }

    void Update()
    {
        //rb.velocity = transform.up * speed;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Ground")) {
            explosionPrefab.Spawn(transform.position, Quaternion.identity);
            CamShake.instance.ShakeCam(cameraShakeAmount, 0.3f);
            Collider[] targets = Physics.OverlapSphere(transform.position, blastRadius, 1 << 8);
            foreach (Collider target in targets) {
                target.GetComponent<Health>().TakeDamage(damage);
            }

            gameObject.Recycle();
        }
    }

    void CleanUp()
    {
        gameObject.Recycle();
    }

    void OnDisable()
    {
        CancelInvoke("CleanUp");
    }

}
