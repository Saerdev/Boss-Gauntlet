using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum eGameAction {
    LEFTCLICK,
    RIGHTCLICK,
    AUTOFIRE,
    SPAWNWAVE,
	MOVE,
    DODGE,
	SKILL1,
	SKILL2,
	SKILL3,
	SKILL4,
	SKILL5,
	SKILL6,
	SKILL7,
	SKILL8,
    TURRET1,
    TURRET2,
    INVENTORY,
    CRAFTING,
    SKILLS,
    INTERACT,
    SAVE,
    LOAD,
	EXIT
}

public interface IKeyListener {
	bool OnKeyDown(eGameAction action);
	bool OnKeyHeld(eGameAction action);
    bool OnKeyUp(eGameAction action);
}

public class InputManager : PersistentUnitySingleton<InputManager> {
	private Dictionary<KeyCode, eGameAction> keyActionMap = new Dictionary<KeyCode, eGameAction>();
	private Dictionary<eGameAction, List<IKeyListener>> observers = new Dictionary<eGameAction, List<IKeyListener>>();

	public void AttachListener(eGameAction action, IKeyListener listener) {
		if (!observers.ContainsKey(action)) {
			observers.Add(action, new List<IKeyListener>());
		}

		observers[action].Add(listener);
	}

    public void RemoveListener(eGameAction action, IKeyListener listener)
    {
        if (observers.ContainsKey(action))
            observers[action].Remove(listener);
    }

	new void Awake() {
		SetDefaultKeys();
	}

	void Update() {
		foreach (KeyCode key in keyActionMap.Keys) {
			if (Input.GetKeyDown(key)) {
				if (KeyPressed(key, ButtonPressType.Down))
					return;
			}
			else if (Input.GetKey(key)) {
				if (KeyPressed(key, ButtonPressType.Held))
					return;
			}
			else if (Input.GetKeyUp(key))
				KeyPressed(key, ButtonPressType.Up);
		}
	}

	bool KeyPressed(KeyCode key, ButtonPressType type) {
		eGameAction action = keyActionMap[key];

		if (observers.ContainsKey(action))
			foreach (IKeyListener listener in observers[action]) 
				if (type == ButtonPressType.Down) {
					if (listener.OnKeyDown(action))
						return true;
				}
				else if (type == ButtonPressType.Held) {
					if (listener.OnKeyHeld(action))
						return true;
				}
                else if (type == ButtonPressType.Up) {
                    if (listener.OnKeyUp(action))
                        return true;
                }


		return false;
	}

	public void SetDefaultKeys() {
		keyActionMap.Clear();
		//W A S D are all mapped to MOVE
		keyActionMap.Add(KeyCode.W, eGameAction.MOVE);
		keyActionMap.Add(KeyCode.A, eGameAction.MOVE);
		keyActionMap.Add(KeyCode.S, eGameAction.MOVE);
		keyActionMap.Add(KeyCode.D, eGameAction.MOVE);
        keyActionMap.Add(KeyCode.LeftShift, eGameAction.DODGE);
        keyActionMap.Add(KeyCode.Mouse0, eGameAction.LEFTCLICK);
		keyActionMap.Add(KeyCode.Mouse1, eGameAction.RIGHTCLICK);
        keyActionMap.Add(KeyCode.Z, eGameAction.AUTOFIRE);
        keyActionMap.Add(KeyCode.Space, eGameAction.SPAWNWAVE);
        keyActionMap.Add(KeyCode.Alpha1, eGameAction.SKILL1);		
		keyActionMap.Add(KeyCode.Alpha2, eGameAction.SKILL2);
		keyActionMap.Add(KeyCode.Alpha3, eGameAction.SKILL3);
		keyActionMap.Add(KeyCode.Alpha4, eGameAction.SKILL4);
		keyActionMap.Add(KeyCode.Alpha5, eGameAction.SKILL5);
		keyActionMap.Add(KeyCode.Alpha6, eGameAction.SKILL6);
		keyActionMap.Add(KeyCode.Alpha7, eGameAction.SKILL7);
		keyActionMap.Add(KeyCode.Alpha8, eGameAction.SKILL8);
        keyActionMap.Add(KeyCode.Q, eGameAction.TURRET1);
        keyActionMap.Add(KeyCode.R, eGameAction.TURRET2);
        keyActionMap.Add(KeyCode.B, eGameAction.INVENTORY);
        keyActionMap.Add(KeyCode.C, eGameAction.CRAFTING);
        keyActionMap.Add(KeyCode.V, eGameAction.SKILLS);
        keyActionMap.Add(KeyCode.E, eGameAction.INTERACT);
        keyActionMap.Add(KeyCode.F1, eGameAction.SAVE);
        keyActionMap.Add(KeyCode.F5, eGameAction.LOAD);
        keyActionMap.Add(KeyCode.Escape, eGameAction.EXIT);
	}

	public enum ButtonPressType{
		Down,
		Held,
		Up
	}
}
