using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialManager : MonoBehaviour {

    public bool tutorialEnabled = true;

    public GameObject gatheringTutorial1, craftingTutorial1, craftingTutorial2, craftingWindow, turretTutorial1;

    private int tutorialStep;

    private GameObject[] tutorials = new GameObject[] { };
    private List<TutorialState> tutorialStates = new List<TutorialState>();

    private struct TutorialState {
        public GameObject tutorial;
        public bool wasActive;

        public TutorialState(GameObject tut, bool wasActive)
        {
            tutorial = tut;
            this.wasActive = wasActive;
        }
    }

    void Awake()
    {
        gatheringTutorial1.SetActive(true);
        craftingTutorial1.SetActive(false);
        craftingTutorial2.SetActive(false);
        turretTutorial1.SetActive(false);

        tutorials = new GameObject[] { gatheringTutorial1, craftingTutorial1, craftingTutorial2, turretTutorial1 };

        for (int i = 0; i < tutorials.Length; i++) {
            tutorialStates.Add(new TutorialState(tutorials[i], tutorials[i].activeInHierarchy));
        }
    }

    // Use this for initialization
    void Start()
    {
        Interact.eOnPickedUp += Gathering1;
        GameManager.eOnOpenedWindow += Crafting1;
        GameManager.eOnClosedWindow += Crafting2;
        CraftingWindow.eOnCrafted += Crafting3;
        TurretDeploy.eOnDeployedTurret += Turret1;
    }

    public void OnValidate()
    {
        if (!tutorialEnabled) {
            tutorialStates.Clear();
            for (int i = 0; i < tutorials.Length; i++) {
                if (tutorials[i]) {
                    tutorialStates.Add(new TutorialState(tutorials[i], tutorials[i].activeInHierarchy));
                    tutorials[i].SetActive(false);
                }
            }
        }
        else {
            for (int i = 0; i < tutorials.Length; i++) {
                    print(tutorialStates[i].tutorial);
                if (tutorials[i]) {
                    tutorials[i].SetActive(tutorialStates[i].wasActive);
                }
            }
        }
    }

    void Gathering1(GameObject obj)
    {
        if (tutorialEnabled && obj.CompareTag("Resource") && tutorialStep == 0) {
            Destroy(gatheringTutorial1);
            craftingTutorial1.SetActive(true);
            tutorialStep++;
        }
    }

    void Crafting1(GameObject obj)
    {
        if (tutorialEnabled && obj == craftingWindow && tutorialStep == 1) {
            Destroy(craftingTutorial1);
            GameManager.eOnOpenedWindow -= Crafting1;
            craftingTutorial2.SetActive(true);
            tutorialStep++;
        }
    }

    void Crafting2(GameObject obj)
    {
        if (tutorialEnabled && tutorialStep == 2 && obj == craftingWindow) {
            Destroy(craftingTutorial2);
            GameManager.eOnClosedWindow -= Crafting2;
            tutorialStep++;
        }
    }

    void Crafting3(GameObject obj)
    {
        if (tutorialEnabled && tutorialStep == 3 && obj.name == "Turret") {
            CraftingWindow.eOnCrafted -= Crafting3;
            turretTutorial1.SetActive(true);
            tutorialStep++;
        }
    }

    void Turret1(int num)
    {
        if (tutorialEnabled && tutorialStep == 4) {
            TurretDeploy.eOnDeployedTurret -= Turret1;
            turretTutorial1.SetActive(false);
            tutorialStep++;
        }
    }

    void OnDestroy()
    {
        Interact.eOnPickedUp -= Gathering1;
    }
}
