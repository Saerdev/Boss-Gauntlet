﻿using UnityEngine;
using System.Collections;

public class LootPickup : MonoBehaviour {

    private Camera mainCam;

    // Use this for initialization
    void Start()
    {
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
            if (hit.collider.CompareTag("Loot")) {
                Destroy(hit.collider.gameObject);
            }
        }
    }

}
