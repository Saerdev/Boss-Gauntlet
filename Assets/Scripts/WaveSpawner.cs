using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class WaveSpawner : MonoBehaviour, IKeyListener {

    public static WaveSpawner instance;
    public float minSpawnRange = 5, maxSpawnRange = 40;

    //public Transform[] entrances;
    private OddmentTable<GameObject> enemyList = new OddmentTable<GameObject>();

    public delegate void OnSpawnWave(int waveNumber);
    public event OnSpawnWave eOnSpawnWave;

    public delegate void OnSpawnedEnemy(GameObject obj);
    public event OnSpawnedEnemy eOnSpawnedEnemy;

    public float timeBetweenWaves = 30;
    public float timeBetweenEnemySpawns = 1;
    private float timeSinceBeganSpawning = 0;
    private bool isSpawning;

    public float timeUntilNextWave;
    private int waveNumber;

    //private WaveMakeup waveMakeup;
    //private OddmentTable<GameObject> spawnTable = new OddmentTable<GameObject>();

    public bool endlessMode;

    private EntityCounter entityCounter;
    //private Text waveCounter;

    void Awake()
    {
        instance = this;

        InputManager.Instance.AttachListener(eGameAction.SPAWNWAVE, this);

        //waveCounter = GameObject.Find("WaveCounter").GetComponent<Text>();
        entityCounter = GameObject.Find("EntityCounter").GetComponent<EntityCounter>();
    }

    void Start()
    {
        eOnSpawnWave = SpawnerHelper;
        waveNumber = 1;

        timeUntilNextWave = timeBetweenWaves;
    }

    void Update()
    {
        //timeUntilNextWave -= Time.deltaTime;

        //if (timeUntilNextWave <= 0) {
        //    eOnSpawnWave(waveNumber);
        //}

        if (isSpawning) {
            timeSinceBeganSpawning += Time.deltaTime;
        }
    }

    void SpawnerHelper(int wave)
    {
        isSpawning = true;
        //waveCounter.text = "Wave: " + wave;
        StartCoroutine(SpawnEnemy(wave));

        waveNumber++;
        timeUntilNextWave = timeBetweenWaves;
    }

    IEnumerator SpawnEnemy(int wave)
    {
        System.Random rand = new System.Random();

        //This needs to stay as a local variable in order to prevent overriding previous waves when spawning multiple at a time
        WaveMakeup waveMakeup = (WaveMakeup)Resources.Load("Waves/" + SceneManager.GetActiveScene().name + "/Wave " + wave);

        if (!waveMakeup)
            waveMakeup = (WaveMakeup)Resources.Load("Waves/" + SceneManager.GetActiveScene().name + "/FinalRepeatedWave");

        if (waveMakeup.waveComposition.enemyTypes.Length == 0)
            yield return null;

        foreach (WaveMakeup.Types enemy in waveMakeup.waveComposition.enemyTypes) {
            enemyList.Add(enemy.type, enemy.spawnWeight);
        }

        //for (int i = 0; i < waveMakeup.waveComposition.Length; i++) {
        for (int j = 0; j < waveMakeup.waveComposition.totalQuantity; j++) {
            //Randomize spawn location in a large circle
            float xPos = rand.Next(Mathf.RoundToInt(-maxSpawnRange), Mathf.RoundToInt(maxSpawnRange));
            float zPos = rand.Next(Mathf.RoundToInt(-maxSpawnRange), Mathf.RoundToInt(maxSpawnRange));

            if (Mathf.Abs(zPos) <= minSpawnRange) {
                if (xPos >= 0)
                    xPos = Mathf.Clamp(xPos, minSpawnRange, maxSpawnRange);
                else
                    xPos = Mathf.Clamp(xPos, -minSpawnRange, -maxSpawnRange);
            }

            if (Mathf.Abs(xPos) <= minSpawnRange) {
                if (zPos >= 0)
                    zPos = Mathf.Clamp(zPos, minSpawnRange, maxSpawnRange);
                else
                    zPos = Mathf.Clamp(zPos, -minSpawnRange, -maxSpawnRange);
            }

            Vector3 spawnPoint = new Vector3(transform.position.x + xPos, transform.position.y + 0.5f, transform.position.z + zPos);
            UnityEngine.AI.NavMeshHit navHit;
            if (UnityEngine.AI.NavMesh.SamplePosition(spawnPoint, out navHit, maxSpawnRange, UnityEngine.AI.NavMesh.AllAreas)) {
                spawnPoint = navHit.position;
                if (Mathf.Abs((spawnPoint - Player.rb.position).magnitude) > maxSpawnRange || Mathf.Abs(spawnPoint.y - Player.rb.position.y) > 2) {
                    j--;
                    continue;
                }
                // GameObject enemy = (GameObject)Instantiate(waveMakeup.waveComposition[i].enemyType[Random.Range(0, waveMakeup.waveComposition[i].enemyType.Length)], 
                //                     entrances[Random.Range(0, entrances.Length)].position + Vector3.left * Random.Range(-5, 5) + Vector3.up * Random.Range(-5, 5), Quaternion.identity);
                GameObject enemy = (GameObject)Instantiate(enemyList.Pick(), spawnPoint, Quaternion.identity);
                //GameObject enemy = (GameObject) Instantiate(waveMakeup.waveComposition[i].enemyType[Random.Range(0, waveMakeup.waveComposition[i].enemyType.Length)],
                //                    new Vector3(xPos, 0.5f, zPos), Quaternion.identity);
                Enemy enemyScript = enemy.GetComponent<Enemy>();
                if (eOnSpawnedEnemy != null)
                    eOnSpawnedEnemy(enemy);
                if (enemyScript.enemyName != "Summoner")
                    enemyScript.exit = transform.position;
                EntityCounter.entities++;
                entityCounter.UpdateText(null);
                enemyScript.eOnDestroyed += (GameObject obj) => EntityCounter.entities--;
                enemyScript.eOnDestroyed += entityCounter.UpdateText;
                //yield return new WaitForSeconds(1f);
                enemyScript.GetNewPath();
                waveMakeup.spawnsPerSecond = waveMakeup.spawnRate.Evaluate(timeSinceBeganSpawning);
                yield return new WaitForSeconds(1f / waveMakeup.spawnsPerSecond);
            }
        }
        //}
    }

    public bool OnKeyDown(eGameAction action)
    {
        //if (action == eGameAction.SPAWNWAVE) {
        //    eOnSpawnWave(waveNumber);
        //    return true;
        //}

        return false;
    }

    public bool OnKeyHeld(eGameAction action)
    {
        return false;
    }

    public bool OnKeyUp(eGameAction action)
    {
        return false;
    }

    void OnDestroy()
    {
        InputManager.Instance.RemoveListener(eGameAction.SPAWNWAVE, this);
    }
}
