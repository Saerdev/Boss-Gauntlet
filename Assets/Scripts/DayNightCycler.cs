using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class DayNightCycler : MonoBehaviour {

    public Gradient dayColor, nightColor;
    private Light sun;
    [Range(0, 210)]
    public float time;
    public static bool isNightTime;
    public AnimationCurve dayGraph, nightGraph, zGraph;
    public float nightStart = 100, nightEnd = 210;
    public float daySpeedMultiplier = 2;

	private List<GameObject> lightTrees =  new List<GameObject>();
	//private List<GameObject> particleTrees = new List<GameObject>();
	private bool lightTreesActive;

	public ParticleSystem ltParticles;

    void Awake()
    {
        sun = GetComponent<Light>();

		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("LightTree")) {
			lightTrees.Add (obj.GetComponent<Light>().gameObject);
//			particleTrees.Add (obj.GetComponent<ParticleSystem> (true).gameObject);
		}


    }

	// Use this for initialization
	void Start () {

	}

    // Update is called once per frame
    void Update() {
		time += Time.deltaTime;
        //time += Time.deltaTime / (7.5f + 7.5f * (1 / 3f));
        //time += Time.deltaTime + (dayNum * daySpeedMultiplier);
        //if (isNightTime)
        //    time += Time.deltaTime / (7.5f + 7.5f * (1 / 3f));
        //else
        //    time += Time.deltaTime / (7.5f + 7.5f * (1 / 3f)) * daySpeedMultiplier;
        time = Mathf.Repeat(time, 210);
        //if (time > nightStart || time < nightEnd) {
        //    sun.enabled = false;
        //}
        //else {
        //    sun.enabled = true;
        //}
        if (time >= nightStart) {
            isNightTime = true;
        }
        else {
            isNightTime = false;
        }

		//lightCrystalsActive = isNightTime;

		if (isNightTime) {
			sun.color = nightColor.Evaluate (time);
			sun.transform.localEulerAngles = new Vector3 (nightGraph.Evaluate (time), 0, zGraph.Evaluate(time));

			if (!lightTreesActive) {
				foreach (GameObject light in lightTrees) {
					light.SetActive (true);
//					ParticleSystem ps = Instantiate(ltParticles, ltParentPos, Quaternion.identity) as ParticleSystem;
//					ps.transform.SetParent (light.transform);
//					ParticleSystem ps = GetComponentInChildren<ParticleSystem>(true);
//					var em = ps.emission;
//					em.enabled = false;
				}

				lightTreesActive = true;
			}
		} else {
			sun.color = dayColor.Evaluate (time / nightStart);
			sun.transform.localEulerAngles = new Vector3 (dayGraph.Evaluate (time), 0, zGraph.Evaluate(time));

			if (lightTreesActive) {
				foreach (GameObject light in lightTrees) {
					light.GetComponent<Light> ().enabled = false;
					light.GetComponent<Animator> ().enabled = false;
					ParticleSystem ps = light.GetComponentInChildren<ParticleSystem>();
					var em = ps.emission;
					em.enabled = false;
				}
					
				lightTreesActive = false;


			}
		}

    }
}
