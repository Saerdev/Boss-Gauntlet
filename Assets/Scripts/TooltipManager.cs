using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TooltipManager : MonoBehaviour {

    public static TooltipManager instance;

    public Vector3 offset;
    private Vector2 quadrant, centeredPos;
    private Vector3  halfScreen;

    private GameObject skillBar;

    public bool isActive { get { return gameObject.activeSelf; } }

    private RectTransform rt;
    [HideInInspector]
    public Text tooltipText;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        halfScreen = new Vector3(Screen.width / 2, Screen.height / 2, 0);

        rt = GetComponent<RectTransform>();
        tooltipText = GetComponentInChildren<Text>();
        skillBar = FindObjectOfType<SkillBar>().gameObject;
        HideTooltip();
    }

    void Update()
    {
        //if (gameObject.activeSelf)
        //    transform.position = Input.mousePosition;
    }

    public void ShowTooltip(string text, GameObject obj)
    {
        centeredPos = Input.mousePosition - halfScreen;
        quadrant = new Vector2(Mathf.Sign(centeredPos.x), Mathf.Sign(centeredPos.y));
        //float oWidth = rt.rect.width;

        Vector3 finalOffset = Vector3.zero;

        if (obj.transform.parent == skillBar.transform) {
            if (tooltipText.text != text)
                tooltipText.text = text;

            if (quadrant.x < 0)
                finalOffset.x += offset.x;
            else
                finalOffset.x -= offset.x;

            if (quadrant.y < 0)
                finalOffset.y -= offset.y;
            else
                finalOffset.y += offset.y;

            transform.position = skillBar.transform.position + finalOffset;
            rt.sizeDelta = new Vector2(rt.rect.width, tooltipText.preferredHeight + 20);
        }
        else {
            Item item = obj.GetComponentInChildren<Item>();
            GameManager.eOnClosedWindow += HideTooltip;

            if (quadrant.x < 0)
                finalOffset.x += rt.rect.width / 2 + 20;
            else
                finalOffset.x -= rt.rect.width / 2 + 20;

            if (quadrant.y < 0)
                finalOffset.y += rt.rect.height + 40;

            finalOffset.y -= rt.rect.height + 20;

            tooltipText.text = (item.itemName != "" ? item.itemName : "Unnamed") + (item.itemDescription != "" ? "\n\n" + item.itemDescription : "");
            transform.position = obj.transform.position + finalOffset;
            //rt.sizeDelta = new Vector2(oWidth - 20, tooltipText.preferredHeight + 20);
        }

        // Scale the vertical size of the tooltip based on the amount of contained text
        gameObject.SetActive(true);
    }

    public void HideTooltip(GameObject window = null)
    {
        if (window == gameObject) 
            GameManager.eOnClosedWindow -= HideTooltip;

            gameObject.SetActive(false);
    }
}
