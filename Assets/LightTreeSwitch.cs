﻿using UnityEngine;
using System.Collections;

public class LightTreeSwitch : MonoBehaviour {

	public static bool inLightRange = false;

	public void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("LightTree") && DayNightCycler.isNightTime == true) {
			other.GetComponent<Animator> ().enabled = true;
			other.GetComponent<Light> ().enabled = true;
		
			ParticleSystem ps = other.GetComponentInChildren<ParticleSystem> ();
			var em = ps.emission;
			em.enabled = true;

			inLightRange = true;
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("LightTree")) {
			other.GetComponent<Animator> ().enabled = false;
			other.GetComponent<Light> ().enabled = false;

			ParticleSystem ps = other.GetComponentInChildren<ParticleSystem> ();
			var em = ps.emission;
			em.enabled = false;

			inLightRange = false;
		}
	}

	public void OnTriggerStay(Collider other)
	{
		if (other.CompareTag ("LightTree") && DayNightCycler.isNightTime == true) {
			other.GetComponent<Animator> ().enabled = true;
			other.GetComponent<Light> ().enabled = true;

			ParticleSystem ps = other.GetComponentInChildren<ParticleSystem> ();
 			var em = ps.emission;
			em.enabled = true;

			inLightRange = true;
		}
	}
}
