﻿using UnityEngine;
using UnityEditor;

//[CustomPropertyDrawer(typeof(WaveMakeup.WaveComposition))]
public class WaveMakeupDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect totalQuantityRect = new Rect(position.x, position.y, 50, position.height);
        Rect oddmentRect = new Rect(position.x + 10, position.y, 30, position.height);

        EditorGUI.PropertyField(totalQuantityRect, property.FindPropertyRelative("totalQuantity"));
        EditorGUI.PropertyField(oddmentRect, property.FindPropertyRelative("enemyTypes"), GUIContent.none);

        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
}
