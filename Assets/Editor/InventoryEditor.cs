using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Inventory))]
public class InventoryEditor : Editor {

    private Inventory inventory;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUI.BeginChangeCheck();
        if (Inventory.changedIndex != -1) {
            GUI.changed = true;
        }

        if (EditorGUI.EndChangeCheck()) {
            Inventory.instance.RemoveItem(ItemDatabase.itemDatabase["BaseItem"], 1, Inventory.changedIndex);
            Inventory.changedItem = null;
            Inventory.changedIndex = -1;
            GUI.changed = false;
        }
    }
}
